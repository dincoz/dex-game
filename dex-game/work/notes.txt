
scenarioList: [
{
	id: 100001,
	spList: [
	{
		id: 200001
		function: "item_picked",
		attr: {
			"playerId": 200,
			"itemId"  : 10156
		}
	}
	,{
		id: 200002
		function: "region_entered",
		attr: {
			"playerId": 200
			"region"  : baseRegion {
							x = 10,
							y = 10,
							height = 300,
							width = 500
						}
		}
	}]
}]

sceneInst = DBHandler.getSceneInst()

for(ScenarioInst scenarioInst : sceneInst.scenarioInstList){
	for(ScenarioPreqInst sp: scenarioInst.preqList){
		if(sp.function = "item_picked"){
			map.items.add(new ScenarioItem(sp.attr("itemId"), Db.getPlayer(sp.attr("playerId")), ...));
		}
		else if(sp.function = "region_entered"){
			map.triggerRegions.add(new ScenarioTriggerRegion(Db.getPlayer(sp.attr("playerId")), sp.attr("region"), ...));
		}
	}
}

IScenarioPreqElement{
	
}

Item{
	int itemId;
	Item(int itemId){
		this.itemId = itemId;
	}
	itemPickedAction(){
		...
	}
}

ScenarioItem extends Item{
	Player spControlPlayer;
	ScenarioItem(int itemId, Player spControlPlayer){
		super(itemId);
		this.spControlPlayer = spControlPlayer;
	}
	@Override
	itemPickedAction(Player pickPlayer){
		super.itemPickedAction();
		if(pickPlayer == spControlPlayer){
			triggerScenarioPreq();
		}
	}
}

object_unlocked(object:"bar_door":10101)
enemy_attacks(enemy:null, npc:{sp1.npc:200}, attackType:null)

void triggerScenarioPreq(long scenInstId){
	
}
