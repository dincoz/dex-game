package com.dexgdx.game;

import java.util.Collections;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.dexgdx.game.action.ActionManager;
import com.dexgdx.game.object.PlayerActor;

public class GameTimer extends TimerTask {
	
	private Game game;
	
	private ActionManager actionManager;
	
	public GameTimer(Game game, ActionManager actionManager) {
		super();
		this.game = game;
		this.actionManager = actionManager;
	}

	@Override
	public void run() {
		boolean playersAllDestroyed = true;
		for (PlayerActor player : game.gameObjects.getPlayerList()) {
			if(!player.isDestroyed()){
				playersAllDestroyed = false;
			}
		}
		if(playersAllDestroyed){
			Gdx.app.exit();
		}
		else if(!game.paused){
	        try {
	            Collections.sort(game.gameObjects.getSolidObjects(), game.yPositionComparator);
			} catch (Exception e) {
				// TODO: KALDIRILACAK
			}
			actionManager.runActorNextActions();
			actionManager.runActorNextTriggers();
		}
	}

}
