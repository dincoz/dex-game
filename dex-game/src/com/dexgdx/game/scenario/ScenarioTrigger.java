package com.dexgdx.game.scenario;

public class ScenarioTrigger implements IScenarioTrigger {

	long elementId;
	long scenarioId;
	
	public ScenarioTrigger(long elementId, long scenarioId){
		this.elementId = elementId;
		this.scenarioId = scenarioId;
	}

	@Override
	public long getScenarioId(){
		return scenarioId;
	}

	@Override
	public void scenarioItemTriggered() {
//		ScenarioManager.trigger(scenarioOid, elementOid);
	}

}
