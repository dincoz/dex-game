package com.dexgdx.game.scenario;

public interface IScenarioTrigger {
	long getScenarioId();
	void scenarioItemTriggered();
}
