package com.dexgdx.game.animation;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.constants.EMoveDirection;

public class DexAnimation extends Animation {
	
	private int stopRowIndex;	
	private EMoveDirection eMoveDirection;

	public DexAnimation(EMoveDirection eMoveDirection, int stopRowIndex, float frameDuration, TextureRegion... keyFrames) {
		super(frameDuration, keyFrames);
		this.stopRowIndex = stopRowIndex;
		this.eMoveDirection = eMoveDirection;
	}

	public int getStopRowIndex() {
		return stopRowIndex;
	}
	
	public TextureRegion getStopFrame(){
		return getKeyFrame(stopRowIndex);
	}

	public EMoveDirection getMoveDirection() {
		return eMoveDirection;
	}
	
}
