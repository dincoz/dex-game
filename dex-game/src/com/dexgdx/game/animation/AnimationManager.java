package com.dexgdx.game.animation;

import com.dexgdx.game.Game;
import com.dexgdx.game.util.trajectory.TrajectoryCalculator;

public class AnimationManager {
	
	private Game game;
	
	public AnimationManager(Game game) {
		super();
		this.game = game;
	}

	public DexAnimation getAnimationByKey(int playerNameId, int costumeNameId, int animationType){
		return getAnimationByKey(playerNameId, costumeNameId, animationType, 1);
	}
	
	public DexAnimation getAnimationByKey(int playerNameId, int costumeNameId, int animationType, int moveDirection){
		AnimationInfoKey animationInfoKey = AnimationInfoKey.fromData(playerNameId, costumeNameId, animationType, moveDirection) ;
		DexAnimation response = game.getAnimations().get(animationInfoKey.getKey());
		if(response == null){
			animationInfoKey = AnimationInfoKey.fromData(playerNameId, costumeNameId, animationType, TrajectoryCalculator.getSimpleDirection(moveDirection)) ;
			response = game.getAnimations().get(animationInfoKey.getKey());
		}
		return response;
	}
}
