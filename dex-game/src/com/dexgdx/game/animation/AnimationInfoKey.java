package com.dexgdx.game.animation;

import com.dexgdx.game.json.JSONAnimationObject;

public class AnimationInfoKey {
	
	private static AnimationInfoKey animationInfoKey;

	private int playerNameId;
	private int costumeNameId;
	private int animationType;
	private int moveDirection;
	
	public AnimationInfoKey() {
		super();
	}
	
	public AnimationInfoKey(int playerNameId, int costumeNameId,
			int animationType, int moveDirection) {
		super();
		initialize(playerNameId, costumeNameId, animationType, moveDirection);
	}
	
	private void initialize(int playerNameId, int costumeNameId,
			int animationType, int moveDirection){
		this.playerNameId = playerNameId;
		this.costumeNameId = costumeNameId;
		this.animationType = animationType;
		this.moveDirection = moveDirection;
	}
	
	public int getPlayerNameId() {
		return playerNameId;
	}
	public void setPlayerNameId(int playerNameId) {
		this.playerNameId = playerNameId;
	}
	public int getCostumeNameId() {
		return costumeNameId;
	}
	public void setCostumeNameId(int costumeNameId) {
		this.costumeNameId = costumeNameId;
	}
	public int getAnimationType() {
		return animationType;
	}
	public void setAnimationType(int animationType) {
		this.animationType = animationType;
	}
	public int getMoveDirection() {
		return moveDirection;
	}

	public void setMoveDirection(int moveDirection) {
		this.moveDirection = moveDirection;
	}

//	@Override
//	public boolean equals(Object obj) {
//		AnimationInfoKey animationInfoObj = (AnimationInfoKey) obj;
//		return animationInfoObj.playerNameId == playerNameId &&
//				animationInfoObj.costumeNameId == costumeNameId &&
//				animationInfoObj.animationType == animationType &&
//				animationInfoObj.moveDirection == eMoveDirection;
//	}
//	@Override
//	public int hashCode() {
//		int i = Integer.parseInt("" + playerNameId + costumeNameId + animationType + eMoveDirection) * 2;
//		return i;
//	}
	public Integer getKey() {
		Integer i = Integer.parseInt("" + playerNameId + costumeNameId + animationType + moveDirection);
		return i;
	}
	
	
	public static AnimationInfoKey fromData(int playerNameId, int costumeNameId,
			int animationType, int moveDirection){
		if(animationInfoKey == null){
			animationInfoKey = new AnimationInfoKey(playerNameId, costumeNameId, animationType, moveDirection);
		}
		else {
			animationInfoKey.initialize(playerNameId, costumeNameId, animationType, moveDirection);
		}
		return animationInfoKey;
	}
	
	public static AnimationInfoKey fromData(JSONAnimationObject jsonAnimationObject, int moveDirection){
		if(animationInfoKey == null){
			animationInfoKey = new AnimationInfoKey(jsonAnimationObject.getPlayerObjectInfo().getObjectInfo().getInfoNumber(), jsonAnimationObject.getCostumeNameId(), 
					jsonAnimationObject.getAnimationType(), moveDirection);
		}
		else {
			animationInfoKey.initialize(jsonAnimationObject.getPlayerObjectInfo().getObjectInfo().getInfoNumber(), jsonAnimationObject.getCostumeNameId(), 
					jsonAnimationObject.getAnimationType(), moveDirection);
		}
		return animationInfoKey;
	}
}
