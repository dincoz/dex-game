package com.dexgdx.game.inventory;
import java.util.*;
import com.dexgdx.game.util.*;
import com.dexgdx.game.object.*;

public class Inventory{
	
	public static final int SIZE = 24;
	public static final int SIZE_X = 6;
	public static final int SIZE_Y = 4;
	
	private Map<String, ObjectInfo> itemIndexMap;
	private Map<ObjectInfo, InventoryItem> itemMap;
	
	public Inventory(){
		itemIndexMap = new HashMap<String, ObjectInfo>();
		itemMap = new HashMap<ObjectInfo, InventoryItem>();
	}
	
	public void addItem(InventoryItem item){
		for(int x = 0 ; x < SIZE_X ; x++){
			for(int y = 0 ; y < SIZE_Y ; y++){
				if(itemIndexMap.get(x+","+y) == null){
					itemIndexMap.put(x+","+y, item.getObjectInfo());
					itemMap.put(item.getObjectInfo(),item);
					return;
				}
			}
		}
	}
	
	public void removeItem(int indexX, int indexY){
		itemMap.remove(itemIndexMap.get(indexX+","+indexY));
		itemIndexMap.remove(indexX+","+indexY);
	}
	
	public void switchItems(int fromIndexX, int fromIndexY, int toIndexX, int toIndexY){
		ObjectInfo toObjectInfo = itemIndexMap.get(toIndexX+","+toIndexY);
		itemIndexMap.put(toIndexX+","+toIndexY, itemIndexMap.get(fromIndexX + "," + fromIndexY));
		itemIndexMap.put(fromIndexX + "," + fromIndexY, toObjectInfo);
	}
	
	public InventoryItem getItem(int indexX, int indexY){
		return itemMap.get(itemIndexMap.get(indexX + "," + indexY));
	}
}
