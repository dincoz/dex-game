package com.dexgdx.game;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.InventoryItem;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.EnemyActor;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.object.NeutralActor;
import com.dexgdx.game.object.PlayerActor;

public class GameObjects {

	//DATA
	private List<SolidObject> solidObjects ;
	private List<InventoryItem> inventoryObjects ;
	private List<SolidObject> otherSceneObjects ;	
	private List<AbstractActor> actorList ;
	private List<EnemyActor> enemyList ;
	private List<PlayerActor> playerList ;
	private List<NeutralActor> neutralActorList ;
	private List<PlayerActor> selectedPlayerList;
	private List<MobileObject> movingObjects;
	private List<AbstractActor> attackingActors;
	
	public GameObjects() {
		solidObjects = new ArrayList<SolidObject>();
		otherSceneObjects = new ArrayList<SolidObject>();
		inventoryObjects = new ArrayList<InventoryItem>();
		actorList = new ArrayList<AbstractActor>();
		enemyList = new ArrayList<EnemyActor>();
		playerList = new ArrayList<PlayerActor>();
		neutralActorList = new ArrayList<NeutralActor>();
		selectedPlayerList = new ArrayList<PlayerActor>();
		movingObjects = new ArrayList<MobileObject>();
		attackingActors = new ArrayList<AbstractActor>();
	}

	public void addSolidObject(SolidObject solidObject){
		solidObjects.add(solidObject);
	}

	public void removeSolidObject(SolidObject solidObject){
		solidObjects.remove(solidObject);
	}

	public void addOtherSceneObject(SolidObject solidObject){
		addSolidObject(solidObject);
		otherSceneObjects.add(solidObject);
	}

	public void removeOtherSceneObject(SolidObject solidObject) {
		removeSolidObject(solidObject);
		otherSceneObjects.remove(solidObject);
	}
	
	public void addInventoryObject(InventoryItem inventoryItem){
		inventoryObjects.add(inventoryItem);
	}

	public void removeInventoryObject(SolidObject solidObject) {
		removeSolidObject(solidObject);
		inventoryObjects.remove(solidObject);
	}
	
	public void addActor(AbstractActor actor){
		addSolidObject(actor);
		if(actor instanceof PlayerActor)
			playerList.add((PlayerActor)actor);
		else if(actor instanceof EnemyActor)
			enemyList.add((EnemyActor)actor);
		else if(actor instanceof NeutralActor)
			neutralActorList.add((NeutralActor)actor);
		actorList.add(actor);
	}
	
	public void removeActor(AbstractActor actor){
		enemyList.remove(actor);
		playerList.remove(actor);
		actorList.remove(actor);
		neutralActorList.remove(actor);
		movingObjects.remove(actor);
		if(actor instanceof PlayerActor)
			deSelectPlayer((PlayerActor)actor);
		removeSolidObject(actor);
	}
	
	public AbstractActor getActor(int actorIndex){
		return (AbstractActor)solidObjects.get(actorIndex);
	}

	public void selectPlayer(PlayerActor player, boolean multiple){
//		boolean displayInfo = selectedPlayerList.contains(player) && selectedPlayerList.size() == 1;
		if(!multiple){
			deSelectAllPlayers();
		}
		if(playerList.contains(player) && !selectedPlayerList.contains(player)){
			player.setSelected(true);
			selectedPlayerList.add(player);
		}
//		if(!multiple && displayInfo){
//			displayActorInfo(player);
//		}
	}
	
	public void deSelectPlayer(PlayerActor player){
		player.setSelected(false);
		selectedPlayerList.remove(player);
	}

	public void selectSinglePlayer(PlayerActor player){
		selectPlayer(player, false);
	}
	
	public void selectAllPlayers(){
		for (PlayerActor player : playerList) {
			selectPlayer(player, true);
		}
	}
	
	public void deSelectAllPlayers(){
		Object[] playerToDeselect = selectedPlayerList.toArray();
		for (Object player : playerToDeselect) {
			deSelectPlayer((PlayerActor)player);
		}
	}
	
	public boolean isActorSelected(AbstractActor actor){
		return selectedPlayerList.contains(actor);
	}
	
	public boolean isAnyPlayerSelected(){
		return selectedPlayerList.size() > 0;
	}

	
	public void addMovingObject(MobileObject movingObject){
		if(!solidObjects.contains(movingObject)){
			addSolidObject(movingObject);
		}
		if(!movingObjects.contains(movingObject)){
			movingObjects.add(movingObject);
		}
	}
	
	public void removeMovingObject(MobileObject movingObject){
		movingObjects.remove(movingObject);
		if(!(movingObject instanceof AbstractActor)){
			removeSolidObject(movingObject);
		}
	}
	
	public void addAttackingActor(AbstractActor attackingActor, AbstractActor targetActor){
		attackingActor.startAttacking(targetActor);
		if(!attackingActors.contains(attackingActor)){
			attackingActors.add(attackingActor);
		}
	}
	
	public void removeAttackingActor(AbstractActor attackingActor){
		attackingActor.stopAttacking();
		attackingActors.remove(attackingActor);
	}

	public synchronized SolidObject getCollidingObject(SolidObject exclude, BaseRegion baseRegion) {
		SolidObject response = null;
		boolean isColliding = false;
		
		for (SolidObject solidObject : solidObjects){
			if(!solidObject.isDestroyed() && solidObject != exclude && solidObject.isCollidingObject()){
				isColliding = solidObject.isCollidingWith(baseRegion);
				if(isColliding){
					response = solidObject;
					break;
				}
			}
		}
		return response;
	}
	
	public List<AbstractActor> getActorList() {
		return actorList;
	}
	public List<EnemyActor> getEnemyList() {
		return enemyList;
	}
	public List<PlayerActor> getPlayerList() {
		return playerList;
	}
	
	public List<NeutralActor> getNeutralActorList() {
		return neutralActorList;
	}
	public List<PlayerActor> getSelectedPlayerList() {
		return selectedPlayerList;
	}
	public List<MobileObject> getMovingObjects() {
		return movingObjects;
	}
	public List<AbstractActor> getAttackingActors() {
		return attackingActors;
	}
	public List<SolidObject> getSolidObjects() {
		return solidObjects;
	}
	public List<SolidObject> getOtherSceneObjects() {
		return otherSceneObjects;
	}
	public List<InventoryItem> getInventoryObjects() {
		return inventoryObjects;
	}
	
	
}
