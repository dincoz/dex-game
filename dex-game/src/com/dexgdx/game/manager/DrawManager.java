package com.dexgdx.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EColor;
import com.dexgdx.game.constants.EUIContainer;
import com.dexgdx.game.dialog.DialogChoiceSet;
import com.dexgdx.game.dialog.DialogFlow;
import com.dexgdx.game.dialog.DialogSimpleLine;
import com.dexgdx.game.dialog.IDialogStep;
import com.dexgdx.game.input.DexTrailSwipeController;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.BasePoint;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.PlayerActor;
import com.dexgdx.game.object.TargetCursor;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.UIGame;
import com.dexgdx.game.ui.button.UIButton;
import com.dexgdx.game.ui.container.UIContainerActorInfo;
import com.dexgdx.game.ui.dialog.UIDialogBox;

public class DrawManager {

	SpriteBatch spriteBatch;
	ShapeRenderer renderer;
	
	public DrawManager(SpriteBatch spriteBatch, ShapeRenderer renderer) {
		super();
		this.spriteBatch = spriteBatch;
		this.renderer = renderer;
	}
	
	public DrawManager renderer(ShapeType type){
		if(spriteBatch.isDrawing())
			spriteBatch.end();
		renderer.begin(type);
		return this;
	}
	
	public DrawManager spriteBatch(){
		if(renderer.isDrawing())
			renderer.end();
		spriteBatch.begin();
		return this;
	}
	
	public DrawManager end(){
		if(renderer.isDrawing())
			renderer.end();
		if(spriteBatch.isDrawing())
			spriteBatch.end();
		return this;
	}

	public DrawManager regionImage(BaseRegion imageRegion, TextureRegion image, boolean transparent) {
		if(image != null) {
	        Color c = spriteBatch.getColor();
			if(transparent){
				Gdx.gl.glEnable(GL20.GL_BLEND);
				spriteBatch.setColor(c.r, c.g, c.b, .7f);
			}
			spriteBatch.draw(image, imageRegion.x, imageRegion.y, imageRegion.width, imageRegion.height);
			if(transparent){
				spriteBatch.setColor(c.r, c.g, c.b, 1f);
				Gdx.gl.glDisable(GL20.GL_BLEND);
			}
		}
		return this;
	}
	
	public DrawManager objectWithImage(SolidObject solidObject, boolean transparent) {
		return regionImage(solidObject.getImageCurrentRegion(), solidObject.getImageTextureRegion(), transparent);
	}
	
	public DrawManager objectWithImageRotate(SolidObject solidObject, boolean transparent, float angle) {
		BaseRegion imageRegion = solidObject.getImageCurrentRegion();
        Color c = spriteBatch.getColor();
		if(transparent){
			Gdx.gl.glEnable(GL20.GL_BLEND);
			spriteBatch.setColor(c.r, c.g, c.b, .7f);
		}
//		float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation
		spriteBatch.draw(solidObject.getImageTextureRegion(), imageRegion.x, imageRegion.y, imageRegion.width/2, imageRegion.height/2, imageRegion.width, imageRegion.height, 1, 1, angle);
		if(transparent){
			spriteBatch.setColor(c.r, c.g, c.b, 1f);
			Gdx.gl.glDisable(GL20.GL_BLEND);
		}
		return this;
	}

	public DrawManager rectangle(String color, float x, float y, float width, float height, float transparency){
        Color c = Color.valueOf(color);
		renderer.setColor(c.r, c.g, c.b, transparency);
		renderer.rect(x, y, width, height);
		return this;
	}

	public DrawManager gradientRectangle(float tg, float g, float x, float y, float width, float height, float transparency){
		int red = 255 - Math.round(255 * (g/tg));
		int green = Math.round(255 * (g/tg));
		int blue = 0;
		renderer.setColor(Color.valueOf(String.format("%02x%02x%02x", red, green, blue)));
		renderer.rect(x, y, width, height);
		return this;
	}
	

	public DrawManager moveTarget(EColor eRingColor, PlayerActor playerActor){
		TargetCursor tc = playerActor.getTargetCursor();
		if(tc != null) {
			tc = tc.getNext();
			renderer.setColor(tc.getColor());
			renderer.triangle(tc.getTri1()[0].x, tc.getTri1()[0].y, tc.getTri1()[1].x, tc.getTri1()[1].y, tc.getCenter().x, tc.getCenter().y);
			renderer.triangle(tc.getTri2()[0].x, tc.getTri2()[0].y, tc.getTri2()[1].x, tc.getTri2()[1].y, tc.getCenter().x, tc.getCenter().y);
			renderer.triangle(tc.getTri3()[0].x, tc.getTri3()[0].y, tc.getTri3()[1].x, tc.getTri3()[1].y, tc.getCenter().x, tc.getCenter().y);
			renderer.triangle(tc.getTri4()[0].x, tc.getTri4()[0].y, tc.getTri4()[1].x, tc.getTri4()[1].y, tc.getCenter().x, tc.getCenter().y);
		}
		return this;
	}
	
	public DrawManager ring(Color color, BaseRegion region){
		renderer.setColor(color);
		renderer.ellipse(region.x, region.y, region.width, region.height);
		return this;
	}
	
	public DrawManager actorRing(AbstractActor actor){
		ring(Color.valueOf(actor.getRingColor().getHexCode()), actor);
		
//		TOUCHREGION
//		drawRing(Color.valueOf(EColor.PATH_NODE.getHexCode()), actor.getTouchRegion());
		return this;
	}
	
	public DrawManager swipeLine(Game game, DexTrailSwipeController dexTrailSwipeController){
		if(game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction != EActionType.OTHER) {
			renderer.setColor(Color.valueOf(EColor.BEING_TARGET.getHexCode()+"44"));
//			renderer.setColor(Color.valueOf("FFFFFF"));
			renderer.end();
			for(PlayerActor player : game.gameObjects.getSelectedPlayerList()) {
				BasePoint center = player.getCenterPoint();
				if(player.selectedActiveAttackAction == EActionType.TRAIL ||
						player.selectedActiveAttackAction == EActionType.BULLET) {
					renderer.begin(ShapeType.Line);
					renderer.line(
							center.x, 
							center.y - 40, 
							dexTrailSwipeController.endPoint.x, 
							dexTrailSwipeController.endPoint.y - 40);
					renderer.end();
					renderer.begin(ShapeType.Line);
					renderer.ellipse(
							dexTrailSwipeController.endPoint.x - 5, 
							dexTrailSwipeController.endPoint.y - 40 - 4, 
							10, 
							8);
//					renderer.ellipse(
//							(center.x + dexTrailSwipeController.endPoint.x) / 2, 
//							(center.y + dexTrailSwipeController.endPoint.y) / 2 - 40, 
//							5, 
//							4);
					renderer.end();
					renderer.begin(ShapeType.Line);
					renderer.ellipse(
							dexTrailSwipeController.endPoint.x - 2, 
							dexTrailSwipeController.endPoint.y - 2, 5, 4);
					renderer.end();
				}
			}
		}
		
		return this;
	}
	
	public DrawManager swipeTarget(Game game, DexTrailSwipeController dexTrailSwipeController, UIGame ui){
		spriteBatch.end();
		for(PlayerActor player : game.gameObjects.getSelectedPlayerList()) {
			if(player.selectedActiveAttackAction == EActionType.TRAIL ||
					player.selectedActiveAttackAction == EActionType.BULLET) {
				BasePoint center = player.getCenterPoint();
				TextureRegion crossHairImage = ui.container(EUIContainer.BOTTOM_ACTIONS).getByActionType(player.selectedActiveAttackAction).getDefaultImage();
				spriteBatch.begin();
				spriteBatch.draw(
						crossHairImage,
						(center.x + dexTrailSwipeController.endPoint.x) / 2 - 15, 
						(center.y + dexTrailSwipeController.endPoint.y) / 2 - 15-40, 30, 30
					);
				spriteBatch.end();
			}
		}
		spriteBatch.begin();
		return this;
	}
	
//	public DrawManager uiDrawActorInfoTriangle(UIContainerActorInfo uiContainerActorInfo) {
//		Color c = uiContainerActorInfo.getColor();
//		renderer.setColor(c.r, c.g, c.b, uiContainerActorInfo.transparency);
//		BaseRegion actorInfo = uiContainerActorInfo
//		renderer.triangle(uiContainerActorInfo.getX(), y1, x2, y2, x3, y3);
//		return this;
//	}

	public DrawManager uiWithImage(UIButton uiButton) {
		uiButton.checkToggleStatusAndSize();
		uiWithImageCommon(uiButton);
		return this;
	}
	
	public DrawManager uiWithImage(UIElement uiElement) {
		uiWithImageCommon(uiElement);
		return this;
	}
	
	private void uiWithImageCommon(UIElement uiElement) {
		if(uiElement != null) {
			Color c = spriteBatch.getColor();
			spriteBatch.setColor(c.r, c.g, c.b, uiElement.transparency);
			if(uiElement.getWrapperImage() != null){
				spriteBatch.draw(uiElement.getWrapperImage(), uiElement.getRelativeX(),
						uiElement.getRelativeY(), uiElement.getWidth(),
						uiElement.getHeight());
			}
			if(uiElement.getImage() != null){
				spriteBatch.draw(uiElement.getImage(), uiElement.getRelativeX(),
						uiElement.getRelativeY(), uiElement.getWidth(),
						uiElement.getHeight());
				spriteBatch.setColor(c.r, c.g, c.b, 1f);
			}
		}
	}

	public DrawManager uiWithNoImage(UIElement uiElement) {
		Color c = uiElement.getColor();
		renderer.setColor(c.r, c.g, c.b, uiElement.transparency);
		renderer.rect(uiElement.getRelativeX(), uiElement.getRelativeY(),
				uiElement.getWidth(), uiElement.getHeight());
		return this;
	}

	public DrawManager uiDialogBoxText(UIElement uiElement) {
		UIDialogBox uiDialogBox = (UIDialogBox) uiElement;
		int stepIndex = 0;
		for (IDialogStep dialogStep : uiDialogBox.dialogSteps) {
			if(dialogStep instanceof DialogFlow){
				DialogSimpleLine dialogLine = ((DialogFlow) dialogStep).lines.get(0);
				
				uiDialogBox.font.setColor(Color.valueOf(dialogLine.lineOwner.getOriginalRingColor().getHexCode()));
				uiDialogBox.font.drawMultiLine(spriteBatch, dialogLine.lineOwner.getObjectInfo().getName() + " : ", 
						uiDialogBox.getRelativeX(), 
						uiDialogBox.getRelativeY() + (stepIndex * uiDialogBox.font.getLineHeight()));

				uiDialogBox.font.setColor(Color.WHITE);
				uiDialogBox.font.drawMultiLine(spriteBatch,dialogLine.text, 
						uiDialogBox.getRelativeX() + 120, 
						uiDialogBox.getRelativeY() + (stepIndex * uiDialogBox.font.getLineHeight()));
				stepIndex++;
			}
			else {
				DialogChoiceSet dialogChoiceSet = (DialogChoiceSet) dialogStep;
				int choiceIndex = 0;
				for (String dialogLine : dialogChoiceSet.choiceTexts) {
					uiDialogBox.font.setColor(Color.RED);
					uiDialogBox.font.drawMultiLine(spriteBatch, (choiceIndex + 1) + "- " + dialogLine, 
							uiDialogBox.getRelativeX(), 
							uiDialogBox.getRelativeY() + (stepIndex * uiDialogBox.font.getLineHeight()));
					choiceIndex++;
					stepIndex++;
				}
			}
		}
		return this;
	}
	

	public DrawManager uiActorInfoIndicators(UIContainerActorInfo uiContainerActorInfo) {
		Color c = spriteBatch.getColor();
		spriteBatch.setColor(c.r, c.g, c.b, uiContainerActorInfo.transparency);

		spriteBatch.draw( uiContainerActorInfo.bloodIndicator.bloodTube,
				uiContainerActorInfo.bloodIndicator.getRelativeX(),
				uiContainerActorInfo.bloodIndicator.getRelativeY(),
				uiContainerActorInfo.bloodIndicator.getTubeWidth(),
				uiContainerActorInfo.bloodIndicator.getHeight());
		
//		spriteBatch.draw( uiContainerActorInfo.bloodIndicator.bloodStartImage,
//				uiContainerActorInfo.bloodIndicator.getRelativeX(),
//				uiContainerActorInfo.bloodIndicator.getRelativeY(),
//				uiContainerActorInfo.bloodIndicator.getWidth(),
//				uiContainerActorInfo.bloodIndicator.getHeight());

		spriteBatch.draw( uiContainerActorInfo.bloodIndicator.bloodImage, 
				uiContainerActorInfo.bloodIndicator.getRelativeX(),
				uiContainerActorInfo.bloodIndicator.getRelativeY(),
				uiContainerActorInfo.bloodIndicator.getBloodWidth(),
				uiContainerActorInfo.bloodIndicator.getHeight());

		spriteBatch.draw( uiContainerActorInfo.diseaseIndicator.diseaseTube,
				uiContainerActorInfo.diseaseIndicator.getRelativeX(),
				uiContainerActorInfo.diseaseIndicator.getRelativeY(),
				uiContainerActorInfo.diseaseIndicator.getTubeWidth(),
				uiContainerActorInfo.diseaseIndicator.getHeight());
		
//		spriteBatch.draw( uiContainerActorInfo.diseaseIndicator.diseaseStartImage,
//				uiContainerActorInfo.diseaseIndicator.getRelativeX(),
//				uiContainerActorInfo.diseaseIndicator.getRelativeY(),
//				uiContainerActorInfo.diseaseIndicator.getWidth(),
//				uiContainerActorInfo.diseaseIndicator.getHeight());

		spriteBatch.draw( uiContainerActorInfo.diseaseIndicator.diseaseImage, 
				uiContainerActorInfo.diseaseIndicator.getRelativeX(),
				uiContainerActorInfo.diseaseIndicator.getRelativeY(),
				uiContainerActorInfo.diseaseIndicator.getDiseaseWidth(),
				uiContainerActorInfo.diseaseIndicator.getHeight());

//		spriteBatch.draw( uiContainerActorInfo.bloodIndicator.bloodEndImage, 
//				uiContainerActorInfo.bloodIndicator.getRelativeX() + uiContainerActorInfo.bloodIndicator.getWidth() * 10,
//				uiContainerActorInfo.bloodIndicator.getRelativeY(),
//				uiContainerActorInfo.bloodIndicator.getWidth(),
//				uiContainerActorInfo.bloodIndicator.getHeight());
		
		spriteBatch.setColor(c.r, c.g, c.b, 1f);
		return this;
	}
	
	public DrawManager alphaEnable(){
		Gdx.gl.glEnable(GL20.GL_BLEND);
		return this;
	}
	
	public DrawManager alphaDisable(){
		Gdx.gl.glDisable(GL20.GL_BLEND);
		return this;
	}
}
