package com.dexgdx.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EObjectInfo;

public class ParticleManager {
	
	private Game game;
	
	public ParticleManager(Game game) {
		super();
		this.game = game;
		
//		Particle p = null;
//		p.
	}

	public void loadParticleTextures(){
		ParticleEffect p = new ParticleEffect();
		p.load(Gdx.files.internal("data/particles/missile/particle.p"), Gdx.files.internal("data/particles/missile"));
		game.setDexMissileParticleEffectPool(new ParticleEffectPool(p, 10, 20));

		ParticleEffect p2 = new ParticleEffect();
		p2.load(Gdx.files.internal("data/particles/trail/particle.p"), Gdx.files.internal("data/particles/trail"));
		game.setDexTrailParticleEffectPool(new ParticleEffectPool(p2, 10, 50));
	}

	public PooledEffect obtainFromParticleEffectPool(EObjectInfo particleId){
		PooledEffect pooledEffect = null;
		if(particleId == EObjectInfo.DEX_MISSILE){
			pooledEffect = game.getDexMissileParticleEffectPool().obtain();
		}
		else if(particleId == EObjectInfo.DEX_TRAIL){
			pooledEffect = game.getDexTrailParticleEffectPool().obtain();
		}
		
		return pooledEffect;
	}
	
	public void freeToParticleEffectPool(EObjectInfo particleId, PooledEffect pooledEffect){
		if(particleId == EObjectInfo.DEX_MISSILE){
			game.getDexMissileParticleEffectPool().free(pooledEffect);
		}
		else if(particleId == EObjectInfo.DEX_TRAIL){
			game.getDexTrailParticleEffectPool().free(pooledEffect);
		}
	}

	public ParticleEffectPool getDexTrailParticleEffectPool() {
		return game.getDexTrailParticleEffectPool();
	}
	
}
