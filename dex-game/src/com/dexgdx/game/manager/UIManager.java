package com.dexgdx.game.manager;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIContainer;
import com.dexgdx.game.object.PlayerActor;
import com.dexgdx.game.ui.UIGame;
import com.dexgdx.game.ui.button.UIButtonBullet;
import com.dexgdx.game.ui.button.UIButtonDexCrush;
import com.dexgdx.game.ui.button.UIButtonDexGuard;
import com.dexgdx.game.ui.button.UIButtonDexMissile;
import com.dexgdx.game.ui.button.UIButtonDexTrail;
import com.dexgdx.game.ui.button.UIButtonDialog;
import com.dexgdx.game.ui.button.UIButtonHeal;
import com.dexgdx.game.ui.button.UIButtonInventoryOpen;
import com.dexgdx.game.ui.button.UIButtonPause;
import com.dexgdx.game.ui.button.UINodeQueueAction;
import com.dexgdx.game.ui.container.UIContainerPortrait;
import com.dexgdx.game.ui.dialog.UIDialogBox;
import com.dexgdx.game.ui.portrait.UIPortrait;
import com.dexgdx.game.ui.container.*;
import com.dexgdx.game.inventory.*;
import com.dexgdx.game.ui.button.*;

public class UIManager {

	Game game;
	
	public UIManager(Game game) {
		super();
		this.game = game;
	}
	
	public void setCameraAndUI(OrthographicCamera camera) {
		
		game.ui = new UIGame(game, camera);
		
		game.dialogBox = new UIDialogBox(game, camera, null);
		game.inventoryBox = (UIContainerInventory) game.ui.container(EUIContainer.INVENTORY);
		game.containerPortrait = (UIContainerPortrait) game.ui.container(EUIContainer.PORTRAIT);
		
		game.ui.container(EUIContainer.PAUSE).add(new UIButtonPause(game, camera, game.getTexture(EImagePath.UNPAUSE), game.getTexture(EImagePath.PAUSE)));
		game.ui.container(EUIContainer.CENTER_MENU).add(new UIButtonInventoryOpen(game, camera));
		game.ui.container(EUIContainer.INVENTORY).add(new UIButtonInventoryClose(game, camera));
		
		
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonHeal(game, camera, game.getTexture(EImagePath.DEX_HEAL_ON)));
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonDexMissile(game, camera));
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonDexTrail(game, camera));
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonDexCrush(game, camera));
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonDexGuard(game, camera));
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonDialog(game, camera));
		game.ui.container(EUIContainer.BOTTOM_ACTIONS).add(new UIButtonBullet(game, camera));
		
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonHeal(game, camera, game.getTexture(EImagePath.DEX_HEAL_ON)));
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonDexMissile(game, camera));
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonDexTrail(game, camera));
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonDexCrush(game, camera));
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonDexGuard(game, camera));
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonDialog(game, camera));
		game.ui.container(EUIContainer.SIDE_ACTIONS).add(new UIButtonBullet(game, camera));
		
		for(int i = 0 ; i < 8 ; ++i){
			game.ui.container(EUIContainer.QUEUE_ACTIONS).add(new UINodeQueueAction(game, camera, EActionType.OTHER, i));
		}
		
		game.ui.container(EUIContainer.DIALOG).add(game.dialogBox);
		for(int x = 0 ; x < Inventory.SIZE_X ; ++x){
			for(int y = 0 ; y < Inventory.SIZE_Y ; ++y){
				game.ui.container(EUIContainer.INVENTORY).add(new UIButtonInventoryItem(game, camera, game.inventoryBox, x, y));
			}
		}
		
		int i = 0;
		for (PlayerActor player : game.gameObjects.getPlayerList()) {
			game.ui.container(EUIContainer.PORTRAIT).add(new UIPortrait(game, camera, player, i));
			i++;
		}
		
//		game.containerActorInfo = (UIContainerActorInfo) ui.container(EUIContainer.ACTOR_INFO);
//		ui.container(EUIContainer.ACTOR_INFO).add(new UIIndicatorMan(game, camera, game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_MAN), game.containerActorInfo));
//		ui.container(EUIContainer.ACTOR_INFO).add(new UIIndicatorBlood(game, camera, null, game.containerActorInfo));
//		ui.container(EUIContainer.ACTOR_INFO).add(new UIIndicatorDisease(game, camera, null, game.containerActorInfo));
	}
}
