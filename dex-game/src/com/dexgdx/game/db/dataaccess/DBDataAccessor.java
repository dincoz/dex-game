package com.dexgdx.game.db.dataaccess;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dexgdx.game.Game;
import com.dexgdx.game.db.annotaion.Column;
import com.dexgdx.game.db.annotaion.Table;
import com.dexgdx.game.db.core.AbstractDataHandler;
import com.dexgdx.game.db.core.AbstractDataHandler.Result;

public class DBDataAccessor implements IDataAccessor{

	private AbstractDataHandler db;
	private Game game;
	
	private Map<Class<?>, List<Field>> classFields;

	public DBDataAccessor(AbstractDataHandler dbHandler, Game game) {
		this.db = dbHandler;
		this.game = game;
		classFields = new HashMap<Class<?>, List<Field>>();
	}
	
	public <T> List<T> getElementsByGameInst(Class<?> dbObjClass, int gameInst){
		Result result = db.query("select * from " + ((Table) dbObjClass.getAnnotation(Table.class)).name() + " where game_inst_id = " + gameInst);
		return fillListWithResult(dbObjClass, result);
	}
	
	@SuppressWarnings("unchecked")
	private <T> List<T> fillListWithResult(Class<?> dbObjClass, Result result) {
		List<T> daoList = new ArrayList<T>();
		try {
			List<Field> fields = classFields.get(dbObjClass);
			if(fields == null) {
				game.message = dbObjClass.getName() + " dao class not found";
			}
			else {
				while(result.moveToNext()){
					Object daoObject = dbObjClass.getConstructor().newInstance();
					for (Field field : fields) {
						setFieldValue(result, daoObject, field);
					}
					daoList.add((T)daoObject);
				}
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return daoList;
	}

	private void setFieldValue(Result result, Object daoObject, Field field)
			throws IllegalAccessException {
		Annotation annotation = field.getAnnotation(Column.class);
		Column defElementDAOColumn = (Column) annotation;
		field.set(daoObject, result.get(result.getColumnIndex(defElementDAOColumn.name())));
	}
}
