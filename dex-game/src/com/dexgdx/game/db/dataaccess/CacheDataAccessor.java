package com.dexgdx.game.db.dataaccess;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dexgdx.game.Game;
import com.dexgdx.game.db.domain.*;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.ObjectLockingIndexedCollection;
//import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.resultset.ResultSet;

import static com.googlecode.cqengine.query.QueryFactory.*;

@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
public class CacheDataAccessor implements IDataAccessor{
	
	private Game game;
	
	private List<Class<?>> daoClassList;
	private Map<String, IndexedCollection> daoClassDataMap;
	
	private IndexedCollection instCityDAOs;

	public CacheDataAccessor(Game game) {
		this.game = game;
		
		daoClassList = new ArrayList<Class<?>>();
		daoClassList.add(InstCityDAO.class);

		daoClassDataMap = new HashMap<String, IndexedCollection>();
		daoClassDataMap.put("InstCityDAO", instCityDAOs);
		
		for (Class<?> daoClass : daoClassList) {
			game.daoHandler.addDBType(daoClass);
		}
		
		instCityDAOs = new ObjectLockingIndexedCollection<InstCityDAO>();
		instCityDAOs.addIndex(HashIndex.onAttribute(InstCityDAO.GAME_INST_ID));
	}
	
//	public List getElementsByGameInst(String daoClass, Attribute attribute, int gameInstId){
		
//		List response = new ArrayList();
//		Query query = equal(attribute, gameInstId);
//		ResultSet list = daoClassDataMap.get(daoClass).retrieve(query);
//		for (Object instCityObj : list) {
//			response.add(instCityObj);
//		}
//		return response;
//	}
	
	public List<InstCityDAO> getElementsByGameInst(int gameInstId){
		
		IndexedCollection<InstCityDAO> instCityDAOs = new ObjectLockingIndexedCollection<InstCityDAO>();
		Query<InstCityDAO> query = equal(InstCityDAO.GAME_INST_ID, gameInstId);
		ResultSet<InstCityDAO> list = instCityDAOs.retrieve(query);

		List<InstCityDAO> response = new ArrayList<InstCityDAO>();
		for (InstCityDAO instCityObj : list) {
			response.add(instCityObj);
		}
		return response;
	}
}