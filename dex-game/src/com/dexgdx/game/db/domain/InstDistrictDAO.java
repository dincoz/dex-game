package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.*;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;

@Table(name = "INST_DISTRICT")
public class InstDistrictDAO extends InstDAO{

	@Column(name="district_def_id")
	public Integer defDistrictId;

	@Column(name="visible")
	public Integer visible;
	
	@Column(name="enabled")
	public Integer enabled;
	
	@Column(name="current")
	public Integer current;
	
	public static final Attribute<InstDistrictDAO, Integer> GAME_INST_ID = new SimpleAttribute<InstDistrictDAO, Integer>(
		"gameInstId") {
		@Override
		public Integer getValue(InstDistrictDAO instDistrictDAO, QueryOptions arg1) {
			return instDistrictDAO.gameInstId;
		}
	};

	public static final Attribute<InstDistrictDAO, Integer> CURRENT = new SimpleAttribute<InstDistrictDAO, Integer>(
		"current") {
		@Override
		public Integer getValue(InstDistrictDAO instDistrictDAO, QueryOptions arg1) {
			return instDistrictDAO.current;
		}
	};
}
