package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.Column;
import com.dexgdx.game.db.annotaion.Table;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;

@Table(name = "DEF_ELEMENT")
public class DefElementDAO {
	
	@Column(name="_id")
	public Integer id;
	
//	@Column(name = "NAME")
//	public String name;
//
//	public String type;
//	public String subTtype;

	public static final Attribute<DefElementDAO, Integer> ID = new SimpleAttribute<DefElementDAO, Integer>(
			"id") {
		@Override
		public Integer getValue(DefElementDAO defElementDAO, QueryOptions arg1) {
			return defElementDAO.id;
		}
	};
	
//	public static final Attribute<DefElementDAO, String> NAME = new SimpleAttribute<DefElementDAO, String>(
//			"name") {
//		@Override
//		public String getValue(DefElementDAO defElementDAO, QueryOptions arg1) {
//			return defElementDAO.name;
//		}
//	};
}
