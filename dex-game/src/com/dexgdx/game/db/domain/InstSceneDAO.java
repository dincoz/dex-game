package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.*;
import com.googlecode.cqengine.attribute.*;
import com.googlecode.cqengine.query.option.*;

@Table(name = "INST_SCENE")
public class InstSceneDAO extends InstDAO{

	@Column(name="scene_def_id")
	public Integer defSceneId;

	@Column(name="visible")
	public Integer visible;

	@Column(name="enabled")
	public Integer enabled;

	@Column(name="current")
	public Integer current;

	public static final Attribute<InstSceneDAO, Integer> GAME_INST_ID = new SimpleAttribute<InstSceneDAO, Integer>(
		"gameInstId") {
		@Override
		public Integer getValue(InstSceneDAO instSceneDAO, QueryOptions arg1) {
			return instSceneDAO.gameInstId;
		}
	};

	public static final Attribute<InstSceneDAO, Integer> CURRENT = new SimpleAttribute<InstSceneDAO, Integer>(
		"current") {
		@Override
		public Integer getValue(InstSceneDAO instSceneDAO, QueryOptions arg1) {
			return instSceneDAO.current;
		}
	};
}
