package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.*;

@Table(name = "INST_SCENARIO")
public class InstScenarioDAO extends InstDAO{

	@Column(name="scenario_def_id")
	public String defScenarioId;

	@Column(name="state")
	public Integer state;
}
