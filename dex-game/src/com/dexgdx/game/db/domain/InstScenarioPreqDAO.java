package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.*;

@Table(name = "INST_SCENARIO_PREQ")
public class InstScenarioPreqDAO extends InstDAO{

	@Column(name="scenario_inst_id")
	public Integer instScenarioId;

	@Column(name="scenario_preq_def_id")
	public Integer defScenarioPreqId;
	
	@Column(name="state")
	public Integer state;
	
}
