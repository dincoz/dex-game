package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.*;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;

@Table(name = "INST_CITY")
public class InstCityDAO extends InstDAO{

	@Column(name="city_def_id")
	public Integer defCityId;

	@Column(name="visible")
	public Integer visible;
	
	@Column(name="enabled")
	public Integer enabled;
	
	@Column(name="current")
	public Integer current;
	
	public static final Attribute<InstCityDAO, Integer> GAME_INST_ID = new SimpleAttribute<InstCityDAO, Integer>(
		"gameInstId") {
		@Override
		public Integer getValue(InstCityDAO instCityDAO, QueryOptions arg1) {
			return instCityDAO.gameInstId;
		}
	};

	public static final Attribute<InstCityDAO, Integer> CURRENT = new SimpleAttribute<InstCityDAO, Integer>(
		"current") {
		@Override
		public Integer getValue(InstCityDAO instCityDAO, QueryOptions arg1) {
			return instCityDAO.current;
		}
	};
}
