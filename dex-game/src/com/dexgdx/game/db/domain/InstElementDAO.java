package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.Column;

public class InstElementDAO extends InstDAO {

	@Column(name="scene_inst_id")
	public Integer instSceneId;

	@Column(name="elem_def_id")
	public Integer defElemId;

	@Column(name="X")
	public Integer x;

	@Column(name="Y")
	public Integer y;
}
