package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.Column;
import com.dexgdx.game.db.annotaion.Table;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;

@Table(name = "DEF_SCENARIO")
public class DefScenarioDAO {

	@Column(name="_id")
	public Integer id;

	@Column(name = "NAME")
	public String name;

	public static final Attribute<DefScenarioDAO, Integer> ID = new SimpleAttribute<DefScenarioDAO, Integer>(
			"id") {
		@Override
		public Integer getValue(DefScenarioDAO defScenarioDAO, QueryOptions arg1) {
			return defScenarioDAO.id;
		}
	};
	
	public static final Attribute<DefScenarioDAO, String> NAME = new SimpleAttribute<DefScenarioDAO, String>(
			"name") {

		@Override
		public String getValue(DefScenarioDAO defScenarioDAO, QueryOptions arg1) {
			return defScenarioDAO.name;
		}
	};
}
