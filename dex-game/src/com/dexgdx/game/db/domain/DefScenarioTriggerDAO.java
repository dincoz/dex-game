package com.dexgdx.game.db.domain;

import com.dexgdx.game.db.annotaion.Column;
import com.dexgdx.game.db.annotaion.Table;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;

@Table(name = "DEF_SCENARIO_TRGGR")
public class DefScenarioTriggerDAO{
	
	@Column(name="_id")
	public Integer id;

	@Column(name = "DEF_ELEMENT_id")
	public Integer defElementDAOId;

	@Column(name = "DEF_SCENARIO_id")
	public Integer defScenarioDAOId;

	public static final Attribute<DefScenarioTriggerDAO, Integer> ID = new SimpleAttribute<DefScenarioTriggerDAO, Integer>(
			"id") {
		@Override
		public Integer getValue(DefScenarioTriggerDAO defScenarioTriggerDAO, QueryOptions arg1) {
			return defScenarioTriggerDAO.id;
		}
	};

	public static final Attribute<DefScenarioTriggerDAO, Integer> DEF_ELEMENT_ID = new SimpleAttribute<DefScenarioTriggerDAO, Integer>(
			"defElementDAOId") {
		@Override
		public Integer getValue(DefScenarioTriggerDAO defScenarioTriggerDAO, QueryOptions arg1) {
			return defScenarioTriggerDAO.defElementDAOId;
		}
	};

	public static final Attribute<DefScenarioTriggerDAO, Integer> DEF_SCENARIO_ID = new SimpleAttribute<DefScenarioTriggerDAO, Integer>(
			"defScenarioDAOId") {
		@Override
		public Integer getValue(DefScenarioTriggerDAO defScenarioTriggerDAO, QueryOptions arg1) {
			return defScenarioTriggerDAO.defScenarioDAOId;
		}
	};
}
