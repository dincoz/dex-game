package com.dexgdx.game.db.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dexgdx.game.*;
import com.dexgdx.game.db.dataaccess.CacheDataAccessor;
import com.dexgdx.game.db.domain.*;
import com.googlecode.cqengine.*;
import com.googlecode.cqengine.index.hash.*;
import com.googlecode.cqengine.query.*;

import static com.googlecode.cqengine.query.QueryFactory.*;

@SuppressWarnings("unused")
public class GameDBObjects {
	
	private Game game;
	
	private List<Class<?>> instDAOClassList;
	
	private IndexedCollection<InstCityDAO> instCityDAOs;
	private IndexedCollection<InstDistrictDAO> instDistrictDAOs;

	public GameDBObjects(Game game) {
		super();
		this.game = game;
		
//		instDAOClassList = new ArrayList<Class<?>>();
//		instDAOClassList.add(InstCityDAO.class);
//		instDAOClassList.add(InstDistrictDAO.class);
//		
//		for (Class<?> daoClass : instDAOClassList) {
//			game.daoHandler.addDBType(daoClass);
//		}
//		
//		instCityDAOs = new ObjectLockingIndexedCollection<InstCityDAO>();
//		instCityDAOs.addIndex(HashIndex.onAttribute(InstCityDAO.GAME_INST_ID));
//		
//		instDistrictDAOs = new ObjectLockingIndexedCollection<InstDistrictDAO>();
//		instDistrictDAOs.addIndex(HashIndex.onAttribute(InstDistrictDAO.GAME_INST_ID));
	}
	
	public void loadGameInst(int gameInstId) {/**
		defElementDAOs = new ObjectLockingIndexedCollection<DefElementDAO>();
		defElementDAOs.addIndex(HashIndex.onAttribute(DefElementDAO.ID));
		daoLoader.loadElements(defElementDAOs, DefElementDAO.class);
		
		for (DefElementDAO defScenarioTriggerDAO : defElementDAOs) {
			System.out.println(defScenarioTriggerDAO.id);
		}

		defScenarioDAOs = new ObjectLockingIndexedCollection<DefScenarioDAO>();
		defScenarioDAOs.addIndex(HashIndex.onAttribute(DefScenarioDAO.ID));
		daoLoader.loadElements(defScenarioDAOs, DefScenarioDAO.class);
		
		for (DefScenarioDAO defScenarioTriggerDAO : defScenarioDAOs) {
			System.out.println(defScenarioTriggerDAO.id);
		}
		
//		Query<DefScenarioTriggerDAO> query = equal(DefScenarioTriggerDAO.DEF_SCENARIO_ID, 10000);
//		for (DefScenarioTriggerDAO defScenarioTriggerDAO : defScenarioTriggerDAOs.retrieve(query)) {
//			Query<DefElementDAO> query2 = equal(DefElementDAO.ID, defScenarioTriggerDAO.defElementDAOId);
//			System.out.println(defElementDAOs.retrieve(query2).uniqueResult().id);
//		}
		**/
//		game.daoHandler.loadElementsByGameInst(instCityDAOs, InstCityDAO.class, gameInstId);
//		game.daoHandler.loadElementsByGameInst(instDistrictDAOs, InstDistrictDAO.class, gameInstId);
		
//		Query<InstCityDAO> query = equal(InstCityDAO.GAME_INST_ID, gameInstId);

//		for (InstCityDAO instCityDAO : instCityDAOs.retrieve(query)) {
//			//System.out.println(defElementDAOs.retrieve(query2).uniqueResult().name);
//			game.message += instCityDAO.id + "\n";
//		}
		
//		CacheDataAccessor a = new CacheDataAccessor(game);
//		a.getElementsByGameInst("InstCityDAO", InstCityDAO.GAME_INST_ID, gameInstId);
//		a.getElementsByGameInst(instCityDAOs, InstCityDAO.GAME_INST_ID, gameInstId);
	}

}
