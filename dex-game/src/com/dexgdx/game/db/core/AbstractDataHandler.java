package com.dexgdx.game.db.core;

public abstract class AbstractDataHandler {

	protected static String database_name = "dex_game";
	protected static AbstractDataHandler instance = null;
	protected static int version = 1;

	public abstract void execute(String sql);
	public abstract int executeUpdate(String sql);
	public abstract Result query(String sql);
	
	public void onCreate() {
	}

	public void onUpgrade() {
	}

	public interface Result {
		
		public boolean isEmpty();

		public boolean moveToNext();

		public int getColumnIndex(String name);

		public int getInt(int columnIndex);

		public float getFloat(int columnIndex);

		public String getString(int columnIndex);

		public Object get(int columnIndex);
	}
}