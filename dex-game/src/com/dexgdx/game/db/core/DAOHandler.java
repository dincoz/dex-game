package com.dexgdx.game.db.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dexgdx.game.Game;
import com.dexgdx.game.db.annotaion.Column;
import com.dexgdx.game.db.annotaion.Table;
import com.dexgdx.game.db.core.AbstractDataHandler.Result;
import com.googlecode.cqengine.IndexedCollection;

public class DAOHandler {
	
	private AbstractDataHandler db;
	private Game game;
	
	private Map<Class<?>, List<Field>> classFields;

	public DAOHandler(AbstractDataHandler dbHandler, Game game) {
		super();
		this.db = dbHandler;
		this.game = game;
		classFields = new HashMap<Class<?>, List<Field>>();
	}
	
	public void addDBType(Class<?> dbObjClass) {
		List<Field> fields = new ArrayList<Field>();
		Class<?> currentClass = dbObjClass;
		while(currentClass != null) {
			for(Field field : currentClass.getDeclaredFields()){
				if(field.isAnnotationPresent(Column.class)) {
					fields.add(field);
				}
			}
			currentClass = currentClass.getSuperclass();
		}
		classFields.put(dbObjClass, fields);
	}
	
	public <T> void loadElementsByGameInst(IndexedCollection<T> dbObjectDAOs, Class<?> dbObjClass, int gameInst){
		Result result = db.query("select * from " + ((Table) dbObjClass.getAnnotation(Table.class)).name() + " where game_inst_id = " + gameInst);
		fillListWithResult(dbObjectDAOs, dbObjClass, result);
	}

	private void setFieldValue(Result result, Object daoObject, Field field)
			throws IllegalAccessException {
		Annotation annotation = field.getAnnotation(Column.class);
		Column defElementDAOColumn = (Column) annotation;
		field.set(daoObject, result.get(result.getColumnIndex(defElementDAOColumn.name())));
	}

	@SuppressWarnings("unchecked")
	private <T> void fillListWithResult(IndexedCollection<T> dbObjectDAOs, Class<?> dbObjClass, Result result) {
		try {
			List<Field> fields = classFields.get(dbObjClass);
			if(fields == null) {
				game.message = dbObjClass.getName() + " dao class not found";
				return;
			}
			while(result.moveToNext()){
				Object daoObject = dbObjClass.getConstructor().newInstance();
				for (Field field : fields) {
					setFieldValue(result, daoObject, field);
				}
				dbObjectDAOs.add((T)daoObject);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
