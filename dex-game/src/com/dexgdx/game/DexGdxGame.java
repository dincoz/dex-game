package com.dexgdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.dexgdx.game.action.AbstractAction;
import com.dexgdx.game.constants.EGamePlatform;
import com.dexgdx.game.constants.EColor;
import com.dexgdx.game.constants.EUIContainer;
import com.dexgdx.game.input.ObjectTouchController;
import com.dexgdx.game.input.CameraPanController;
import com.dexgdx.game.input.CameraRelativePointCalculator;
import com.dexgdx.game.input.DexTrailSwipeController;
import com.dexgdx.game.input.FlingController;
import com.dexgdx.game.input.IFlingable;
import com.dexgdx.game.manager.DrawManager;
import com.dexgdx.game.object.*;
import com.dexgdx.game.ui.button.*;
import com.dexgdx.game.constants.*;
import com.dexgdx.game.db.core.AbstractDataHandler;
import com.dexgdx.game.inventory.*;

public class DexGdxGame extends ApplicationAdapter implements GestureListener, InputProcessor {
	
	private Game game;
	
	private OrthographicCamera camera;
    private ShapeRenderer renderer;
    private SpriteBatch spriteBatch;
    private DrawManager draw;

    private BitmapFont font;
    
    BasePoint camMoveTrajection;
    
    EGamePlatform gamePlatform;
    AbstractDataHandler dbHandler;
    
    // INPUT CONTROLLERS
    private DexTrailSwipeController dexTrailSwipeController;
//    private DexConeSwipeController dexConeSwipeController;
    private ObjectTouchController objectTouchController;
    private CameraRelativePointCalculator camRelativePointCalculator;
    private CameraPanController camPanController;
    private FlingController queueActionFController;
    
	//COMPONENTS
	public SelectionRectangle selectionRectangle;
	
	float zoomRatioTemp;
	
	public DexGdxGame(float zoomRatio, EGamePlatform gamePlatform, AbstractDataHandler dbHandler) {
		super();
		zoomRatioTemp = zoomRatio;
		this.gamePlatform = gamePlatform;
		this.dbHandler = dbHandler;
	}
    
	@Override
	public void create() { 
		game = new Game(dbHandler);
		
		renderer = new ShapeRenderer();
		spriteBatch = new SpriteBatch();
		draw = new DrawManager(spriteBatch, renderer);
		
		game.zoomRatio = zoomRatioTemp;

		camera = new OrthographicCamera();
		camera.setToOrtho(true);
		camera.zoom = game.zoomRatio;
		
		game.uiManager.setCameraAndUI(camera);
		
		queueActionFController = new FlingController((IFlingable)game.ui.container(EUIContainer.QUEUE_ACTIONS));
		
		camMoveTrajection = new BasePoint();
		
	    float westLimit = Gdx.graphics.getWidth() / 2 * game.zoomRatio;
	    float eastLimit = game.currentMap.getMainImage().getRegionWidth() - westLimit;
	    float northLimit = Gdx.graphics.getHeight() / 2 * game.zoomRatio;
	    float southLimit = game.currentMap.getMainImage().getRegionHeight() - northLimit;
		
		font = new BitmapFont(Gdx.files.internal("data/fonts/dialogbox2.fnt"),false);
        font.setColor(Color.WHITE);
        float fontScale = 
//        		1f
        		.3f
        		;
        font.setScale(fontScale, -fontScale);

        dexTrailSwipeController = new DexTrailSwipeController();
//        dexConeSwipeController = new DexConeSwipeController();
        objectTouchController = new ObjectTouchController(game);
        camRelativePointCalculator = new CameraRelativePointCalculator(camera);
        camPanController = new CameraPanController(westLimit, eastLimit, northLimit, southLimit, camera, game.zoomRatio);

    	selectionRectangle = new SelectionRectangle(game);
		
        InputMultiplexer im = new InputMultiplexer();
        GestureDetector gd = new GestureDetector(this);
        setStage(im);
        im.addProcessor(gd);
        im.addProcessor(this);
        Gdx.input.setInputProcessor(im);
        Gdx.graphics.setContinuousRendering(false);
		Gdx.graphics.requestRendering();
		renderer.setProjectionMatrix(camera.combined);
		spriteBatch.setProjectionMatrix(camera.combined);
		
//		dexConeSwipeController.reset(5, game.gameObjects.getPlayerList().get(0));
		
		game.run();

		Gdx.gl.glLineWidth(1.9f / game.zoomRatio);
		Gdx.gl.glClearColor(0, 0, 0, 1);

	}

	private Stage stage ;
//	private Table container;
	
	private void setStage(InputMultiplexer im) {
		
		stage = new Stage();
        im.addProcessor(stage);
        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		Gdx.input.setInputProcessor(stage);

//		container = new Table();
//		container.setFillParent(true);
		
//		stage.addActor(container);

		Table table = new Table();
//		table.setSize(Gdx.graphics.getWidth() * 3 / 4, 100 / game.zoomRatio);

		final ScrollPane scroll = new ScrollPane(table, skin);
		scroll.setSize(Gdx.graphics.getWidth() * 3 / 4, 100 / game.zoomRatio);
		
		for (int i = 0; i < 25; i++) {

			TextButton button = new TextButton(i + "dos", skin);
//			button.setSize(200 / game.zoomRatio, 200 / game.zoomRatio);
//			button.setPosition(0, 0);
			table.add(button);
			button.addListener(new ClickListener() {
				public void clicked (InputEvent event, float x, float y) {
					System.out.println("click " + x + ", " + y);
				}
				
			});


		}
		

//		final TextButton flickButton = new TextButton("Flick Scroll", skin.get("toggle", TextButtonStyle.class));
//		flickButton.setChecked(true);
//		flickButton.addListener(new ChangeListener() {
//			public void changed (ChangeEvent event, Actor actor) {
//				scroll.setFlickScroll(flickButton.isChecked());
//			}
//		});
//
//		final TextButton fadeButton = new TextButton("Fade Scrollbars", skin.get("toggle", TextButtonStyle.class));
//		fadeButton.setChecked(true);
//		fadeButton.addListener(new ChangeListener() {
//			public void changed (ChangeEvent event, Actor actor) {
//				scroll.setFadeScrollBars(fadeButton.isChecked());
//			}
//		});
//
//		final TextButton smoothButton = new TextButton("Smooth Scrolling", skin.get("toggle", TextButtonStyle.class));
//		smoothButton.setChecked(true);
//		smoothButton.addListener(new ChangeListener() {
//			public void changed (ChangeEvent event, Actor actor) {
//				scroll.setSmoothScrolling(smoothButton.isChecked());
//			}
//		});
//
//		final TextButton onTopButton = new TextButton("Scrollbars On Top", skin.get("toggle", TextButtonStyle.class));
//		onTopButton.addListener(new ChangeListener() {
//			public void changed (ChangeEvent event, Actor actor) {
//				scroll.setScrollbarsOnTop(onTopButton.isChecked());
//			}
//		});

//		stage.addActor(scroll);//.expand().fill().colspan(4);
		
//		container.row();//.space(10).padBottom(10);
//		container.add(flickButton).right().expandX();
//		container.add(onTopButton);
//		container.add(smoothButton);
//		container.add(fadeButton).left().expandX();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		camera.position.set(camPanController.screenLimitData.getWestLimit(), camPanController.screenLimitData.getNorthLimit(), 0);
		stage.getViewport().update(width, height, true);
	}
	
	Polygon p;
	
	@Override
	public void render () {
		
//		if(gamePlatform == EGamePlatform.PC)
//			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));
//		else
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		
		if(gamePlatform == EGamePlatform.PC){
			if(camera.position.x + camMoveTrajection.x < camPanController.screenLimitData.getEastLimit()
					&& camera.position.x + camMoveTrajection.x > camPanController.screenLimitData.getWestLimit())
				camera.position.x += camMoveTrajection.x;
			if(camera.position.y + camMoveTrajection.y < camPanController.screenLimitData.getSouthLimit()
					&& camera.position.y + camMoveTrajection.y > camPanController.screenLimitData.getNorthLimit())
				camera.position.y += camMoveTrajection.y;
		}
		camera.update();
		renderer.setProjectionMatrix(camera.combined);
		spriteBatch.setProjectionMatrix(camera.combined);

		spriteBatch.begin();
		spriteBatch.draw(game.currentMap.getMainImage(), 0, 0, game.currentMap.getMainImage().getRegionWidth(), game.currentMap.getMainImage().getRegionHeight());
		spriteBatch.end();
		if(!game.paused)
			game.stateTime += Gdx.graphics.getDeltaTime() * (1 / game.gameFrameRatio);  
    		
		int animIndex = 0;
		int index = 0;
		
		while (index < game.gameObjects.getSolidObjects().size()) {
			SolidObject solidObject = game.gameObjects.getSolidObjects().get(index);
			
			if(!solidObject.isDestroyed()){
				if(game.gameObjects.getMovingObjects().contains(solidObject)){
					MobileObject mobileObject = (MobileObject) solidObject;
					if(!game.paused && mobileObject != null && 
							mobileObject.getMoveTarget() != null &&
							mobileObject instanceof PlayerActor && 
							!(mobileObject.getMoveTarget() instanceof EnemyActor)){
						draw.renderer(ShapeType.Line)
							.moveTarget(EColor.SELECTED_PLAYER, (PlayerActor) mobileObject)
						.end();
					}
					if(mobileObject instanceof AbstractActor){
						AbstractActor actor = (AbstractActor) mobileObject;
						draw.renderer(ShapeType.Line)
							.actorRing(actor)
						.end();
				        
						if(!actor.isInWaitState()){
							if(!game.paused)
								animIndex++;
							actor.loadImageTextureByFrame(animIndex);
						}
						else {
							actor.loadImageTextureByStopFrame();
						}
						draw.spriteBatch()
							.objectWithImage(actor, false)
						.end();
						
						drawActorHealthBar(actor);
					}
					else if(mobileObject instanceof DexMissileObject){
						DexMissileObject dexMissileObject = (DexMissileObject) mobileObject;
						if(dexMissileObject.x != -1){
							spriteBatch.begin();
							dexMissileObject.getPooledEffect().draw(spriteBatch);
							spriteBatch.end();
							if(!game.paused) {
								dexMissileObject.getPooledEffect().update(Gdx.graphics.getDeltaTime() * (1 / game.gameFrameRatio));
							}
						}
						
//						if(dexMissileObject.getPooledEffect().isComplete()){
//							game.getParticleManager().freeToParticleEffectPool(EObjectId.DEX_MISSILE, dexMissileObject.getPooledEffect());
//						}
						
					}
					else if(solidObject instanceof DexTrailObject){
						DexTrailObject dexTrailObject = (DexTrailObject) solidObject;
						if(dexTrailObject.isMoving()){
							//game.message=dexTrailObject.getPooledEffectCount() +"";
							spriteBatch.begin();
							for (int i = 0; i < dexTrailObject.getPooledEffectCount(); i++) {
								if(dexTrailObject.x != -1){
									dexTrailObject.getPooledEffect(i).getPooledEffectMapRel().draw(spriteBatch);
									if(!game.paused){
										dexTrailObject.getPooledEffect(i).getPooledEffectMapRel().update(Gdx.graphics.getDeltaTime() * (1 / game.gameFrameRatio));
									}
//									game.message = Gdx.graphics.getDeltaTime() + "\n" + game.stateTime + "\n" + dexTrailObject.getPooledEffect(i).getPooledEffectMapRel().getEmitters().get(0).duration;
								}
//								if(dexTrailObject.getPooledEffect(i).getPooledEffectMapRel().isComplete()){
//									game.getParticleManager().freeToParticleEffectPool(EObjectId.DEX_TRAIL, dexTrailObject.getPooledEffect(i).getPooledEffectMapRel());
//								}
							}
							spriteBatch.end();
						}
					}
					else if(solidObject instanceof BulletObject){
						BulletObject bulletObject = (BulletObject) solidObject;
						if(bulletObject.trajectory != null) {
							bulletObject.loadImageTextureByStopFrame();
							double atan = Math.atan(bulletObject.trajectory.y/bulletObject.trajectory.x);
							Double alpha = Math.toDegrees(atan);
							if(bulletObject.trajectory.x < 0)
								alpha -= 180;
//							game.message = bulletObject.trajectory.x + "," + bulletObject.trajectory.y + "\n" + atan + "\n" + alpha;
//							draw.renderer(ShapeType.Line)
//								.ring(Color.BLUE, bulletObject)
//							.end();
//							draw.spriteBatch()
//								.objectWithImage(bulletObject, false)
//							.end();
							draw.spriteBatch()
								.objectWithImageRotate(bulletObject, false, alpha.floatValue())
							.end();
						}
					}
				}
				else if(game.gameObjects.getEnemyList().contains(solidObject) || 
						game.gameObjects.getPlayerList().contains(solidObject) || 
						game.gameObjects.getNeutralActorList().contains(solidObject)){
					AbstractActor actor = (AbstractActor) solidObject;
					draw.renderer(ShapeType.Line)
						.actorRing(actor)
					.end();
					if(!actor.isMoving()){
						actor.loadImageTextureByStopFrame();
						draw.spriteBatch()
							.objectWithImage(actor, false)
						.end();
					}
					drawActorHealthBar(actor);
				}
				else {
					draw.renderer(ShapeType.Line)
						.ring(Color.BLUE,solidObject)
					.end();
					solidObject.loadImageTextureByStopFrame();
					draw.spriteBatch()
						.objectWithImage(solidObject, false)
					.end();
				}
			}
			
			index++;
		}

		
		index = 0;
		while (index < game.gameObjects.getPlayerList().size()) {
			PlayerActor actor = game.gameObjects.getPlayerList().get(index);
			if(!actor.isDestroyed()){
				
				if(game.paused && 
						actor != null &&
						actor.getMoveTarget() != null)
					
					draw.renderer(ShapeType.Line)
						.moveTarget(EColor.SELECTED_PLAYER, actor)
					.end();
			}
			index++;
		}
//		index = 0;
//		while (index < game.getEnemyList().size()) {
//			AbstractActor actor = game.getEnemyList().get(index);
//			if(!actor.isDestroyed()){
//				drawActorHealthBar(actor);
//			}
//			index++;
//		}
//		index = 0;
//		while (index < game.getNeutralActorList().size()) {
//			AbstractActor actor = game.getNeutralActorList().get(index);
//			if(!actor.isDestroyed()){
//				drawActorHealthBar(actor);
//			}
//			index++;
//		}
		
		Gdx.gl.glEnable(GL20.GL_BLEND);
        Color c = spriteBatch.getColor();
		spriteBatch.setColor(c.r, c.g, c.b, .6f);
		spriteBatch.begin();
		spriteBatch.draw(game.currentMap.getImageOverlay(1), 0, 0, game.currentMap.getMainImage().getRegionWidth(), game.currentMap.getMainImage().getRegionHeight());
		spriteBatch.end();
		spriteBatch.setColor(c.r, c.g, c.b, 1f);
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
		if(selectionRectangle.visible()){
			renderer.begin(ShapeType.Line);
			renderer.setColor(Color.valueOf(EColor.SELECTION_RECTANGLE.getHexCode()));
			renderer.box(selectionRectangle.x, selectionRectangle.y, 0, selectionRectangle.width, selectionRectangle.height, 0);
			renderer.end();

			Gdx.gl.glEnable(GL20.GL_BLEND);
			renderer.begin(ShapeType.Filled);
			renderer.setColor(Color.valueOf(EColor.SELECTION_RECTANGLE.getHexCode()+"11"));
			renderer.box(selectionRectangle.x, selectionRectangle.y, 0, selectionRectangle.width, selectionRectangle.height, 0);
			renderer.end();
			Gdx.gl.glDisable(GL20.GL_BLEND);
		}
        
        if(dexTrailSwipeController.swiping && game.gameObjects.isAnyPlayerSelected()){
        	draw.alphaEnable().renderer(ShapeType.Line)
        		.swipeLine(game, dexTrailSwipeController)
				.alphaDisable()
        		.spriteBatch()
        		.swipeTarget(game, dexTrailSwipeController, game.ui)
        	.end();
        }
		
//		if(game.containerActorInfo.visible) {
//			draw.alphaEnable().spriteBatch()
//				.uiWithImage(game.containerActorInfo)
//				.uiWithImage(game.containerActorInfo.manIndicator)
//				.uiActorInfoIndicators(game.containerActorInfo)
//			.alphaDisable().end();
//		}
//		
//        if(lastFingerLiftedAfterPinch){
//        	touchDownCounterAfterPinch += Gdx.graphics.getDeltaTime();
//        	if(touchDownCounterAfterPinch > 0.2f){
//        		pinching = false;
//        		lastFingerLiftedAfterPinch = false;
//        		touchDownCounterAfterPinch = 0;
//        	}
//        }
        
        if(!pinching && screenPressed){
        	screenPressedCounter += Gdx.graphics.getDeltaTime();
        	if(screenPressedCounter > 0.5f){
        		screenPressedCounter = 0;
        		screenPressed = false;
        		longPress();
        	}
        }
		
		//userinterface
        draw.alphaEnable().spriteBatch();
        for (int i = 0; i < game.containerPortrait.uiPortraits.size(); i++) {
            draw.uiWithImage(game.containerPortrait.getFrameByIndex(i))
	    		.uiWithImage(game.containerPortrait.getByIndex(i))
	    		.regionImage(game.containerPortrait.getByIndex(i), game.containerPortrait.getDeathOverlayByIndex(i), false);
		}
        draw.alphaDisable().end();
        
//        draw.alphaEnable().spriteBatch()
//	    	.uiWithImage(game.containerPortrait.getFrameByIndex(0))
//	    	.uiWithImage(game.containerPortrait.getByIndex(0))
//	    	.uiWithImage(game.containerPortrait.getFrameByIndex(1))
//	    	.uiWithImage(game.containerPortrait.getByIndex(1))
//        .alphaDisable().end();

		if(!game.dialogBox.active && !game.inventoryBox.isOpen()) {
	        draw.alphaEnable().renderer(ShapeType.Filled)
	        	.uiWithNoImage(game.ui.container(EUIContainer.BOTTOM_ACTIONS))
	        	.uiWithNoImage(game.ui.container(EUIContainer.SIDE_ACTIONS))
		        .uiWithNoImage(game.ui.container(EUIContainer.PAUSE))
		        .uiWithNoImage(game.ui.container(EUIContainer.CENTER_MENU))
	        .spriteBatch()
				.uiWithImage(game.ui.container(EUIContainer.PAUSE).getSingleElement())
				.uiWithImage(game.ui.container(EUIContainer.CENTER_MENU).getSingleElement())
	        .alphaDisable().end();
			
			if(game.gameObjects.getSelectedPlayerList().size() == 1){
				PlayerActor actor = game.gameObjects.getSelectedPlayerList().get(0);
				if(!actor.isDestroyed()){
					game.ui.container(EUIContainer.BOTTOM_ACTIONS).preDraw();
					game.ui.container(EUIContainer.SIDE_ACTIONS).preDraw();
					int a = 0;
					for(EActionType actionType : actor.getPossibleActiveActions()){
						//if(actionType == EActionType.HEAL || actionType == EActionType.GUARD){
				        draw.alphaEnable().spriteBatch()
			        		.uiWithImage(game.ui.container(EUIContainer.BOTTOM_ACTIONS).getByActionType(actionType).withIndex(a))
				        .end().alphaDisable();
						a++;
						//}
					}
					a = 0;
					for(EActionType actionType : actor.getPossiblePassiveActions()){
						//if(actionType == EActionType.HEAL || actionType == EActionType.GUARD){
				        draw.alphaEnable().spriteBatch()
			        		.uiWithImage(game.ui.container(EUIContainer.SIDE_ACTIONS).getByActionType(actionType).withIndex(a))
				        .end().alphaDisable();
						a++;
						//}
					}
					if(actor.getActionRunner().getPotentialActionRunner().getActionList().size() > 0){
						queueActionFController.checkFlingLoop();
						
						float waitTime = 0;
						for (int i = 0; i < actor.getActionRunner().getPotentialActionRunner().getActionList().size(); i++) {
							AbstractAction action = actor.getActionRunner().getPotentialActionRunner().getActionList().get(i);
							if(action.actionType != EActionType.PICKITEM) {
								UIButton currentButton = game.ui.container(EUIContainer.QUEUE_ACTIONS).getByIndexAndAction(i, action.actionType);
							
						        draw.alphaEnable().renderer(ShapeType.Filled)
						       		.uiWithNoImage(currentButton)
						        .end().alphaDisable();
						        
						        draw.spriteBatch()
						        	.uiWithImage(currentButton)
						        .end();
								
								if(i > 0 || actor.getActionRunner().getPotentialActionRunner().waitTime == 0){
									waitTime =  Math.round(action.waitTime / 10);
								}
								else {
									waitTime =  Math.round(actor.getActionRunner().getPotentialActionRunner().waitTime / 10);
								}
								
								float waitRatio = waitTime / (action.waitTime / 10);
								float barTransparency = currentButton.transparency;

								draw.alphaEnable().renderer(ShapeType.Filled)
									.rectangle("000000", currentButton.getRelativeX(), currentButton.getRelativeY() - 7, 
											currentButton.getWidth() , 7, currentButton.transparency)
									.rectangle("0033FF", currentButton.getRelativeX(), currentButton.getRelativeY() - 7, 
											currentButton.getWidth() * waitRatio , 7, barTransparency)
								.end().alphaDisable();
							}
						}
					}
						
				}
				index++;
			}
		}
		else if(game.dialogBox.active){

//	        draw.alphaEnable().renderer(ShapeType.Filled)
//		    	.uiWithNoImage(game.ui.container(EContainer.DIALOG))
//		    .alphaDisable().end();
//	        draw.alphaEnable().spriteBatch()
//		    	.uiDialogBoxText(game.ui.container(EContainer.DIALOG).getSingleElement())
//		    .alphaDisable().end();
	        
	        draw.alphaEnable().spriteBatch()
		    	.uiWithImage(game.ui.container(EUIContainer.DIALOG))
		    	.uiDialogBoxText(game.ui.container(EUIContainer.DIALOG).getSingleElement())
		    .alphaDisable().end();
		}
		else if(game.inventoryBox.isOpen()){
			draw.alphaEnable().spriteBatch()
		    	.uiWithImage(game.ui.container(EUIContainer.INVENTORY))
		    	.uiWithImage(game.ui.container(EUIContainer.INVENTORY).getSingleElement());
			
			for(int i = 0 ; i < Inventory.SIZE ; i++){
				draw.uiWithImage(game.inventoryBox.getByIndex(i));
			}
			draw.alphaDisable().end();
		}

//		if(dexConeSwipeController.swiping) {
//			for (int i = 0; i < dexConeSwipeController.size(); i++) {
//				draw.renderer(ShapeType.Line)
//					.ring(Color.GREEN, dexConeSwipeController.get(i))
//				.end();
//			}
//		}
		spriteBatch.begin();
        font.drawMultiLine(spriteBatch, game.message, 
        		camera.position.x - game.zoomRatio * camera.viewportWidth / 2 + 5, 
        		camera.position.y - game.zoomRatio * camera.viewportHeight / 2 + 10);
//        font.drawMultiLine(spriteBatch, game.message, 
//        		game.ui.container(EContainer.DIALOG).getSingleElement().getRelativeX(), 
//        		game.ui.container(EContainer.DIALOG).getSingleElement().getRelativeY());
        font.setColor(Color.YELLOW);
        font.drawMultiLine(spriteBatch, queueActionFController.message, 
        		camera.position.x - game.zoomRatio * camera.viewportWidth / 2 + 5, 
        		camera.position.y - game.zoomRatio * camera.viewportHeight / 2 + 300);
        spriteBatch.end();

		game.renderIsSynched = true;

		stage.draw();
	}
	
	private void drawActorHealthBar(AbstractActor actor) {
		int height = 2;
		draw.renderer(ShapeType.Filled).
			rectangle("111111", actor.getImageCurrentRegion().x, actor.getImageCurrentRegion().y - height, actor.getImageCurrentRegion().width, height, 1f)
		.end();
		draw.renderer(ShapeType.Filled).
			gradientRectangle(actor.getInitialHitPoint(), actor.getRemainingHitPoint(), actor.getImageCurrentRegion().x, actor.getImageCurrentRegion().y - height, 
					actor.getImageCurrentRegion().width * actor.getRemainingHitPoint() / actor.getInitialHitPoint()
					, height, 1)
		.end();
	}
	
	boolean pinching = false;
	
	boolean screenPressed;
	float screenPressedCounter = 0;
	BasePoint screenPressPosition = new BasePoint();
	
	
	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		
		return false;
	}
	
//	private boolean isUiElementTouched(){
//		
//		
//
//		boolean checkContainer = false;
//		if(game.ui.getTouchedContainer() == game.ui.container(EUIContainer.DIALOG) && game.dialogBox.active){
//			checkContainer = true;
//		}
//		else if(game.ui.getTouchedContainer() == game.ui.container(EUIContainer.INVENTORY) && game.inventoryBox.isOpen()) {
//			checkContainer = true;
//		}
//		else if(!game.dialogBox.active && !game.inventoryBox.isOpen()){
//			checkContainer = true;
//		}
//		if(checkContainer){
//			if(uiContainers.get(containerType).foundTouchedElement(x, y)){
//				contains = true;
//				touchedContainer = uiContainers.get(containerType);
//				break;
//			}
//		}
//		
//		
//		
//		boolean isTouched = 
//			((game.dialogBox.active && game.ui.getTouchedContainer() == game.ui.container(EUIContainer.DIALOG)) || !game.dialogBox.active) ||
//			((game.inventoryBox.isOpen() && game.ui.getTouchedContainer() == game.ui.container(EUIContainer.INVENTORY) || !game.inventoryBox.isOpen()));
//		return isTouched;
//	}
	
	public boolean touchDown(int x, int y, int pointer, int button) {

		if(queueActionFController.isFlinging())
			return false;

		BasePoint relPoint = camRelativePointCalculator.calc(x, y);
		
		screenPressPosition.x = x;
		screenPressPosition.y = y;
		
		if(game.ui.foundTouchedContainer(relPoint.x, relPoint.y)){
			if(game.ui.getTouchedContainer() != null){
				if(game.ui.getTouchedContainer().getTouchedElement() != null){
//					if(isUiElementTouched()) {
						game.ui.getTouchedContainer().getTouchedElement().touchedDown();
//					}
				}
//				if(isUiElementTouched()) {
					game.ui.getTouchedContainer().touchedDown();
//				}
			}
//			if(isUiElementTouched()) {
				game.ui.touchedDown();
				queueActionFController.touchDown(relPoint.y);
//			}
			return false;
		}

		objectTouchController.check(relPoint.x, relPoint.y);
		
//		if(actorTouchController.actorTouched) {
//			dexConeSwipeController.reset(5, actorTouchController.player);
//			dexConeSwipeController.swipe(screenPressPosition.x, screenPressPosition.y);
//		}
		
		screenPressed = true;
		return false;
	}
	
	public boolean touchUp(int x, int y, int pointer, int button) {

		BasePoint relPoint = camRelativePointCalculator.calc(x, y);
		if(queueActionFController.isFlinging()){
			queueActionFController.touchUp();
		}
		else if(dexTrailSwipeController.swiping){
			game.playerSwiped(dexTrailSwipeController, objectTouchController);
			dexTrailSwipeController.swiping = false;
		}
		else if(selectionRectangle.visible()){
			selectionRectangle.setInvisible();
		}
		else {
			if(game.ui.foundTouchedContainer(relPoint.x, relPoint.y) && game.ui.isTouchedDown()){
				if(game.ui.getTouchedContainer() != null && game.ui.getTouchedContainer().isTouchedDown()){
					if(game.ui.getTouchedContainer().getTouchedElement() != null && game.ui.getTouchedContainer().getTouchedElement().isTouchedDown()){
//						if(isUiElementTouched()) {
							game.ui.getTouchedContainer().getTouchedElement().touchedUp();
//						}
					}
//					if(isUiElementTouched()) {
						game.ui.getTouchedContainer().touchedUp();
//					}
				}
//				if(isUiElementTouched()) {
					game.ui.touchedUp();
//				}
				return false;
			}
			if(game.ui.isTouchedDown()){
				if(game.ui.getTouchedContainer() != null && game.ui.getTouchedContainer().isTouchedDown()){
					if(game.ui.getTouchedContainer().getTouchedElement() != null && game.ui.getTouchedContainer().getTouchedElement().isTouchedDown()){
						game.ui.getTouchedContainer().getTouchedElement().reset();
					}
					game.ui.getTouchedContainer().reset();
				}
				game.ui.reset();
			}
		}
		screenPressed = false;
		screenPressedCounter = 0;
		camPanController.panning = false;
		
		return false;
	}
	
	public void longPress() {
		if(game.inventoryBox.isOpen() || game.dialogBox.active) {
			return;
		}
		if(game.ui.isTouchedDown() || queueActionFController.isFlinging()){
			return;
		}
		
		camRelativePointCalculator.calc(screenPressPosition);
		
		if(screenPressPosition.x > 30 && screenPressPosition.y > 30 && !objectTouchController.objectTouched){
			selectionRectangle.x = screenPressPosition.x;
			selectionRectangle.y = screenPressPosition.y;
			selectionRectangle.width = 10;
			selectionRectangle.height = 10;
			selectionRectangle.setVisible(screenPressPosition.x, screenPressPosition.y);
		}
	}

	
	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if(game.dialogBox.active || game.inventoryBox.isOpen() || game.ui.isTouchedDown() || queueActionFController.isFlinging()){
			return false;
		}

		BasePoint relPoint = camRelativePointCalculator.calc(x, y);
		
		screenPressed = false;
		screenPressedCounter = 0;
		
//		int edgeLimit = 110;
		
		boolean screenEdgeTouched = true 
//			screenPressPosition.x < edgeLimit ||
//			screenPressPosition.x > Gdx.graphics.getWidth() - edgeLimit||
//			screenPressPosition.y < edgeLimit ||
//			screenPressPosition.y > Gdx.graphics.getHeight() - edgeLimit - 300
			;
		
		if(selectionRectangle.visible()){
			selectionRectangle.resize(relPoint);
		}
		else if(objectTouchController.player != null){
			if(!game.gameObjects.getSelectedPlayerList().contains(objectTouchController.player)){
				game.playerSelected(objectTouchController.player);
			}
//			for(PlayerActor player : game.gameObjects.getSelectedPlayerList()) {
//				if(player.getPossibleActions().contains(EActionType.TRAIL)){
					dexTrailSwipeController.swipe(objectTouchController.player, relPoint.x, relPoint.y);
//				}
//			}
		}
		else if(screenEdgeTouched){
			//game.message=Gdx.graphics.getWidth()+"\n"+screenPressPosition.x+"\n"+screenPressPosition.y;
			camPanController.pan(x, y, screenPressPosition);
		}
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		if(game.dialogBox.active || game.inventoryBox.isOpen() || game.ui.isTouchedDown() || queueActionFController.isFlinging()){
			return false;
		}
		
		BasePoint relPoint = camRelativePointCalculator.calc(x, y);
		
		if(objectTouchController.objectTouched){
			if(objectTouchController.player != null){
				game.playerSelected(objectTouchController.player);
			}
			else if(objectTouchController.enemy != null){
				game.enemySelected(objectTouchController.enemy);
			}
			else if(objectTouchController.neutralActor != null){
				game.neutralActorSelected(objectTouchController.neutralActor);
			}
			else if(objectTouchController.otherObject != null){
				game.otherObjectSelected(objectTouchController.otherObject);
			}
		}
		else {
			game.emptyScreenTouched(relPoint.x, relPoint.y);
		}
		
		return false;
	}
	
	
	@Override
	public boolean keyDown(int keycode) {
		if(game.inventoryBox.isOpen() || game.dialogBox.active) {
			return false;
		}
		if(keycode == Input.Keys.UP && camera.position.y > 9){
			camMoveTrajection.y = -10;
		}
		else if(keycode == Input.Keys.DOWN){
			camMoveTrajection.y = 10;
		}
		if(keycode == Input.Keys.LEFT && camera.position.x > 9){
			camMoveTrajection.x = -10;
		}
		else if(keycode == Input.Keys.RIGHT){
			camMoveTrajection.x = 10;
		}
		else if(keycode == Input.Keys.SPACE){
			game.togglePause();
		}
		camera.update();
		return false;
	}
	
	@Override
	public boolean keyUp(int keycode) {
		if(game.inventoryBox.isOpen() || game.dialogBox.active) {
			return false;
		}
		camMoveTrajection.y = 0;
		camMoveTrajection.x = 0;
		return false;
	}
	
	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		if(game.inventoryBox.isOpen() || game.dialogBox.active) {
			return false;
		}
		if(queueActionFController.isFlinging()){
			queueActionFController.fling(velocityY * game.zoomRatio);
		}
		return false;
	}
	
	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		if(game.inventoryBox.isOpen() || game.dialogBox.active) {
			return false;
		}
		BasePoint relPoint = camRelativePointCalculator.calc(x, y);
		if(queueActionFController.isFlinging()){
			queueActionFController.touchDragged(relPoint.y);
			return false;
		}
//		if(dexConeSwipeController.swiping){
//			dexConeSwipeController.swipe(relPoint.x, relPoint.y);
//			return false;
//		}
		return false;
	}



	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}
	
	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean longPress(float x, float y) {
		return false;
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stu
		if(game.inventoryBox.isOpen()){
			game.inventoryBox.close();
		}
		else{
			super.dispose();
			stage.dispose();
			game.getAssetManager().clear();
		}
		
	}
	
}
