package com.dexgdx.game.input;

import com.dexgdx.game.object.BasePoint;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.PlayerActor;

public class DexTrailSwipeController {

	public boolean swiping;
	public BaseRegion swipingPlayer;
	public BaseRegion swipingPlayerFirstSwipePosition;
	public BasePoint endPoint;
//	public BasePoint swipingPlayerStartPoint;
	
	private BasePoint tempCenterPoint;
	
	public DexTrailSwipeController() {
		super();
		endPoint = new BasePoint();
	}

	public void swipe(PlayerActor swipingPlayer, float x, float y){
		if(!swiping){
			this.swipingPlayer = swipingPlayer;
			this.swipingPlayerFirstSwipePosition = swipingPlayer.cloneRegion();
//			this.swipingPlayerStartPoint = this.swipingPlayer.getCenterPoint();
		}
		tempCenterPoint = this.swipingPlayerFirstSwipePosition.getCenterPoint();
		endPoint.x = tempCenterPoint.x * 2 - x + (swipingPlayer.x - swipingPlayerFirstSwipePosition.x);
		endPoint.y = tempCenterPoint.y * 2 - y + (swipingPlayer.y - swipingPlayerFirstSwipePosition.y);
		swiping = true;
	}
}
