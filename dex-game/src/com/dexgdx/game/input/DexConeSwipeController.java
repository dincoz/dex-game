package com.dexgdx.game.input;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.BaseRegion;

public class DexConeSwipeController {

	public boolean swiping;
	public AbstractActor swipingActor;
	private List<BaseRegion> coneRings;

	private double radius = 100;
	private double piRadian = Math.PI / 180;
	
	private double xT, yT, ratio, degree, coneX, coneY;
	
	public DexConeSwipeController() {
		super();
		coneRings = new ArrayList<BaseRegion>();
	}
	
	public void reset(int size, AbstractActor swipingActor) {
		if(swipingActor != null) {
			this.swipingActor = swipingActor;
			coneRings.clear();
			for (int i = 0; i < size; i++) {
				coneRings.add(swipingActor.cloneRegion());
			}
		}
	}

	public void swipe(float x, float y){
		swiping = true;
		xT = x - swipingActor.getCenterPoint().x;
		yT = y - swipingActor.getCenterPoint().y;
		ratio = yT / xT;
		degree = Math.atan(ratio) * (1 / piRadian);
		if(xT <= 0)
			degree += 180;
		int half = (coneRings.size() - 1) / 2;
		int index = 0;
		for (double i = -1 * half; i < coneRings.size() - half; i++) {
			coneX = swipingActor.getCenterPoint().x + radius * Math.cos((degree + (10*i)) * piRadian);
			coneY = (swipingActor.getCenterPoint().y + radius * Math.sin((degree + (10*i)) * piRadian) * 4/5);
			coneRings.get(index).x = (float)coneX - swipingActor.width / 2;
			coneRings.get(index).y = (float)coneY - swipingActor.height / 2;
			index++;
		}
	}
	
	public int size() {
		return coneRings.size();
	}
	
	public BaseRegion get(int index) {
		return coneRings.get(index);
	}
}
