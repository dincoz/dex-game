package com.dexgdx.game.input;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.EnemyActor;
import com.dexgdx.game.object.NeutralActor;
import com.dexgdx.game.object.PlayerActor;
import com.dexgdx.game.object.SolidObject;

public class ObjectTouchController {
	
	private Game game;

	public boolean objectTouched;
	public EnemyActor enemy;
	public PlayerActor player;
	public NeutralActor neutralActor;
	public SolidObject otherObject;
	
	public ObjectTouchController(Game game) {
		super();
		this.game = game;
	}

	public void check(float x, float y){
		setNoTouch();
		for (PlayerActor playerActor : game.gameObjects.getPlayerList()) {
			if(!playerActor.isDestroyed() && playerActor.contains(x, y)){
				setObject(playerActor);
				break;
			}
		}
		if(!objectTouched){
			for (EnemyActor enemyActor : game.gameObjects.getEnemyList()) {
				if(!enemyActor.isDestroyed() && enemyActor.contains(x, y)){
					setObject(enemyActor);
					break;
				}
			}
		}
		if(!objectTouched){
			for (NeutralActor neutralActor : game.gameObjects.getNeutralActorList()) {
				if(!neutralActor.isDestroyed() && neutralActor.contains(x, y)){
					setObject(neutralActor);
					break;
				}
			}
		}
		if(!objectTouched){
			for (SolidObject solidObject : game.gameObjects.getOtherSceneObjects()) {
				if(!solidObject.isDestroyed() && solidObject.contains(x, y)){
					setObject(solidObject);
					break;
				}
			}
		}
		if(!objectTouched){
			for (PlayerActor playerActor : game.gameObjects.getPlayerList()) {
				if(!playerActor.isDestroyed() && playerActor.getTouchRegion().contains(x, y)){
					setObject(playerActor);
					break;
				}
			}
			if(!objectTouched){
				for (EnemyActor enemyActor : game.gameObjects.getEnemyList()) {
					if(!enemyActor.isDestroyed() && enemyActor.getTouchRegion().contains(x, y)){
						setObject(enemyActor);
						break;
					}
				}
			}
			if(!objectTouched){
				for (NeutralActor neutralActor : game.gameObjects.getNeutralActorList()) {
					if(!neutralActor.isDestroyed() && neutralActor.getTouchRegion().contains(x, y)){
						setObject(neutralActor);
						break;
					}
				}
			}
		}
	}
	
	private void setObject(PlayerActor player){
		this.player = player;
		this.enemy = null;
		this.neutralActor = null;
		this.otherObject = null;
		objectTouched = true;
	}
	
	private void setObject(EnemyActor enemy){
		this.enemy = enemy;
		this.player = null;
		this.neutralActor = null;
		this.otherObject = null;
		objectTouched = true;
	}
	
	private void setObject(NeutralActor neutralActor){
		this.enemy = null;
		this.player = null;
		this.neutralActor = neutralActor;
		this.otherObject = null;
		objectTouched = true;
	}
	
	private void setObject(SolidObject otherObject){
		this.enemy = null;
		this.player = null;
		this.neutralActor = null;
		this.otherObject = otherObject;
		objectTouched = true;
	}
	
	private void setNoTouch(){
		this.enemy = null;
		this.player = null;
		this.neutralActor = null;
		this.otherObject = null;
		objectTouched = false;
	}
}
