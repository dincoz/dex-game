package com.dexgdx.game.input;

public interface IFlingable {

	int size();
	float initY();
	float y(int index);
	float x(int index);
	float transparency(int index);
	int selectedIndex();
	boolean isStarted();

	void lowerX(int index, float amount);
	void raiseY(int index, float amount);
	void setTransparency(int index, float transparency);
	void complete(int index);
	void reset(int index);
}
