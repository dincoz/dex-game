package com.dexgdx.game.input;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.dexgdx.game.object.BasePoint;

public class CameraRelativePointCalculator {
	
	private OrthographicCamera camera;
	private Vector3 relativePointVector;
	private BasePoint relativePoint;

	public CameraRelativePointCalculator(OrthographicCamera camera) {
		this.camera = camera;
		
		relativePointVector = new Vector3();
		relativePoint = new BasePoint();
	}

	public void calc(BasePoint point){
		calc(point.x, point.y);
		point.x = relativePoint.x;
		point.y = relativePoint.y;
	}
	
	public BasePoint calc(float x, float y){
		relativePointVector.x = x;
		relativePointVector.y = y;
		relativePointVector = camera.unproject(relativePointVector);
		relativePoint.x = relativePointVector.x;
		relativePoint.y = relativePointVector.y;
		return relativePoint;
	}
}
