package com.dexgdx.game.input;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.object.BasePoint;

public class CameraPanController {
	
	public ScreenLimitData screenLimitData;
	private OrthographicCamera camera;
	
	public boolean panning;

	private BasePoint deltaPosition;
	private BasePoint currentCamPosition;
	private float zoomRatio;

	public CameraPanController(float westLimit, float eastLimit, float northLimit, float southLimit, OrthographicCamera camera, float zoomRatio) {
		this.camera = camera;
		this.screenLimitData = new ScreenLimitData(westLimit, eastLimit, northLimit, southLimit);
		this.zoomRatio = zoomRatio;
		
		deltaPosition = new BasePoint();		
		currentCamPosition = new BasePoint();
	}

	public void pan(float x, float y, BasePoint screenPressPosition){

		if(!panning){
			currentCamPosition.setFromVector(camera.position);
		}
		panning = true;
		
		deltaPosition.set((screenPressPosition.x - x) * zoomRatio,
							(screenPressPosition.y - y) * zoomRatio);
		
		if(		currentCamPosition.x + deltaPosition.x < screenLimitData.getEastLimit() 
			&& 	currentCamPosition.x + deltaPosition.x > screenLimitData.getWestLimit())
			camera.position.x = currentCamPosition.x + deltaPosition.x;
		if(		currentCamPosition.y + deltaPosition.y < screenLimitData.getSouthLimit() 
			&& 	currentCamPosition.y + deltaPosition.y > screenLimitData.getNorthLimit())
			camera.position.y = currentCamPosition.y + deltaPosition.y;
		camera.update();
	}
	
	public class ScreenLimitData {

	    private float westLimit;
	    private float eastLimit;
	    private float northLimit;
	    private float southLimit;
	    
		public ScreenLimitData(float westLimit, float eastLimit, float northLimit,
				float southLimit) {
			super();
			this.westLimit = westLimit;
			this.eastLimit = eastLimit;
			this.northLimit = northLimit;
			this.southLimit = southLimit;
		}

		public float getWestLimit() {
			return westLimit;
		}

		public float getEastLimit() {
			return eastLimit;
		}

		public float getNorthLimit() {
			return northLimit;
		}

		public float getSouthLimit() {
			return southLimit;
		}
	    
	    
	}
}
