package com.dexgdx.game.input;

import com.badlogic.gdx.Gdx;

public class FlingController {

	private IFlingable flinger;
	private float velocity;
	private float dragPositionY;
	private float firstTouchY;
	private float initY;
	private boolean completed;
	private boolean transfering;
	private float[] tempXArray;
	
	public String message = "";
	
	public FlingController(IFlingable flinger) {
		super();
		this.flinger = flinger;
		transfering = false;
		completed = true;
		velocity = 0;
		dragPositionY = 0;
	}
	
	private void resetFling(){
		velocity = 0;
		dragPositionY = 0;
		flinger.reset(flinger.selectedIndex());
		transfering = false;
		completed = true;
	}

	public void checkFlingLoop(){
		if(!completed){
			initY = flinger.initY();
			reCalcR();
			if(transfering)
				transfer();
			Gdx.graphics.requestRendering();
		}
	}
	
	public boolean isFlinging(){
		return !completed;
	}
	
	public void touchDown(float y){
		if(completed && flinger.isStarted()){
			firstTouchY = y;
			completed = false;
		}
	}
	
	public void touchUp(){
		if(velocity == 0){
			resetFling();
		}
	}
	
	public void touchDragged(float y){
		float diff = (initY - firstTouchY);
		if(initY >= y + diff){
			dragPositionY = y + diff;
		}
		else{
			dragPositionY = initY;
		}
		velocity = 0;
		setY(dragPositionY);
			
			float k = (1 - ((rowY() - initY) * -1) / 100);
			setTransparency(k < 0 ? 0 : (k > 1 ? 1 : k));
		
		if(rowTransparency() == 0 && dragPositionY != 0){
			transfering = true;
		}
	}
	
	public void fling(float velocity){
		this.velocity = velocity;
	}
	
	private void transfer(){
		transfering = false;
		if(tempXArray == null || tempXArray.length != flinger.size()){
			setupTempXArray();
		}
		for(int i = flinger.size() - 1 ; i > flinger.selectedIndex() ; --i){
			flinger.lowerX(i, 10);
			if(flinger.x(i) < tempXArray[i - 1]){
				flinger.lowerX(i, 1);
			}
			else {
				transfering = true;
			}
		}
		if(!transfering){
			flinger.complete(flinger.selectedIndex());
			resetTempXArray();
			completed = true;
		}
	}
	
	private void reCalcR(){
		if(completed)
			return;
		
		float altVelocity = velocity / 150;
//		
//		if(altVelocity > 0 || (altVelocity > -1 && altVelocity < 0)){
//			resetFling();
//			return;
//		}
//		else if(altVelocity < -3){
//			altVelocity = -3;
//		}
		if(altVelocity > 0){
			resetFling();
			return;
		}
		if(altVelocity != 0){
			altVelocity = -5;
			dragPositionY = 0;
		}
		if(rowTransparency() >= 0 && rowTransparency() <= 1){
			if(altVelocity != 0 && initY > rowY() + altVelocity){
				raiseY(altVelocity);
			}
			
			float k = (1 - ((rowY() - initY) * -1) / 100);
			setTransparency(k < 0 ? 0 : (k > 1 ? 1 : k));
		}
		if(rowTransparency() == 0 && altVelocity != 0){
			transfering = true;
		}
	}
	
	private void setupTempXArray(){
		tempXArray = new float[flinger.size()];
		for(int i = flinger.size() - 1 ; i > flinger.selectedIndex() - 1 ; --i){
			tempXArray[i] = flinger.x(i);
		}
	}
	
	private void resetTempXArray(){
		tempXArray = null;
	}
	
	private float rowTransparency(){
		return flinger.transparency(flinger.selectedIndex());
	}
	
	private void setTransparency(float transparency){
		flinger.setTransparency(flinger.selectedIndex(), transparency);
	}
	
	public float rowY(){
		return flinger.y(flinger.selectedIndex());
	}
	
	private void raiseY(float amount){
		flinger.raiseY(flinger.selectedIndex(), amount);
	}
	
	private void setY(float amount){
		flinger.raiseY(flinger.selectedIndex(), amount - rowY());
	}
}
