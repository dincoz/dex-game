package com.dexgdx.game.constants;

public enum EUIContainer {
	BOTTOM_ACTIONS,
	SIDE_ACTIONS,
	PAUSE,
	QUEUE_ACTIONS,
	DIALOG,
	ACTOR_INFO,
	PORTRAIT,
	CENTER_MENU,
	INVENTORY
}
