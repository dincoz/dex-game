package com.dexgdx.game.constants;

public enum EParticleType {
	TRAIL,
	MISSILE
}
