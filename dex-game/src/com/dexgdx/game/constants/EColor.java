package com.dexgdx.game.constants;

public enum EColor {

	UNSELECTED_PLAYER("00FF00"),
	SELECTION_RECTANGLE("00FF00"),
	SELECTED_PLAYER("FFFFFF"), // #9900FF guzel mor
	UNSELECTED_ENEMY("FF0000"),
	SELECTED_ENEMY("FF0000"),
	BEING_TARGET("CCCCCC"),
//	BEING_TARGET("0000FF"),
	PATH_NODE("5C0099"),
	NEUTRAL_ACTOR("00FFFF");
	
	String hexCode;

	private EColor(String hexCode) {
		this.hexCode = hexCode;
	}
	
	public String getHexCode(){
		return hexCode;
	}
}
