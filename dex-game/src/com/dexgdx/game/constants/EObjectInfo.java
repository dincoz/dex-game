package com.dexgdx.game.constants;

import com.dexgdx.game.util.ObjectInfo;

public enum EObjectInfo {

	TIM(100, "Tim"),
	BRO1(101, "Bro1"),
	TREE(102, "Tree"),
	MARY(103, "Mary"),
	GRIDLOCK(104, "Gridlock"),
	ORC(105, "Orc"),
	BRO2(106, "Bro2"),
	
	DEX_MISSILE(200, "Dex Missile"),
	DEX_TRAIL(201, "Dex Trail"),
	BULLET(202, "Bullet"),
	
	MAP_FOREST_1(300, "Map Forest 1"),
	
	SWORD(400, "Sword");
	
	private ObjectInfo objectInfo;
	
	private EObjectInfo(int id, String name){
		objectInfo = new ObjectInfo(id, name);
	}

	public ObjectInfo getObjectInfo() {
		return objectInfo;
	}
	
}
