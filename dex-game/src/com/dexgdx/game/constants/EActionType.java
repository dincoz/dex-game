package com.dexgdx.game.constants;

public enum EActionType {
	
	MISSILE(true),
	TRAIL(true),
	HEAL(false),
	CRUSH(true),
	GUARD(false),
	DIALOG(false),
	BULLET(true),
	PICKITEM(false),
	OTHER(false);
	
	private boolean activeAction;
	
	private EActionType(boolean activeAction) {
		this.activeAction = activeAction;
	}
	
	public boolean isActiveAction() {
		return activeAction;
	}
}
