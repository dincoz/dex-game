package com.dexgdx.game.constants;

public enum EImagePath {

	SWORD_1(EImagePath.BASE_PATH + "sword.png"),
	INVENTORY_SWORD_1(EImagePath.BASE_PATH + "inventory_sword.png"),
	
	FOREST_MAP(EImagePath.MAP_PATH + "forest_map2.jpg"),
	FOREST_MAP_OVERLAY(EImagePath.MAP_PATH + "forest_map2_overlay.png"),
	
	DEX_MISSILE(EImagePath.BUTTON_PATH + "dex_missile.png"),
	DEX_TRAIL(EImagePath.BUTTON_PATH + "dex_trail.png"),
	BULLET(EImagePath.BUTTON_PATH + "bullet.png"),
	DEX_FLAME(EImagePath.BUTTON_PATH + "dex_flame.png"),
	DEX_HEAL_ON(EImagePath.BUTTON_PATH + "dex_heal_on.png"),
	DEX_HEAL_OFF(EImagePath.BUTTON_PATH + "dex_heal_off.png"),
	DEX_CRUSH(EImagePath.BUTTON_PATH + "dex_crush.png"),
	DEX_GUARD(EImagePath.BUTTON_PATH + "dex_guard.png"),
	
	PAUSE(EImagePath.BUTTON_PATH + "pause.png"),
	UNPAUSE(EImagePath.BUTTON_PATH + "unpause.png"),

	INVENTORY(EImagePath.BUTTON_PATH + "inventory.png"),
	
	CONTAINER_ACTION_BUTTON(EImagePath.CONTAINER_PATH + "action_button_container.png"),
	CONTAINER_ACTION_BUTTON_TOGGLE(EImagePath.CONTAINER_PATH + "toggle_action_button_container.png"),
	CONTAINER_DIALOG(EImagePath.CONTAINER_PATH + "dialog_container.png"),
	CONTAINER_INVENTORY(EImagePath.CONTAINER_PATH + "inventory_container.png"),
	CONTAINER_PAUSE(EImagePath.CONTAINER_PATH + "pause_container.png"),
	CONTAINER_INVENTORY_BUTTON(EImagePath.CONTAINER_PATH + "inventory_button_container.png"),
	CONTAINER_ACTOR_INFO(EImagePath.INDICATOR_PATH + "actor_info_container.png"),
	
	DIALOG(EImagePath.BUTTON_PATH + "dialog.png"),
	
	DISPLAY_ACTOR_INFO_MAN(EImagePath.INDICATOR_PATH + "man.png"),
	DISPLAY_ACTOR_INFO_BLOOD(EImagePath.INDICATOR_PATH + "blood.jpg"),
	DISPLAY_ACTOR_INFO_BLOOD_START(EImagePath.INDICATOR_PATH + "blood_start.jpg"),
	DISPLAY_ACTOR_INFO_BLOOD_END(EImagePath.INDICATOR_PATH + "blood_end.jpg"),
	DISPLAY_ACTOR_INFO_BLOOD_TUBE(EImagePath.INDICATOR_PATH + "blood_tube.jpg"),
	DISPLAY_ACTOR_INFO_DISEASE(EImagePath.INDICATOR_PATH + "disease.jpg"),
	DISPLAY_ACTOR_INFO_DISEASE_START(EImagePath.INDICATOR_PATH + "disease_start.jpg"),
	DISPLAY_ACTOR_INFO_DISEASE_END(EImagePath.INDICATOR_PATH + "disease_end.jpg"),
	DISPLAY_ACTOR_INFO_DISEASE_TUBE(EImagePath.INDICATOR_PATH + "disease_tube.jpg"),

	PORTRAIT_BRO1(EImagePath.PORTRAIT_PATH + "bro.png"),
	PORTRAIT_BRO2(EImagePath.PORTRAIT_PATH + "bro2.png"),
	PORTRAIT_SIS(EImagePath.PORTRAIT_PATH + "sis.png"),
	PORTRAIT_NONE(EImagePath.PORTRAIT_PATH + "none.png"),
	PORTRAIT_SELECTED(EImagePath.PORTRAIT_PATH + "selected_container.png"),
	PORTRAIT_UNSELECTED(EImagePath.PORTRAIT_PATH + "unselected_container.png"),
	DEATH_OVERLAY(EImagePath.PORTRAIT_PATH + "dead_overlay.png");

	public String path;

	private static final String BASE_PATH = "data/objects/baseobjects/";
	private static final String MAP_PATH = "data/maps/";
	private static final String BUTTON_PATH = "data/objects/ui/button/";
	private static final String CONTAINER_PATH = "data/objects/ui/container/";
	private static final String INDICATOR_PATH = "data/objects/ui/indicators/";
	private static final String PORTRAIT_PATH = "data/objects/ui/portraits/";
	
	private EImagePath(String path) {
		this.path = path;
	}
}
