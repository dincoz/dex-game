package com.dexgdx.game.constants;

public enum EPlayerType {
	ENEMY, PLAYER, NEUTRAL;
}
