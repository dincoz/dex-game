package com.dexgdx.game.constants;

public enum EMoveDirection {
	
	SOUTH(1), 
	SOUTH_WEST(2), 
	SOUTH_WEST_2(9), 
	SOUTH_WEST_3(10), 
	WEST(3),
	NORTH_WEST(4),
	NORTH_WEST_2(11), 
	NORTH_WEST_3(12), 
	NORTH(5),
	NORTH_EAST(6),
	NORTH_EAST_2(13),
	NORTH_EAST_3(14), 
	EAST(7),
	SOUTH_EAST(8),
	SOUTH_EAST_2(15),
	SOUTH_EAST_3(16) ;
	
	public Integer index;
	
	private EMoveDirection(int i){
		index = new Integer(i);
	}
	
	public EMoveDirection getOpposite(){
		EMoveDirection opposite = null;
		
		if(this == SOUTH)
			opposite = NORTH;
		else if(this == NORTH)
			opposite = SOUTH;
		
		else if(this == WEST)
			opposite = EAST;
		else if(this == EAST)
			opposite = WEST;
		
		else if(this == SOUTH_WEST)
			opposite = NORTH_EAST;
		else if(this == NORTH_EAST)
			opposite = SOUTH_WEST;
		
		else if(this == SOUTH_EAST)
			opposite = NORTH_WEST;
		else if(this == NORTH_WEST)
			opposite = SOUTH_EAST;

		else if(this == SOUTH_WEST_2)
			opposite = NORTH_EAST_3;
		else if(this == NORTH_EAST_3)
			opposite = SOUTH_WEST_2;

		else if(this == SOUTH_EAST_2)
			opposite = NORTH_WEST_3;
		else if(this == NORTH_WEST_3)
			opposite = SOUTH_EAST_2;

		else if(this == SOUTH_WEST_3)
			opposite = NORTH_EAST_2;
		else if(this == NORTH_EAST_2)
			opposite = SOUTH_WEST_3;

		else if(this == SOUTH_EAST_3)
			opposite = NORTH_WEST_2;
		else if(this == NORTH_WEST_2)
			opposite = SOUTH_EAST_3;
		
		return opposite;
	}
}
