package com.dexgdx.game.constants;

public enum EMoveStopCondition {
	WHEN_TARGET_TOUCHED,
	WHEN_OVER,
	WHEN_STEPCOUNT_REACHED,
	WHEN_TOUCHED_OR_STEPCOUNT_REACHED
}
