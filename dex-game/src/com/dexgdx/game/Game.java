package com.dexgdx.game;

import com.badlogic.gdx.assets.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.dexgdx.game.action.*;
import com.dexgdx.game.animation.*;
import com.dexgdx.game.constants.*;
import com.dexgdx.game.db.*;
import com.dexgdx.game.db.core.AbstractDataHandler;
import com.dexgdx.game.db.core.DAOHandler;
import com.dexgdx.game.db.domain.GameDBObjects;
import com.dexgdx.game.initializer.*;
import com.dexgdx.game.input.*;
import com.dexgdx.game.json.*;
import com.dexgdx.game.manager.*;
import com.dexgdx.game.object.*;
import com.dexgdx.game.ui.*;
import com.dexgdx.game.ui.container.*;
import com.dexgdx.game.ui.dialog.*;
import com.dexgdx.game.util.*;

import java.util.*;

public class Game {

	public GameObjects gameObjects;
	
	public SceneMap currentMap;
	
	//MANAGER
	public JSONManager jsonManager;
	private ActionManager actionManager;
	private AnimationManager animationManager;
	private ParticleManager particleManager;
	public UIManager uiManager;
	
	public DAOHandler daoHandler;
	public UIDialogBox dialogBox;
	public UIContainerInventory inventoryBox;
//	public UIContainerActorInfo containerActorInfo;
	public UIContainerPortrait containerPortrait;
	
	private AssetManager assetManager;
	
	private Map<Integer, DexAnimation> animations;

    public UIGame ui;
	
	//FACTORY
	private ObjectFactory objectFactory;
	private List<RegionActionTrigger> regionActionTriggers;

	private ParticleEffectPool dexMissileParticleEffectPool;
	private ParticleEffectPool dexTrailParticleEffectPool;

	//UTILS
	public DefaultConstants defaultConstants;
	public Map<EActionType, TextureRegion> defaultImageOfAction;
	public YPositionComparator yPositionComparator;
	
	private Timer gameTimer;
	public int gameSpeed;
    public float stateTime;
    public float gameFrameRatio = 1;
    public boolean paused;
    public boolean slowPaused;
    public boolean renderIsSynched;
    public UIElement touchedUIElement;
    
    public float zoomRatio;

    // DEBUG
	public String message = "";
	
	public Game(AbstractDataHandler dbHandler){
//		daoHandler = new DAOHandler(dbHandler, this);
		loadGameData();
	}

	public void setRegionActionTriggers(List<RegionActionTrigger> regionActionTriggers){
		this.regionActionTriggers = regionActionTriggers;
	}

	public List<RegionActionTrigger> getRegionActionTriggers(){
		return regionActionTriggers;
	}
	
	public void run(){
		startTimer();
	}
	
	private void loadGameData(){
		
		gameObjects = new GameObjects();
		
		regionActionTriggers = new ArrayList<RegionActionTrigger>();

		jsonManager = new JSONManager(this);
		animationManager = new AnimationManager(this);
		particleManager = new ParticleManager(this);
		actionManager = new ActionManager(this);
		uiManager = new UIManager(this);
		
		assetManager = new AssetManager();
		for(EImagePath path : EImagePath.values()) {
			assetManager.load(path.path, Texture.class);
		}
		assetManager.finishLoading();
		
		objectFactory = new ObjectFactory(this);
	    animations = new HashMap<Integer, DexAnimation>();

		defaultImageOfAction = new HashMap<EActionType, TextureRegion>();
		defaultImageOfAction.put(EActionType.HEAL, getTexture(EImagePath.DEX_HEAL_OFF));
		defaultImageOfAction.put(EActionType.MISSILE, getTexture(EImagePath.DEX_MISSILE));
		defaultImageOfAction.put(EActionType.TRAIL, getTexture(EImagePath.DEX_TRAIL));
		defaultImageOfAction.put(EActionType.BULLET, getTexture(EImagePath.BULLET));
		defaultImageOfAction.put(EActionType.CRUSH, getTexture(EImagePath.DEX_CRUSH));
		defaultImageOfAction.put(EActionType.GUARD, getTexture(EImagePath.DEX_GUARD));
		defaultImageOfAction.put(EActionType.DIALOG, getTexture(EImagePath.DIALOG));
		
		defaultConstants = new DefaultConstants(45, 36, 1.5f, 45, 36, 2f, 45, 36, 10, 45, 36, 4.5f);
		yPositionComparator = new YPositionComparator();

		particleManager.loadParticleTextures();
		
		//TODO: (TEST) json a yazar, hazir json olunca bu kalkacak
		jsonManager.saveJSONActors();
		jsonManager.saveJSONAnimationObjects();
		jsonManager.saveJSONMap();
		jsonManager.saveJSONInventoryItems();
		//TODO
		
		jsonManager.loadActorsFromJSON();
		jsonManager.loadAnimationsFromJSON();
		jsonManager.loadMapsFromJSON();
		jsonManager.loadInventoryItemsFromJSON();
		
		GameDBObjects gameDBObjects = new GameDBObjects(this);
		gameDBObjects.loadGameInst(1);
		
	}

	private void startTimer(){
		gameTimer = new Timer();
		gameSpeed = 10;
		gameTimer.scheduleAtFixedRate(new GameTimer(this, actionManager), 0, gameSpeed);
	}
	
	public void gameSpeed(int speed){
		gameTimer.cancel();
		gameTimer = new Timer();
		gameSpeed = speed;
		gameTimer.scheduleAtFixedRate(new GameTimer(this, actionManager), 0, gameSpeed);
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}
	
	public TextureRegion getTexture(EImagePath imagePath){
		if(imagePath != null) {
			TextureRegion image = new TextureRegion(assetManager.get(imagePath.path, Texture.class));
			image.flip(false, true);
			return image;
		}
		return null;
	}
	
	public void playerSelected(PlayerActor playerActor){
		gameObjects.selectSinglePlayer(playerActor);
	}
	
	public void enemySelected(EnemyActor enemyActor){
		if(gameObjects.isAnyPlayerSelected()){
			for(PlayerActor player : gameObjects.getSelectedPlayerList()){
				if(player.selectedActiveAttackAction == EActionType.MISSILE){
					player.dexMissileAttack(enemyActor);
				}
				else if(player.selectedActiveAttackAction == EActionType.TRAIL){
					player.dexTrailAttack(enemyActor.cloneRegion().getCenterPoint());
				}
				else if(player.selectedActiveAttackAction == EActionType.BULLET){
					player.bulletAttack(enemyActor.cloneRegion().getCenterPoint());
				}
			}
		}
		displayActorInfo(enemyActor);
	}
	
	public void neutralActorSelected(NeutralActor neutralActor){
		if(gameObjects.isAnyPlayerSelected()){
			gameObjects.getSelectedPlayerList().get(0).dialog(neutralActor);
		}
		displayActorInfo(neutralActor);
	}
	
	public void otherObjectSelected(SolidObject solidObject) {
		if(gameObjects.isAnyPlayerSelected()){
			if(solidObject instanceof InventoryItem) {
				gameObjects.getSelectedPlayerList().get(0).setMoveTarget(solidObject);
				gameObjects.getSelectedPlayerList().get(0).pickItem((InventoryItem)solidObject);
			}
		}
	}
	
	private void displayActorInfo(AbstractActor actor) {
//		containerActorInfo.displayActor(actor);
	}
	
	public void emptyScreenTouched(float x, float y){
		if(gameObjects.isAnyPlayerSelected()){
			for(PlayerActor player : gameObjects.getSelectedPlayerList()){
				if(!player.isDestroyed()){
					BaseRegion targetRegion = player.fromCursorToMiddlePosition(x, y);

					while(gameObjects.getCollidingObject(player, targetRegion) != null){
						targetRegion.y++;
					}
					player.setMoveTarget(targetRegion);
					player.wallHugMovement(targetRegion);
					
					x += player.width + 10;
				}
			}
		}
	}
	
	public void playerSwiped(DexTrailSwipeController swipe, ObjectTouchController objectTouchController){
		if(gameObjects.isAnyPlayerSelected() || (gameObjects.getSelectedPlayerList().size() == 1 && gameObjects.getSelectedPlayerList().get(0) == objectTouchController.player)){
			for(PlayerActor player : gameObjects.getSelectedPlayerList()){
				if(!player.isDestroyed()) {
					if(player.selectedActiveAttackAction == EActionType.TRAIL){
						player.dexTrailAttack(swipe.endPoint.clonePoint());
					}
					else if(player.selectedActiveAttackAction == EActionType.BULLET){
						player.bulletAttack(swipe.endPoint.clonePoint());
					}
				}
			}
		}
		swipe.swiping = false;
	}
	
	public void togglePause(){
		if(gameSpeed == 10){
			gameSpeed(30);
			gameFrameRatio = 4;
			slowPaused = true;
		}else {
			gameSpeed(10);
			gameFrameRatio = 1;
			slowPaused = false;
		}
		//paused = !paused;
	}
	
	public ObjectFactory getObjectFactory() {
		return objectFactory;
	}
	public JSONManager getJsonManager() {
		return jsonManager;
	}
	public AnimationManager getAnimationManager() {
		return animationManager;
	}
	public ParticleManager getParticleManager() {
		return particleManager;
	}
	

	public synchronized void checkTriggerSprings(AbstractActor owningActor, AbstractActor actor) {
		/*
		RegionActionTrigger[] itemlist = regionActionTriggers.begin();
		for (int i = 0, n = regionActionTriggers.size; i < n; i++) {
			RegionActionTrigger regionActionTrigger = itemlist[i];
			if(actor != excludedActor && actor.isCollidingWith(regionActionTrigger.getTriggerSetter().getTriggerRegion())){
				regionActionTrigger.springTrigger(actor);
			}
		}
		regionActionTriggers.end();*/
		for(RegionActionTrigger regionActionTrigger : regionActionTriggers){
			if(actor != owningActor && actor.isCollidingWith(regionActionTrigger.getTriggerSetter().getTriggerRegion())){
				regionActionTrigger.springTrigger(actor);
			}
		}
	}
	public Map<Integer, DexAnimation> getAnimations() {
		return animations;
	}
	public void setDexMissileParticleEffectPool(
			ParticleEffectPool dexMissileParticleEffectPool) {
		this.dexMissileParticleEffectPool = dexMissileParticleEffectPool;
	}
	public void setDexTrailParticleEffectPool(
			ParticleEffectPool dexTrailParticleEffectPool) {
		this.dexTrailParticleEffectPool = dexTrailParticleEffectPool;
	}

	public ParticleEffectPool getDexMissileParticleEffectPool() {
		return dexMissileParticleEffectPool;
	}

	public ParticleEffectPool getDexTrailParticleEffectPool() {
		return dexTrailParticleEffectPool;
	}
	
	/*
	public SnapshotArray<RegionActionTrigger> getRegionActionTriggers() {
		return regionActionTriggers;
	}*/

	public void addRegionActionTrigger(RegionActionTrigger regionActionTrigger){
		regionActionTriggers.add(regionActionTrigger);
	}
	
	public void removeRegionActionTrigger(RegionActionTrigger regionActionTrigger){
		regionActionTriggers.remove(regionActionTrigger);
	}
}
