package com.dexgdx.game.ui.portrait;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.object.PlayerActor;
import com.dexgdx.game.ui.UIElement;

public class UIPortraitFrame extends UIElement {
	private static final long serialVersionUID = 1216895939653733475L;
	
	public int index;
	public PlayerActor portraitPlayer;
	
	private TextureRegion selectedPortrait;

	public UIPortraitFrame(Game game, OrthographicCamera camera,
			TextureRegion defaultImage, PlayerActor portraitPlayer, int index) {
		super(game, camera, defaultImage);
		this.index = index;
		this.portraitPlayer = portraitPlayer;

		height = EUIPlacement.PORTRAIT_HEIGHT.value() + EUIPlacement.PORTRAIT_PADDING.value() * 2;
		width = EUIPlacement.PORTRAIT_WIDTH.value() + EUIPlacement.PORTRAIT_PADDING.value() * 2;
		camRelY = EUIPlacement.PORTRAIT_MARGIN.value() - (camera.viewportHeight / 2 * game.zoomRatio) +
				(EUIPlacement.PORTRAIT_HEIGHT.value() + 2 * EUIPlacement.PORTRAIT_PADDING.value() + EUIPlacement.PORTRAIT_MARGIN.value()) * index;
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (EUIPlacement.PORTRAIT_MARGIN.value() * 3 + EUIPlacement.PORTRAIT_WIDTH.value())
				);
		setColor(Color.BLACK);
		transparency = 1f;
		
		selectedPortrait = game.getTexture(EImagePath.PORTRAIT_SELECTED);
	}

	@Override
	protected void onClick() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onReset() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void preTouchDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void preTouchUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public TextureRegion getImage() {
		return game.gameObjects.getSelectedPlayerList().contains(portraitPlayer) && !portraitPlayer.isDestroyed() ? selectedPortrait : super.getImage();
	}
}
