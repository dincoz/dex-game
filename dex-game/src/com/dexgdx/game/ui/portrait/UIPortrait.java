package com.dexgdx.game.ui.portrait;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.object.PlayerActor;
import com.dexgdx.game.ui.UIElement;

public class UIPortrait extends UIElement {
	private static final long serialVersionUID = 1216895939653733475L;
	
	public int index;
	public PlayerActor portraitPlayer;

	public UIPortrait(Game game, OrthographicCamera camera, PlayerActor portraitPlayer, int index) {
		super(game, camera, null);
		this.index = index;
		
		this.portraitPlayer = portraitPlayer;
		defaultImage = portraitPlayer.portrait;
		
		height = EUIPlacement.PORTRAIT_HEIGHT.value();
		width = EUIPlacement.PORTRAIT_WIDTH.value();
		camRelY = EUIPlacement.PORTRAIT_MARGIN.value() * 2 - (camera.viewportHeight / 2 * game.zoomRatio) +
				(EUIPlacement.PORTRAIT_HEIGHT.value() + 2 * EUIPlacement.PORTRAIT_PADDING.value() + EUIPlacement.PORTRAIT_MARGIN.value()) * index;
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (EUIPlacement.PORTRAIT_MARGIN.value() * 2 + EUIPlacement.PORTRAIT_WIDTH.value())
				);
		transparency = 1f;
	}

	@Override
	protected void onClick() {
		game.gameObjects.selectSinglePlayer(portraitPlayer);
	}

	@Override
	protected void onReset() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void preTouchDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void preTouchUp() {
		// TODO Auto-generated method stub
		
	}
	
}
