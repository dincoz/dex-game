package com.dexgdx.game.ui.dialog;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.dialog.DialogFlow;
import com.dexgdx.game.dialog.IDialogStep;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.container.UIContainer;

public class UIDialogBox extends UIElement {
	private static final long serialVersionUID = -2460953944957341825L;
	
	public BitmapFont font;
	public boolean active;
	private DialogFlow currentFlow;
	public List<IDialogStep> dialogSteps;
	private IDialogStep currentStep;
	
	private boolean pausePrevState;

	public UIDialogBox(Game game, OrthographicCamera camera,
			TextureRegion defaultImage) {
		super(game, camera, defaultImage);
		setColor(Color.BLACK);
		transparency = EUIPlacement.DIALOG_BOX_TRANSPARENCY.value();
		
		font = new BitmapFont(Gdx.files.internal("data/fonts/sensation.fnt"),true);
        float fontScale = 
//        		1f
        		.145f
        		;
        
        font.setScale(fontScale);
        
        dialogSteps = new ArrayList<IDialogStep>();
	}
	
	public void initFromContainer(UIContainer container) {
		height = container.height - EUIPlacement.DIALOG_BOX_MARGIN.value() * 2;
		width = container.width - EUIPlacement.DIALOG_BOX_MARGIN.value() * 2;
		camRelY = container.camRelY + EUIPlacement.DIALOG_BOX_MARGIN.value();
		camRelX = container.camRelX + EUIPlacement.DIALOG_BOX_MARGIN.value();
	}

	@Override
	protected void onClick() {
		currentStep.nextStep(0);
		currentStep = currentStep.currentStep();
		if(currentStep != null){
			dialogSteps.add(currentStep.stepDrawClone());
		}
		else {
			end();
		}
	}

	@Override
	protected void onReset() {

	}
	
	public void startDialog(DialogFlow flow) {
		currentFlow = flow;
		currentStep = currentFlow.currentStep();
		active = true;
		pausePrevState = game.paused;
		game.paused = true;
		deltaY = 0;
		dialogSteps.add(currentStep);
	}
	
	public void end() {
		active = false;
		game.paused = pausePrevState;
		deltaY = camera.position.x + camera.viewportHeight / 2 + 10;
		dialogSteps.clear();
	}

	@Override
	protected void preTouchDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void preTouchUp() {
		// TODO Auto-generated method stub
		
	}
}
