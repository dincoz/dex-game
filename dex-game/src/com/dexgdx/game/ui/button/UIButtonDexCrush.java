package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;

public class UIButtonDexCrush extends UIButtonAction {

	private static final long serialVersionUID = 7488005862447093016L;
	
	public UIButtonDexCrush(Game game, OrthographicCamera camera) {
		super(game, camera, EActionType.CRUSH);
	}

	@Override
	protected void onClick() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onReset() {
		super.onReset();
	}

}
