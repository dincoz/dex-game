package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EUIPlacement;

public class UINodeQueueAction extends UIButton{
	private static final long serialVersionUID = -7172454164154778323L;
	
	public EActionType actionType;
	
	public UINodeQueueAction(Game game, OrthographicCamera camera, EActionType actionType, int index) {
		super(game, camera, null);
		
		float buttonSize = 0;
		float leftMargin = 0;
		
		if(index == 0){
			buttonSize = EUIPlacement.ACTIVE_NODE_SIZE.value() ;
			leftMargin = EUIPlacement.NODE_MARGIN.value();
		}
		else {
			buttonSize = EUIPlacement.NODE_SIZE.value() ;
			leftMargin = EUIPlacement.ACTIVE_NODE_SIZE.value() + 
					(EUIPlacement.NODE_MARGIN.value() * (index + 1)) + 
					(EUIPlacement.NODE_SIZE.value() * (index - 1));
		}
		
		height = buttonSize;
		width = buttonSize;
		camRelY = (camera.viewportHeight / 2 * game.zoomRatio - buttonSize) - 
				(EUIPlacement.ACTION_BUTTON_SIZE.value() + EUIPlacement.ACTION_BUTTON_MARGIN.value() + EUIPlacement.NODE_MARGIN.value());
		camRelX = (leftMargin - camera.viewportWidth / 2 * game.zoomRatio);
		
		setColor(Color.BLACK);
		transparency = EUIPlacement.NODE_TRANSPARENCY.value();
		
		hasPressEffect = false;
	}
	
	@Override
	public TextureRegion getImage() {
		return game.defaultImageOfAction.get(actionType);
	}

	@Override
	protected void onClick() {

	}

	@Override
	protected float getButtonDefaultSize() {
		return EUIPlacement.NODE_SIZE.value();
	}
}
