package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.ui.UIElement;

public abstract class UIButton extends UIElement {

	private static final long serialVersionUID = 7951465367785750426L;

	protected int buttonContainerIndex;
	protected float camRelYPressReverse, camRelXPressReverse;

	public boolean isToggleButton;
	public boolean hasPressEffect;
	
	public UIButton(Game game, OrthographicCamera camera, TextureRegion defaultImage) {
		super(game, camera, defaultImage);
		transparency = 1f;
		isToggleButton = false;
		hasPressEffect = true;
	}
	
	public void checkToggleStatusAndSize() {
		if(isToggleButton && hasPressEffect) {
			if(isToggled()) {
				shrinkPressedSize();
			}
			else {
				resetPressedSize();
			}
		}
	}

	public UIButton withIndex(int buttonContainerIndex){
		this.buttonContainerIndex = buttonContainerIndex;
		return this;
	}
	
	@Override
	protected void onReset(){
		if(hasPressEffect) {
			if(isToggleButton) {
				if(isToggled()) {
					shrinkPressedSize();
				}
				else {
					resetPressedSize();
				}
			}
			else {
				resetPressedSize();
			}
		}
	}

	private void resetPressedSize(){
		if(height == getButtonDefaultSize())
			return;
		float buttonSize = getButtonDefaultSize() ;
		height = buttonSize;
		width = buttonSize;
		camRelY = camRelYPressReverse;
		deltaX = camRelXPressReverse;
	}

	private void shrinkPressedSize(){
		if(height == getButtonDefaultSize() - 6)
			return;
		float buttonSize = getButtonDefaultSize() - 6;
		height = buttonSize;
		width = buttonSize;
		camRelYPressReverse = camRelY;
		camRelXPressReverse = deltaX;
		camRelY += 3;
		deltaX += 3;
	}
	
	@Override
	protected void preTouchUp() {
		
	}
	
	protected boolean isToggled() {
		return false;
	}
	
	@Override
	protected void preTouchDown() {
		if(hasPressEffect) 
			shrinkPressedSize();
	}
	
	abstract float getButtonDefaultSize();
}
