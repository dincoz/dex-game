package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;

public class UIButtonDexTrail extends UIButtonAction {

	private static final long serialVersionUID = 8285894601732263583L;

	public UIButtonDexTrail(Game game, OrthographicCamera camera) {
		super(game, camera, EActionType.TRAIL);
		isToggleButton = true;
	}

	@Override
	protected void onClick() {
		if(game.gameObjects.getSelectedPlayerList().size() > 0){
			if(game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction != EActionType.TRAIL) {
				game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction = EActionType.TRAIL;
			}
			else {
				game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction = EActionType.OTHER;
			}
		}
	}

	@Override
	protected void onReset() {
		super.onReset();
	}

}
