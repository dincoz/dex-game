package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.*;
import com.dexgdx.game.*;
import com.dexgdx.game.constants.*;

public class UIButtonBullet extends UIButtonAction{
	private static final long serialVersionUID = 6685476030619019479L;

	public UIButtonBullet(Game game, OrthographicCamera camera) {
		super(game, camera, EActionType.BULLET);
		isToggleButton = true;
	}

	@Override
	protected void onClick() {
		if(game.gameObjects.getSelectedPlayerList().size() > 0){
			if(game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction != EActionType.BULLET) {
				game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction = EActionType.BULLET;
			}
			else {
				game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction = EActionType.OTHER;
			}
		}
	}

	@Override
	protected void onReset() {
		super.onReset();
	}
}
