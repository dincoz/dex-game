package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;

public class UIButtonDexMissile extends UIButtonAction {

	private static final long serialVersionUID = 7488005862447093016L;
	
	public UIButtonDexMissile(Game game, OrthographicCamera camera) {
		super(game, camera, EActionType.MISSILE);
		isToggleButton = true;
	}

	@Override
	protected void onClick() {
		if(game.gameObjects.getSelectedPlayerList().size() > 0){
			if(game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction != EActionType.MISSILE) {
				game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction = EActionType.MISSILE;
			}
			else {
				game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction = EActionType.MISSILE;
			}
		}
	}

}
