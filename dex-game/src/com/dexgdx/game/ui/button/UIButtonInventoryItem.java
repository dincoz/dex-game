package com.dexgdx.game.ui.button;
import com.badlogic.gdx.graphics.*;
import com.dexgdx.game.*;
import com.dexgdx.game.constants.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.dexgdx.game.object.*;
import com.dexgdx.game.ui.container.*;

public class UIButtonInventoryItem extends UIButton{
	private static final long serialVersionUID = -2594714131763020589L;
	
	private int indexX;
	private int indexY;
	
	private UIContainerInventory uiInventory;
	
	public UIButtonInventoryItem(Game game, OrthographicCamera camera, UIContainerInventory uiInventory, int indexX, int indexY) {
		super(game, camera, null);
		
		this.uiInventory = uiInventory;
		
		float buttonSize = EUIPlacement.ACTION_BUTTON_SIZE.value();
		
		height = buttonSize;
		width = buttonSize;

		transparency = EUIPlacement.ACTION_BUTTON_TRANSPARENCY.value();
		wrapperImage = game.getTexture(EImagePath.CONTAINER_ACTION_BUTTON);
		
		this.indexX = indexX;
		this.indexY = indexY;
		
		camRelX = EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value() + game.ui.container(EUIContainer.INVENTORY).camRelX + (EUIPlacement.INVENTORY_BUTTON_ITEM_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value()) * indexX;
		camRelY = EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value() + game.ui.container(EUIContainer.INVENTORY).camRelY + (EUIPlacement.INVENTORY_BUTTON_ITEM_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value()) * indexY
			+ (camera.viewportHeight / 2 * game.zoomRatio);
		
	}
	
	@Override
	protected void onClick(){
		// TODO: Implement this method
	}

	@Override
	protected float getButtonDefaultSize() {
		return EUIPlacement.INVENTORY_BUTTON_ITEM_SIZE.value();
	}

	@Override
	public TextureRegion getImage(){
		if(uiInventory.getPlayerActor()!=null){
			InventoryItem item = uiInventory.getPlayerActor().inventory.getItem(indexX, indexY);
			if(item!= null)
				return item.inventoryImage;
		}
		return null;
	}
	
	
}
