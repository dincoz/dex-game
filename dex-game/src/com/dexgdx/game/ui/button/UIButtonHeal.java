package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;

public class UIButtonHeal extends UIButtonAction {

	private static final long serialVersionUID = -7480206951774326810L;

	private TextureRegion healOnImage;
	
	private boolean healIsOn;

	public UIButtonHeal(Game game, OrthographicCamera camera, TextureRegion healOnImage){
		super(game, camera, EActionType.HEAL);
		this.healOnImage = healOnImage;
		this.healOnImage.flip(false, true);
		
		healIsOn = false;
	}

	@Override
	public TextureRegion getImage(){
		return touchedDown ? defaultImage : defaultImage;
	}

	@Override
	protected void onClick() {
		if(game.gameObjects.getSelectedPlayerList().size() > 0){
			game.gameObjects.getSelectedPlayerList().get(0).heal();
		}
		healIsOn = !healIsOn;
	}

	@Override
	protected void onReset() {
		super.onReset();
		healIsOn = false;
	}
	
}
