package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;

public class UIButtonDexGuard extends UIButtonAction {

	private static final long serialVersionUID = 7488005862447093016L;
	
	public UIButtonDexGuard(Game game, OrthographicCamera camera) {
		super(game, camera, EActionType.GUARD);
	}

	@Override
	protected void onClick() {
		
	}

	@Override
	protected void onReset() {
		super.onReset();
	}

}
