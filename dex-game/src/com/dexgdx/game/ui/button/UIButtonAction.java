package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;

public abstract class UIButtonAction extends UIButton{

	private static final long serialVersionUID = -7172454164154778323L;
	
	public EActionType actionType;
	
	float cameraHalfWidthMinusMargin, cameraHalfHeightMinusMargin, buttonSizeAndMargin;
	
	public UIButtonAction(Game game, OrthographicCamera camera, EActionType actionType) {
		super(game, camera, game.defaultImageOfAction.get(actionType));
		this.actionType = actionType;

		float buttonSize = EUIPlacement.ACTION_BUTTON_SIZE.value();
		float buttonMargin = EUIPlacement.ACTION_BUTTON_MARGIN.value();
		
		height = buttonSize;
		width = buttonSize;
		
		transparency = EUIPlacement.ACTION_BUTTON_TRANSPARENCY.value();
		wrapperImage = game.getTexture(EImagePath.CONTAINER_ACTION_BUTTON);
		buttonSizeAndMargin = (EUIPlacement.ACTION_BUTTON_SIZE.value() + EUIPlacement.ACTION_BUTTON_MARGIN.value());
		
		if(actionType.isActiveAction()) {
			camRelY = (camera.viewportHeight * game.zoomRatio / 2 - (buttonSize + buttonMargin));
			cameraHalfWidthMinusMargin = camera.viewportWidth * game.zoomRatio / 2 - buttonMargin;
		}
		else {
			camRelX = (camera.viewportWidth * game.zoomRatio / 2) - (buttonSize + buttonMargin);
			cameraHalfHeightMinusMargin = camera.viewportHeight * game.zoomRatio / 2 - buttonMargin;
		}
	}
	
//	public UIButtonAction(Game game, OrthographicCamera camera, EActionType actionType) {
//		super(game, camera, game.defaultImageOfAction.get(actionType));
//		this.actionType = actionType;
//
//		float buttonSize = EUIPlacement.ACTION_BUTTON_SIZE.value();
//		float buttonMargin = EUIPlacement.ACTION_BUTTON_MARGIN.value();
//		
//		height = buttonSize;
//		width = buttonSize;
//		camRelY = (camera.viewportHeight * game.zoomRatio / 2 - (buttonSize + buttonMargin));
//		
//		cameraHalfWidthMinusMargin = camera.viewportWidth * game.zoomRatio / 2 - buttonMargin;
//		buttonSizeAndMargin = (EUIPlacement.ACTION_BUTTON_SIZE.value() + EUIPlacement.ACTION_BUTTON_MARGIN.value());
//		
//		transparency = EUIPlacement.ACTION_BUTTON_TRANSPARENCY.value();
//		wrapperImage = game.getTexture(EImagePath.CONTAINER_ACTION_BUTTON);
//	}
	
	@Override
	protected boolean isToggled() {
		boolean response = false;
		
		response = game.gameObjects.getSelectedPlayerList().size() > 0 && 
				(this.actionType == game.gameObjects.getSelectedPlayerList().get(0).selectedActiveAttackAction ||
				this.actionType == game.gameObjects.getSelectedPlayerList().get(0).selectedActiveDefenseAction);
		
		return response;
	}
	
	@Override
	public TextureRegion getWrapperImage() {
		wrapperImage = isToggled() ? game.getTexture(EImagePath.CONTAINER_ACTION_BUTTON_TOGGLE) : game.getTexture(EImagePath.CONTAINER_ACTION_BUTTON);
		return wrapperImage;
	}
	
	@Override
	public float getRelativeY() {
		if(actionType.isActiveAction()) {
			return super.getRelativeY();
		}
		y = camera.position.y + (cameraHalfHeightMinusMargin - buttonSizeAndMargin * (buttonContainerIndex)) + deltaY - buttonSizeAndMargin - (EUIPlacement.PAUSE_BUTTON_SIZE.value() + EUIPlacement.PAUSE_BUTTON_MARGIN.value())
		//	+ game.ui.container(EUIContainer.ACTIONS).x
		;
		return y;
	}
	
	@Override
	public float getRelativeX() {
		if(!actionType.isActiveAction()) {
			return super.getRelativeX();
		}
		x = camera.position.x + (cameraHalfWidthMinusMargin - buttonSizeAndMargin * (buttonContainerIndex)) + deltaX - buttonSizeAndMargin - (EUIPlacement.PAUSE_BUTTON_SIZE.value() + EUIPlacement.PAUSE_BUTTON_MARGIN.value())
//				+ game.ui.container(EUIContainer.ACTIONS).x
				;
		return x;
	}
	
	@Override
	protected float getButtonDefaultSize() {
		return EUIPlacement.ACTION_BUTTON_SIZE.value();
	}
}
