package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;

public class UIButtonPause extends UIButton {

	private static final long serialVersionUID = -7480206951774326810L;

	private TextureRegion pausedImage;

	public UIButtonPause(Game game, OrthographicCamera camera, TextureRegion unPausedImage, TextureRegion pausedImage) {
		super(game, camera, unPausedImage);
		this.pausedImage = pausedImage;
		
		height = EUIPlacement.PAUSE_BUTTON_SIZE.value();
		width = EUIPlacement.PAUSE_BUTTON_SIZE.value();
		camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (EUIPlacement.PAUSE_BUTTON_MARGIN.value() + EUIPlacement.PAUSE_BUTTON_SIZE.value())
				);
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (EUIPlacement.PAUSE_BUTTON_MARGIN.value() + EUIPlacement.PAUSE_BUTTON_SIZE.value())
				);
		wrapperImage = game.getTexture(EImagePath.CONTAINER_PAUSE);
		isToggleButton = true;
	}

	@Override
	public TextureRegion getImage(){
//		return game.gameSpeed == 10 ?  defaultImage : pausedImage;
		return game.paused || game.slowPaused ? pausedImage : defaultImage;
	}

	@Override
	protected void onClick() {
		game.togglePause();
	}
	

	@Override
	protected boolean isToggled() {
		return game.paused || game.slowPaused;
	}
	
	@Override
	protected float getButtonDefaultSize() {
		return EUIPlacement.PAUSE_BUTTON_SIZE.value();
	}
}
