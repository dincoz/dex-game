package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.container.*;
import com.dexgdx.game.constants.*;

public class UIButtonInventoryOpen extends UIButton {
	private static final long serialVersionUID = 683541320390531809L;
	
	UIContainer container;

	public UIButtonInventoryOpen(Game game, OrthographicCamera camera) {
		super(game, camera, game.getTexture(EImagePath.INVENTORY));
		
		height = EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value();
		width = EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value();
		camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
		wrapperImage = game.getTexture(EImagePath.CONTAINER_INVENTORY_BUTTON);
		
		container = game.ui.container(EUIContainer.CENTER_MENU);
		//hasPressEffect=false;
	}

	@Override
	protected void onClick() {
		if(game.inventoryBox.isOpen()){
			game.inventoryBox.close();
		}
		else if(game.gameObjects.isAnyPlayerSelected()) {
			game.inventoryBox.open(game.gameObjects.getSelectedPlayerList().get(0));
		}
	}
	
	@Override
	protected void onReset(){
		super.onReset();
		/***
		
		if(game.inventoryBox.isOpen()){
			container.camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			container.camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
		}
		else if(game.gameObjects.isAnyPlayerSelected()) {
			
			container.camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			container.camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
		}
		getRelativeY();
		container.getRelativeY();
		**/
	}
	
	
	
	
	@Override
	protected float getButtonDefaultSize() {
		return EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value();
	}
}
