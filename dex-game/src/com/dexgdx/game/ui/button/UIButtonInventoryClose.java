package com.dexgdx.game.ui.button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.container.*;
import com.dexgdx.game.constants.*;

public class UIButtonInventoryClose extends UIButton {
	private static final long serialVersionUID = 683541320390531809L;
	
	UIContainer container;

	public UIButtonInventoryClose(Game game, OrthographicCamera camera) {
		super(game, camera, game.getTexture(EImagePath.INVENTORY));
		
		container = game.ui.container(EUIContainer.INVENTORY);
		
		height = EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value();
		width = EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value();
		
		camRelY = container.camRelY + container.height - height - EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value();
		camRelX = container.camRelX + container.width - width - EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value();//(camera.viewportWidth / 2 * game.zoomRatio) - (((camera.viewportWidth * game.zoomRatio) - container.width) / 2 - width);
		
		wrapperImage = game.getTexture(EImagePath.CONTAINER_INVENTORY_BUTTON);
		//hasPressEffect=false;
	}

	@Override
	protected void onClick() {
		if(game.inventoryBox.isOpen()){
			game.inventoryBox.close();
		}
		else if(game.gameObjects.isAnyPlayerSelected()) {
			game.inventoryBox.open(game.gameObjects.getSelectedPlayerList().get(0));
		}
	}
	
	@Override
	protected void onReset(){
		super.onReset();
		/***
		
		if(game.inventoryBox.isOpen()){
			container.camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			container.camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				//EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
		}
		else if(game.gameObjects.isAnyPlayerSelected()) {
			
			container.camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			container.camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
			camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value())
				);
		}
		getRelativeY();
		container.getRelativeY();
		**/
	}
	
	
	
	
	@Override
	protected float getButtonDefaultSize() {
		return EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value();
	}
}
