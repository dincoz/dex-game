package com.dexgdx.game.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.ui.container.UIContainer;

public abstract class UIElement extends BaseRegion {
	private static final long serialVersionUID = 2176171023573813705L;
	
	public float transparency = .6f;

	protected Game game;
	protected OrthographicCamera camera;
	protected boolean touchedDown;
	protected TextureRegion defaultImage;
	protected TextureRegion wrapperImage;
	private Color color;

	public float camRelX;
	public float camRelY;
	public float deltaX = 0f;
	public float deltaY = 0f;

	public UIElement(Game game, OrthographicCamera camera, TextureRegion defaultImage) {
		super(0, 0, 0, 0);
		this.game = game;
		this.camera = camera;
		this.defaultImage = defaultImage;
	}
	
	public TextureRegion getWrapperImage() {
		return wrapperImage;
	}
	
	public TextureRegion getImage(){
		return defaultImage;
	}

	public TextureRegion getDefaultImage(){
		return defaultImage;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}
	
	public float getDeltaX() {
		return deltaX;
	}
	
	public float getDeltaY() {
		return deltaY;
	}
	
	public float getRelativeX() {
		x = camera.position.x  + camRelX + getDeltaX();
		return x;
	}

	public float getRelativeY() {
		y = camera.position.y + camRelY + getDeltaY();
		return y;
	}

	public final boolean isTouchedDown() {
		return touchedDown;
	}

	public final void touchedDown(){
		touchedDown = true;
		boolean  unClickableUi = this instanceof UIGame || this instanceof UIContainer;
		if(!unClickableUi)
			game.touchedUIElement = this;
		preTouchDown();
	}
	
	public final void touchedUp(){
		preTouchUp();
		if(game.touchedUIElement == this){
			onClick();
		}
		else {
			game.touchedUIElement.reset();
		}
		reset();
	}
	
	public final void reset(){
		touchedDown = false;
		onReset();
	}

	protected abstract void preTouchDown();
	protected abstract void preTouchUp();
	protected abstract void onClick();
	protected abstract void onReset();
}
