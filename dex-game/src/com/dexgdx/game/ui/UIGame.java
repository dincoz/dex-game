package com.dexgdx.game.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIContainer;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.ui.container.UIContainer;
import com.dexgdx.game.ui.container.UIContainerActionBottom;
import com.dexgdx.game.ui.container.UIContainerActorInfo;
import com.dexgdx.game.ui.container.UIContainerDialog;
import com.dexgdx.game.ui.container.UIContainerCenterMenu;
import com.dexgdx.game.ui.container.UIContainerPortrait;
import com.dexgdx.game.ui.container.UIContainerQueueActions;
import com.dexgdx.game.ui.container.UIContainerPause;
import com.dexgdx.game.ui.container.UIContainerActionSide;
import com.dexgdx.game.ui.container.*;

public class UIGame extends UIContainer {
	private static final long serialVersionUID = 18440334238915838L;

	private Map<EUIContainer, UIContainer> uiContainers;
	private List<EUIContainer> containerOrder;
	
	public UIGame(Game game, OrthographicCamera camera) {
		super(game, camera, null);
	}

	@Override
	protected void initialize() {
		uiContainers = new HashMap<EUIContainer, UIContainer>();
		UIContainerDialog uiContainerDialog = new UIContainerDialog(game, camera);
		uiContainers.put(EUIContainer.DIALOG, uiContainerDialog);
		UIContainerInventory uiContainerInventory = new UIContainerInventory(game, camera);
		uiContainers.put(EUIContainer.INVENTORY, uiContainerInventory);
		UIContainerActionBottom uiContainerBottomActionButton = new UIContainerActionBottom(game, camera);
		uiContainers.put(EUIContainer.BOTTOM_ACTIONS, uiContainerBottomActionButton);
		UIContainerActionSide uiContainerSideActionButton = new UIContainerActionSide(game, camera);
		uiContainers.put(EUIContainer.SIDE_ACTIONS, uiContainerSideActionButton);
		UIContainerPause uiContainerPause = new UIContainerPause(game, camera, null);
		uiContainers.put(EUIContainer.PAUSE, uiContainerPause);
		UIContainerQueueActions uiContainerNodeActiveAction = new UIContainerQueueActions(game, camera);
		uiContainers.put(EUIContainer.QUEUE_ACTIONS, uiContainerNodeActiveAction);
		UIContainerActorInfo uiContainerActorInfo = new UIContainerActorInfo(game, camera, game.getTexture(EImagePath.CONTAINER_ACTOR_INFO));
		uiContainers.put(EUIContainer.ACTOR_INFO, uiContainerActorInfo);
		UIContainerPortrait uiContainerPortrait = new UIContainerPortrait(game, camera, game.getTexture(EImagePath.PORTRAIT_UNSELECTED), game.getTexture(EImagePath.DEATH_OVERLAY));
		uiContainers.put(EUIContainer.PORTRAIT, uiContainerPortrait);
		UIContainerCenterMenu uiContainerInventoryButton = new UIContainerCenterMenu(game, camera);
		uiContainers.put(EUIContainer.CENTER_MENU, uiContainerInventoryButton);

		containerOrder = new ArrayList<EUIContainer>();
		containerOrder.add(EUIContainer.INVENTORY);
		containerOrder.add(EUIContainer.PAUSE);
		containerOrder.add(EUIContainer.CENTER_MENU);
		containerOrder.add(EUIContainer.DIALOG);
		containerOrder.add(EUIContainer.BOTTOM_ACTIONS);
		containerOrder.add(EUIContainer.SIDE_ACTIONS);
		containerOrder.add(EUIContainer.QUEUE_ACTIONS);
		containerOrder.add(EUIContainer.ACTOR_INFO);
		containerOrder.add(EUIContainer.PORTRAIT);
	}

	public UIContainer container(EUIContainer containerType){
		return uiContainers.get(containerType);
	}
	
	@Override
	public void add(UIElement uiElement) {}
	@Override
	public float getRelativeX() {return 0;}
	@Override
	public float getRelativeY() {return 0;}
	@Override
	protected void onClick() {}
	@Override
	protected void onReset() {}

	protected UIContainer touchedContainer;

	public UIContainer getTouchedContainer(){
		return touchedContainer;
	}
	
	public boolean foundTouchedContainer(float x, float y){
		boolean contains = false;
		for (EUIContainer containerType : containerOrder) {
			boolean checkContainer = false;
			if(containerType == EUIContainer.DIALOG && game.dialogBox.active){
				checkContainer = true;
			}
			else if(containerType == EUIContainer.INVENTORY && game.inventoryBox.isOpen()) {
				checkContainer = true;
			}
			else if(!game.dialogBox.active && !game.inventoryBox.isOpen()){
				checkContainer = true;
			}
			if(checkContainer){
				if(uiContainers.get(containerType).foundTouchedElement(x, y)){
					contains = true;
					touchedContainer = uiContainers.get(containerType);
					break;
				}
			}
		}
		return contains;
	}

	@Override
	public boolean foundTouchedElement(float x, float y) {
		return foundTouchedContainer(x, y);
	}
}
