package com.dexgdx.game.ui.container;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.button.UIButtonAction;

public class UIContainerActionBottom extends UIContainer {
	private static final long serialVersionUID = 4587593165102458291L;
	
	private Map<EActionType, UIButtonAction> uiButtonActions;

	public UIContainerActionBottom(Game game, OrthographicCamera camera) {
		super(game, camera, null);
	}

	@Override
	protected void initialize() {
		uiButtonActions = new HashMap<EActionType, UIButtonAction>();
		
		height = EUIPlacement.ACTION_BUTTON_MARGIN.value() * 2 + EUIPlacement.ACTION_BUTTON_SIZE.value();
		width = (camera.viewportWidth * game.zoomRatio);
		camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (EUIPlacement.ACTION_BUTTON_MARGIN.value() * 2 + EUIPlacement.ACTION_BUTTON_SIZE.value())
				);
		camRelX = (camera.viewportWidth / 2 * game.zoomRatio) * -1;
		
		setColor(Color.BLACK);
		transparency = EUIPlacement.ACTION_CONTAINER_TRANSPARENCY.value();
	}

	@Override
	public void add(UIElement uiElement) {
		UIButtonAction uiButtonAction = (UIButtonAction) uiElement;
		uiButtonActions.put(uiButtonAction.actionType, uiButtonAction);
	}
	
	@Override
	public UIButtonAction getByActionType(EActionType actionType) {
		return uiButtonActions.get(actionType);
	}

	@Override
	protected void onClick() {
		
	}

	@Override
	public boolean foundTouchedElement(float x, float y){
		boolean contains = false;
		for (EActionType actionType : uiButtonActions.keySet()) {
			uiButtonActions.get(actionType).getRelativeX();
			uiButtonActions.get(actionType).getRelativeY();
			if(uiButtonActions.get(actionType).contains(x, y)){
				contains = true;
				touchedElement = uiButtonActions.get(actionType);
				break;
			}
		}
		if(!contains && transparency > 0)
			contains = super.contains(x, y);
		return contains;
	}
	
	@Override
	public void preDraw() {
		for (EActionType actionType : uiButtonActions.keySet()) {
			uiButtonActions.get(actionType).withIndex(-1);
		}
	}
}
