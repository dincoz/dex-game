package com.dexgdx.game.ui.container;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.indicator.UIIndicatorBlood;
import com.dexgdx.game.ui.indicator.UIIndicatorDisease;
import com.dexgdx.game.ui.indicator.UIIndicatorMan;

public class UIContainerActorInfo extends UIContainer {
	private static final long serialVersionUID = -2566343681439460817L;

	public AbstractActor actor;
	public boolean visible;
	public BitmapFont font;
	
	public UIIndicatorMan manIndicator;
	public UIIndicatorBlood bloodIndicator;
	public UIIndicatorDisease diseaseIndicator;
	
	public UIContainerActorInfo(Game game, OrthographicCamera camera,
			TextureRegion defaultImage) {
		super(game, camera, defaultImage);
	}
	
	public void displayActor(AbstractActor actor) {
		if(this.actor != actor && actor != null) {
			this.actor = actor;
			visible = true;
		}
		else {
			this.actor = null;
			visible = false;
		}
	}
	
	@Override
	public float getDeltaX() {
		return actor.getImageCurrentRegion().getCenterPoint().x 
				- (EUIPlacement.ACTORINFO_CONTAINER_MARGINLEFT.value() - actor.getImageCurrentRegion().width / 2)
				- camera.position.x;
	}
	
	@Override
	public float getDeltaY() {
		return actor.getImageCurrentRegion().y - EUIPlacement.ACTORINFO_CONTAINER_MARGINTOP.value() - camera.position.y;
	}

	@Override
	protected void initialize() {
		height = EUIPlacement.ACTORINFO_CONTAINER_HEIGHT.value();
		width = EUIPlacement.ACTORINFO_CONTAINER_WIDTH.value();
		visible = false;
		setColor(Color.BLACK);
		transparency = EUIPlacement.ACTORINFO_CONTAINER_TRANSPARENCY.value();

		font = new BitmapFont(Gdx.files.internal("data/fonts/sensation.fnt"),true);
	    float fontScale = 
//	    		1f
	    		.14f
	    		;
	    
	    font.setScale(fontScale);
	}

	@Override
	public void add(UIElement uiElement) {
		if(uiElement instanceof UIIndicatorMan)
			manIndicator = (UIIndicatorMan) uiElement;
		else if(uiElement instanceof UIIndicatorBlood)
			bloodIndicator = (UIIndicatorBlood) uiElement;
		else if(uiElement instanceof UIIndicatorDisease)
			diseaseIndicator = (UIIndicatorDisease) uiElement;
			
	}

	@Override
	public boolean foundTouchedElement(float x, float y) {
		return false;
	}

	@Override
	protected void onClick() {
		
	}
}
