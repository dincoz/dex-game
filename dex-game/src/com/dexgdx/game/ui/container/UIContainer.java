package com.dexgdx.game.ui.container;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.button.UIButton;
import com.dexgdx.game.ui.button.UIButtonAction;

public abstract class UIContainer extends UIElement {

	private static final long serialVersionUID = -3650895197977393578L;

	public UIContainer(Game game, OrthographicCamera camera,
			TextureRegion defaultImage) {
		super(game, camera, defaultImage);
		initialize();
	}
	
	protected abstract void initialize();
	public abstract void add(UIElement uiElement);

	public UIButtonAction getByActionType(EActionType actionType){return null;}
	public UIElement getSingleElement(){return null;}
	public UIElement getByIndex(int index){return null;}
	public UIButton getByIndexAndAction(int index, EActionType actionType){return null;}
	public UIButton getByXandY(float x, float y){return null;}
	
	protected UIElement touchedElement;
	public UIElement getTouchedElement(){
		return touchedElement;
	}

	public abstract boolean foundTouchedElement(float x, float y);
	
	public void preDraw(){};
	
	@Override
	protected void onReset() {
		touchedElement = null;
	}
	
	@Override
	public TextureRegion getImage() {
		return wrapperImage == null ? defaultImage : wrapperImage;
	}

	@Override
	protected void preTouchUp() {}
	@Override
	protected void preTouchDown() {}
}
