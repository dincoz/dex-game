package com.dexgdx.game.ui.container;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.button.UIButtonPause;

public class UIContainerPause extends UIContainer {
	private static final long serialVersionUID = 6228687225473299954L;

	private UIButtonPause uiButtonPause;
	
	public UIContainerPause(Game game, OrthographicCamera camera,
			TextureRegion defaultImage) {
		super(game, camera, defaultImage);
	}

	@Override
	protected void initialize() {
		height = EUIPlacement.PAUSE_BUTTON_SIZE.value() + EUIPlacement.PAUSE_BUTTON_MARGIN.value() * 2;
		width = EUIPlacement.PAUSE_BUTTON_SIZE.value() + EUIPlacement.PAUSE_BUTTON_MARGIN.value() * 2;
		camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (EUIPlacement.PAUSE_BUTTON_MARGIN.value() * 2 + EUIPlacement.PAUSE_BUTTON_SIZE.value())
				);
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (EUIPlacement.PAUSE_BUTTON_MARGIN.value() * 2 + EUIPlacement.PAUSE_BUTTON_SIZE.value())
				);
		setColor(Color.BLACK);
		transparency = EUIPlacement.PAUSE_BUTTON_TRANSPARENCY.value();
	}

	@Override
	public void add(UIElement uiElement) {
		uiButtonPause = (UIButtonPause) uiElement;
	}
	
	@Override
	public UIElement getSingleElement() {
		return uiButtonPause;
	}

	@Override
	protected void onClick() {

	}

	@Override
	public boolean foundTouchedElement(float x, float y){
		uiButtonPause.getRelativeX();
		uiButtonPause.getRelativeY();
		boolean contains = uiButtonPause.contains(x, y);
		if(contains)
			touchedElement = uiButtonPause;
		if(!contains)
			contains = super.contains(x, y);
		return contains;
	}

}
