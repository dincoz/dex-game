package com.dexgdx.game.ui.container;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.portrait.UIPortrait;
import com.dexgdx.game.ui.portrait.UIPortraitFrame;

public class UIContainerPortrait extends UIContainer {

	private TextureRegion deathOverlay;
	public List<UIPortraitFrame> uiPortraitFrames;
	public List<UIPortrait> uiPortraits;
	
	public UIContainerPortrait(Game game, OrthographicCamera camera,
			TextureRegion defaultImage, TextureRegion deathOverlay) {
		super(game, camera, defaultImage);
		this.deathOverlay = deathOverlay;
	}

	private static final long serialVersionUID = 2562414131886561420L;

	@Override
	protected void initialize() {
		height = EUIPlacement.PORTRAIT_HEIGHT.value() + EUIPlacement.PORTRAIT_PADDING.value() * 2;
		width = EUIPlacement.PORTRAIT_WIDTH.value() + EUIPlacement.PORTRAIT_PADDING.value() * 2;
		camRelY = EUIPlacement.PORTRAIT_MARGIN.value() - (camera.viewportHeight / 2 * game.zoomRatio);
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (EUIPlacement.PORTRAIT_MARGIN.value() * 3 + EUIPlacement.PORTRAIT_WIDTH.value())
				);
		setColor(Color.BLACK);
		transparency = 1f;
		
		uiPortraits = new ArrayList<UIPortrait>();
		uiPortraitFrames = new ArrayList<UIPortraitFrame>();
	}

	@Override
	public void add(UIElement uiElement) {
		UIPortrait uiPortrait = (UIPortrait) uiElement;
		uiPortrait.index = uiPortraits.size();
		uiPortraits.add(uiPortrait);
		
		uiPortraitFrames.add(new UIPortraitFrame(game, camera, defaultImage, uiPortrait.portraitPlayer, uiPortraits.size() - 1));
	}

	@Override
	public boolean foundTouchedElement(float x, float y) {
		boolean contains = false;
		for (UIPortrait portrait : uiPortraits) {
			portrait.getRelativeX();
			portrait.getRelativeY();
			if(portrait.contains(x, y)){
				contains = true;
				touchedElement = portrait;
				break;
			}
		}
		if(!contains && transparency > 0)
			contains = super.contains(x, y);
		return contains;
	}

	@Override
	protected void onClick() {
		
	}
	
	@Override
	public UIElement getByIndex(int index) {
		return uiPortraits.get(index);
	}
	
	public UIElement getFrameByIndex(int index) {
		return uiPortraitFrames.get(index);
	}
	
	public TextureRegion getDeathOverlayByIndex(int index) {
		if(uiPortraits.get(index).portraitPlayer.isDestroyed()) {
			return deathOverlay;
		}
		return null;
	}
}
