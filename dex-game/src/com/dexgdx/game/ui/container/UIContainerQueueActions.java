package com.dexgdx.game.ui.container;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.action.runner.PotentialActionRunner;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.input.IFlingable;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.button.UIButton;
import com.dexgdx.game.ui.button.UINodeQueueAction;

public class UIContainerQueueActions extends UIContainer implements IFlingable {
	private static final long serialVersionUID = -6984749225669824481L;

	private List<UINodeQueueAction> uiNodeActiveActions;
	
	private float nodeY;

	public UIContainerQueueActions(Game game, OrthographicCamera camera) {
		super(game, camera, null);
		uiNodeActiveActions = new ArrayList<UINodeQueueAction>();
	}

	@Override
	protected void initialize() {
		float buttonSize = EUIPlacement.NODE_SIZE.value() ;
		nodeY = (camera.viewportHeight / 2 * game.zoomRatio - buttonSize) - 
				(EUIPlacement.ACTION_BUTTON_SIZE.value() + 2 * EUIPlacement.ACTION_BUTTON_MARGIN.value() + EUIPlacement.NODE_MARGIN.value());
	
		setColor(Color.BLACK);
		transparency = EUIPlacement.NODE_CONTAINER_TRANSPARENCY.value();
	}
	
	@Override
	public void add(UIElement uiElement) {
		UINodeQueueAction uiNodeActiveAction = (UINodeQueueAction) uiElement;
		uiNodeActiveActions.add(uiNodeActiveAction);
	}
	
	@Override
	public UIButton getByIndexAndAction(int index, EActionType actionType) {
		UINodeQueueAction uiNodeQueueAction = (UINodeQueueAction) uiNodeActiveActions.get(index).withIndex(index);
		uiNodeQueueAction.actionType = actionType;
		return uiNodeQueueAction;
	}
	
	@Override
	protected void onClick() {
//		remove(uiNodeActiveActions.indexOf(getTouchedElement()));
	}
	
	protected void remove(int buttonContainerIndex){
		if(game.gameObjects.getSelectedPlayerList().size() == 1){
			PotentialActionRunner potentialActionRunner = game.gameObjects.getSelectedPlayerList().get(0).getActionRunner().getPotentialActionRunner();
			if(buttonContainerIndex > 0 || potentialActionRunner.waitTime == 0){
				potentialActionRunner.getActionList().remove(buttonContainerIndex);
				for (int i = 0 ; i < uiNodeActiveActions.size() ; ++i) {
					uiNodeActiveActions.get(i).withIndex(i);
				}
			}
		}
	}
	
	@Override
	public boolean foundTouchedElement(float x, float y){
		boolean contains = false;
		if(game.gameObjects.getSelectedPlayerList().size() == 1){
			for(int i = 0; i < game.gameObjects.getSelectedPlayerList().get(0).getActionRunner().getPotentialActionRunner().getActionList().size() ; ++i ){
				UINodeQueueAction uiNodeQueueAction = uiNodeActiveActions.get(i);
				uiNodeQueueAction.getRelativeX();
				uiNodeQueueAction.getRelativeY();
	 			if(uiNodeQueueAction.contains(x, y)){
					contains = true;
					touchedElement = uiNodeQueueAction;
					break;
				}
			}
		}
		return contains;
	}

	@Override
	public int size() {
		return game.gameObjects.getSelectedPlayerList().get(0).getActionRunner().getPotentialActionRunner().getActionList().size();
	}

	@Override
	public float y(int index) {
		return uiNodeActiveActions.get(index).getRelativeY();
	}

	@Override
	public float x(int index) {
		return uiNodeActiveActions.get(index).getRelativeX();
	}

	@Override
	public float transparency(int index) {
		return uiNodeActiveActions.get(index).transparency;
	}

	@Override
	public int selectedIndex() {
		if(touchedElement != null)
			return uiNodeActiveActions.indexOf(touchedElement);
		return 0;
	}

	@Override
	public boolean isStarted() {
		return selectedIndex() > 0;
	}

	@Override
	public void lowerX(int index, float amount) {
		uiNodeActiveActions.get(index).deltaX += amount * -1;
	}

	@Override
	public void raiseY(int index, float amount) {
		uiNodeActiveActions.get(index).deltaY += amount;
//		game.message = 
//				"raiseY:deltaY: " + uiNodeActiveActions.get(index).deltaY
//				+ "\nraiseY:amount: " + amount
//				+ "\nraiseY:amount * game.game.zoomRatio: " + (amount * game.game.zoomRatio)
//				+ "\nraiseY:y: " + getRelativeY();
	}

	@Override
	public void setTransparency(int index, float transparency) {
		uiNodeActiveActions.get(index).transparency = transparency <= EUIPlacement.NODE_TRANSPARENCY.value() ? transparency : EUIPlacement.NODE_TRANSPARENCY.value() ;
	}

	@Override
	public void complete(int index) {
		for (UINodeQueueAction uiNodeQueueAction : uiNodeActiveActions) {
			uiNodeQueueAction.deltaX = 0;
			uiNodeQueueAction.getRelativeX();
			uiNodeQueueAction.deltaY = 0;
			uiNodeQueueAction.getRelativeY();
			uiNodeQueueAction.transparency = EUIPlacement.NODE_TRANSPARENCY.value();
		}
		touchedElement = null;
		remove(index);
	}

	@Override
	public void reset(int index) {
		uiNodeActiveActions.get(index).deltaX = 0;
		uiNodeActiveActions.get(index).deltaY = 0;
		uiNodeActiveActions.get(index).transparency = EUIPlacement.NODE_TRANSPARENCY.value();
		
	}

	@Override
	public float initY() {
		return camera.position.y + nodeY;
	}
}
