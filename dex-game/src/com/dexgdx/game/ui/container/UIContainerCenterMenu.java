package com.dexgdx.game.ui.container;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.button.UIButtonInventoryOpen;

public class UIContainerCenterMenu extends UIContainer {
	private static final long serialVersionUID = 6228687225473299954L;

	private UIButtonInventoryOpen uiButtonInventoryOpen;
	
	public UIContainerCenterMenu(Game game, OrthographicCamera camera) {
		super(game, camera, null);
	}

	@Override
	protected void initialize() {
		height = EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_MARGIN.value() * 2;
		width = EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_MARGIN.value() * 2;
		camRelY = (
				(camera.viewportHeight / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_MARGIN.value())
				);
		camRelX = (
				(camera.viewportWidth / 2 * game.zoomRatio)
				- (
				EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_MARGIN.value() + 
				EUIPlacement.INVENTORY_BUTTON_OPEN_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_MARGIN.value())
				);
		setColor(Color.BLACK);
		transparency = EUIPlacement.INVENTORY_BUTTON_OPEN_CONTAINER_TRANSPARENCY.value();
	}

	@Override
	public void add(UIElement uiElement) {
		uiButtonInventoryOpen = (UIButtonInventoryOpen) uiElement;
	}
	
	@Override
	public UIElement getSingleElement() {
		return uiButtonInventoryOpen;
	}

	@Override
	protected void onClick() {

	}

	@Override
	public boolean foundTouchedElement(float x, float y){
		//uiButtonInventoryOpen.getRelativeX();
		//uiButtonInventoryOpen.getRelativeY();
		boolean contains = uiButtonInventoryOpen.contains(x, y);
		if(contains)
			touchedElement = uiButtonInventoryOpen;
		if(!contains)
			contains = super.contains(x, y);
		return contains;
	}

}
