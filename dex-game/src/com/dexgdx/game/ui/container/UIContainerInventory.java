package com.dexgdx.game.ui.container;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.inventory.Inventory;
import com.dexgdx.game.ui.UIElement;

import java.util.*;

import com.dexgdx.game.ui.button.*;
import com.dexgdx.game.object.*;

public class UIContainerInventory extends UIContainer {
	private static final long serialVersionUID = -1246415051490927048L;
	
	private PlayerActor playerActor;
	
	private List<UIButtonInventoryItem> uiButtonInventoryItemList;
	private UIButtonInventoryClose uiButtonInventoryClose;
	
	private boolean open;
	private boolean pausePrevState;

	public UIContainerInventory(Game game, OrthographicCamera camera) {
		super(game, camera, null);
		uiButtonInventoryItemList = new ArrayList<UIButtonInventoryItem>();
	}

	@Override
	protected void initialize() {
		height = 820;
		width = (EUIPlacement.INVENTORY_BUTTON_ITEM_SIZE.value() + EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value()) * Inventory.SIZE_X + EUIPlacement.INVENTORY_BUTTON_ITEM_MARGIN.value();//458;
		
		camRelY = (camera.viewportHeight / 2 * game.zoomRatio) * -1 + ((camera.viewportHeight * game.zoomRatio) - height) / 2;//(EUIPlacement.DIALOG_CONTAINER_LEFT_MARGIN.value());//- (EUIPlacement.DIALOG_CONTAINER_HEIGHT.value()*2 + EUIPlacement.DIALOG_CONTAINER_BOTTOM_MARGIN.value());
		camRelX = (camera.viewportWidth / 2 * game.zoomRatio) * -1 + ((camera.viewportWidth * game.zoomRatio) - width) / 2;
		
		setColor(Color.BLACK);
		wrapperImage = game.getTexture(EImagePath.CONTAINER_INVENTORY);
		transparency = EUIPlacement.INVENTORY_CONTAINER_TRANSPARENCY.value();
	}

	@Override
	public UIElement getByIndex(int index){
		return uiButtonInventoryItemList.get(index);
	}
	
	public void open(PlayerActor playerActor) {
		this.playerActor = playerActor;
		open = true;
		pausePrevState = game.paused;
		game.paused = true;
		deltaY = 0;
	}
	
	public PlayerActor getPlayerActor(){
		return playerActor;
	}

	public void close() {
		open = false;
		game.paused = pausePrevState;
		deltaY = camera.position.x + camera.viewportHeight / 2 + 10;
	}
	
	@Override
	public void add(UIElement uiElement) {
		if(uiElement instanceof UIButtonInventoryItem)
			uiButtonInventoryItemList.add((UIButtonInventoryItem) uiElement);
		else if(uiElement instanceof UIButtonInventoryClose)
			uiButtonInventoryClose = (UIButtonInventoryClose) uiElement;
	}

	public boolean isOpen(){
		return open;
	}
	
	@Override
	public UIElement getSingleElement() {
		return uiButtonInventoryClose;
	}

	@Override
	public boolean foundTouchedElement(float x, float y) {
		boolean contains = uiButtonInventoryClose.contains(x, y);
		if(open) {
			if(contains)
				touchedElement = uiButtonInventoryClose;
			if(!contains){
				for(UIButtonInventoryItem item : uiButtonInventoryItemList){
					item.getRelativeX();
					item.getRelativeY();
					contains = item.contains(x, y);
				}
			}
			if(!contains)
				contains = super.contains(x, y);
		}
		return contains;
	}

	@Override
	protected void onClick() {
		
	}
}
