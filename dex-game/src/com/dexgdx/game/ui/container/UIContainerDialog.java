package com.dexgdx.game.ui.container;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.dialog.UIDialogBox;

public class UIContainerDialog extends UIContainer {
	private static final long serialVersionUID = -1246415051490927048L;
	
	private UIDialogBox uiDialogBox;

	public UIContainerDialog(Game game, OrthographicCamera camera) {
		super(game, camera, null);
	}

	@Override
	protected void initialize() {
		height = EUIPlacement.DIALOG_CONTAINER_HEIGHT.value();
		width = (camera.viewportWidth * game.zoomRatio) - EUIPlacement.DIALOG_CONTAINER_LEFT_MARGIN.value() * 2;
		camRelY = (camera.viewportHeight / 2 * game.zoomRatio) - (EUIPlacement.DIALOG_CONTAINER_HEIGHT.value() + EUIPlacement.DIALOG_CONTAINER_BOTTOM_MARGIN.value());
		camRelX = (camera.viewportWidth / 2 * game.zoomRatio) * -1 + (EUIPlacement.DIALOG_CONTAINER_LEFT_MARGIN.value())
				;
		setColor(Color.BLACK);
		wrapperImage = game.getTexture(EImagePath.CONTAINER_DIALOG);
		transparency = EUIPlacement.DIALOG_CONTAINER_TRANSPARENCY.value();
	}
	
	@Override
	public void add(UIElement uiElement) {
		uiDialogBox = (UIDialogBox) uiElement;
		uiDialogBox.initFromContainer(this);
	}
	
	@Override
	public UIElement getSingleElement() {
		return uiDialogBox;
	}

	@Override
	public boolean foundTouchedElement(float x, float y) {
		boolean contains = false;
		if(uiDialogBox.active) {
			uiDialogBox.getRelativeX();
			uiDialogBox.getRelativeY();
			contains = uiDialogBox.contains(x, y);
			if(contains)
				touchedElement = uiDialogBox;
			if(!contains)
				contains = super.contains(x, y);
		}
		return contains;
	}

	@Override
	protected void onClick() {
	}

}
