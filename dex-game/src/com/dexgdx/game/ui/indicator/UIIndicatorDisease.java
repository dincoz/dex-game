package com.dexgdx.game.ui.indicator;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.container.UIContainerActorInfo;

public class UIIndicatorDisease extends UIElement {
	private static final long serialVersionUID = -5390156664917770243L;
	
	UIContainerActorInfo uiContainerActorInfo;

	public TextureRegion diseaseStartImage;
	public TextureRegion diseaseEndImage;
	public TextureRegion diseaseImage;
	public TextureRegion diseaseTube;

	public UIIndicatorDisease(Game game, OrthographicCamera camera,
			TextureRegion defaultImage, UIContainerActorInfo uiContainerActorInfo) {
		super(game, camera, defaultImage);
		this.uiContainerActorInfo = uiContainerActorInfo;
		
//		float imageRatio = (float)defaultImage.getRegionWidth() / (float)defaultImage.getRegionHeight();
		height = 14;
		width = 5;
		transparency = EUIPlacement.ACTORINFO_CONTAINER_TRANSPARENCY.value();

		diseaseImage = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_DISEASE);
		diseaseStartImage = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_DISEASE_START);
		diseaseEndImage = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_DISEASE_END);
	    diseaseTube = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_DISEASE_TUBE);
	}
	
	public float getTubeWidth() {
		return 72;
	}
	
	public float getDiseaseWidth() {
		return ((uiContainerActorInfo.actor.getInitialHitPoint() - uiContainerActorInfo.actor.getRemainingHitPoint()) / uiContainerActorInfo.actor.getInitialHitPoint()) * 72;
	}

	@Override
	protected void onClick() {
		
	}

	@Override
	protected void onReset() {
		
	}
	
	@Override
	public float getDeltaX() {
		return uiContainerActorInfo.getDeltaX() + EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value() + 35;
	}
	
	@Override
	public float getDeltaY() {
		return uiContainerActorInfo.getDeltaY() + EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value() + 18;
	}

	@Override
	protected void preTouchDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void preTouchUp() {
		// TODO Auto-generated method stub
		
	}

}
