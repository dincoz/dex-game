package com.dexgdx.game.ui.indicator;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.container.UIContainerActorInfo;

public class UIIndicatorBlood extends UIElement {
	private static final long serialVersionUID = -5390156664917770243L;
	
	UIContainerActorInfo uiContainerActorInfo;

//	public TextureRegion bloodStartImage;
//	public TextureRegion bloodEndImage;
	public TextureRegion bloodImage;
	public TextureRegion bloodTube;

	public UIIndicatorBlood(Game game, OrthographicCamera camera,
			TextureRegion defaultImage, UIContainerActorInfo uiContainerActorInfo) {
		super(game, camera, defaultImage);
		this.uiContainerActorInfo = uiContainerActorInfo;
		
//		float imageRatio = (float)defaultImage.getRegionWidth() / (float)defaultImage.getRegionHeight();
		height = 14;
		width = 5;
		transparency = EUIPlacement.ACTORINFO_CONTAINER_TRANSPARENCY.value();

		bloodImage = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_BLOOD);
//		bloodStartImage = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_BLOOD_START);
//		bloodEndImage = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_BLOOD_END);
	    bloodTube = game.getTexture(EImagePath.DISPLAY_ACTOR_INFO_BLOOD_TUBE);
	}
	
	public float getTubeWidth() {
		return 72;
	}
	
	public float getBloodWidth() {
		return (uiContainerActorInfo.actor.getRemainingHitPoint() / uiContainerActorInfo.actor.getInitialHitPoint()) * 72;
	}

	@Override
	protected void onClick() {
		
	}

	@Override
	protected void onReset() {
		
	}
	
	@Override
	public float getDeltaX() {
		return uiContainerActorInfo.getDeltaX() + EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value() + 35;
	}
	
	@Override
	public float getDeltaY() {
		return uiContainerActorInfo.getDeltaY() + EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value();
	}

	@Override
	protected void preTouchDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void preTouchUp() {
		// TODO Auto-generated method stub
		
	}

}
