package com.dexgdx.game.ui.indicator;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EUIPlacement;
import com.dexgdx.game.ui.UIElement;
import com.dexgdx.game.ui.container.UIContainerActorInfo;

public class UIIndicatorMan extends UIElement {
	private static final long serialVersionUID = 2511165804907162342L;
	
	UIContainerActorInfo uiContainerActorInfo;

	public UIIndicatorMan(Game game, OrthographicCamera camera,
			TextureRegion defaultImage, UIContainerActorInfo uiContainerActorInfo) {
		super(game, camera, defaultImage);
		this.uiContainerActorInfo = uiContainerActorInfo;
		
		float imageRatio = (float)defaultImage.getRegionWidth() / (float)defaultImage.getRegionHeight();
		height = uiContainerActorInfo.height - EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value() * 2 - EUIPlacement.ACTORINFO_CONTAINER_TRIANGLE_HEIGHT.value();
		width = height * imageRatio;
		transparency = EUIPlacement.ACTORINFO_CONTAINER_TRANSPARENCY.value();
		
	}

	@Override
	protected void onClick() {

	}

	@Override
	protected void onReset() {

	}
	
	@Override
	public float getDeltaX() {
		return uiContainerActorInfo.getDeltaX() + EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value();
	}
	
	@Override
	public float getDeltaY() {
		return uiContainerActorInfo.getDeltaY() + EUIPlacement.ACTORINFO_MAN_DISPLAY_MARGIN.value();
	}

	@Override
	protected void preTouchDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void preTouchUp() {
		// TODO Auto-generated method stub
		
	}

}
