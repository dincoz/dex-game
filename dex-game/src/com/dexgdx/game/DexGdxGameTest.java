package com.dexgdx.game;

import java.util.List;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.dexgdx.game.constants.EGamePlatform;
import com.dexgdx.game.db.core.AbstractDataHandler;
import com.dexgdx.game.input.CameraPanController;
import com.dexgdx.game.input.CameraRelativePointCalculator;
import com.dexgdx.game.input.DexConeSwipeController;
import com.dexgdx.game.manager.DrawManager;
import com.dexgdx.game.object.*;

public class DexGdxGameTest extends ApplicationAdapter implements GestureListener, InputProcessor {
	
	private Game game;
    AbstractDataHandler dbHandler;
	
	private OrthographicCamera camera;
    private ShapeRenderer renderer;
    private SpriteBatch spriteBatch;
    private DrawManager draw;
	
    private BitmapFont font;
    private CameraRelativePointCalculator camRelativePointCalculator;
    private DexConeSwipeController dexConeSwipeController;
	float zoomRatio;
	
	BaseRegion actorRing;

    private CameraPanController camPanController;

	String message;
	
	public DexGdxGameTest(float zoomRatio, EGamePlatform gamePlatform, AbstractDataHandler dbHandler) {
		super();
		this.dbHandler = dbHandler;
	}
    
	@Override
	public void create () { 
		game = new Game(dbHandler);
		
		renderer = new ShapeRenderer();
		spriteBatch = new SpriteBatch();
		draw = new DrawManager(spriteBatch, renderer);

		camera = new OrthographicCamera();
		camera.setToOrtho(true);
		camera.zoom = zoomRatio;

	    float westLimit = Gdx.graphics.getWidth() / 2 * zoomRatio;
	    float eastLimit = 1920 - westLimit;
	    float northLimit = Gdx.graphics.getHeight() / 2 * zoomRatio;
	    float southLimit = 1080 - northLimit;

        camPanController = new CameraPanController(westLimit, eastLimit, northLimit, southLimit, camera, game.zoomRatio);
		font = new BitmapFont(Gdx.files.internal("data/fonts/roboto.fnt"),false);
        font.setColor(Color.WHITE);
        font.setScale(0.5f, -0.5f);
        
        camRelativePointCalculator = new CameraRelativePointCalculator(camera);
        dexConeSwipeController = new DexConeSwipeController();
		
        InputMultiplexer im = new InputMultiplexer();
        GestureDetector gd = new GestureDetector(this);
        im.addProcessor(gd);
        im.addProcessor(this);
        Gdx.input.setInputProcessor(im);
        
		renderer.setProjectionMatrix(camera.combined);
		spriteBatch.setProjectionMatrix(camera.combined);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.graphics.setContinuousRendering(true);

		dexConeSwipeController.reset(5, game.gameObjects.getPlayerList().get(0));
		
//		actorRing = new BaseRegion(camera.position.x, camera.position.y, 40, 50);
//		actorCenter = new BasePoint((actorRing.x + actorRing.width / 2), (actorRing.y + actorRing.height / 2));
//
//		coneRings = new ArrayList<BaseRegion>();
//		coneRings.add(new BaseRegion(0, 0, actorRing.height, actorRing.width));
//		coneRings.add(new BaseRegion(0, 0, actorRing.height, actorRing.width));
//		coneRings.add(new BaseRegion(0, 0, actorRing.height, actorRing.width));
//		coneRings.add(new BaseRegion(0, 0, actorRing.height, actorRing.width));
//		coneRings.add(new BaseRegion(0, 0, actorRing.height, actorRing.width));
//		coneRings.add(new BaseRegion(0, 0, actorRing.height, actorRing.width));
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		camera.position.set(camPanController.screenLimitData.getWestLimit(), camPanController.screenLimitData.getNorthLimit(), 0);
	}

	
	List<BaseRegion> walkPath;
//	BaseRegion actorRing;
//	BasePoint actorCenter;
//	List<BaseRegion> coneRings;
	
	@Override
	public void render () {
	    Gdx.gl.glClearColor(0,.2f,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		renderer.setProjectionMatrix(camera.combined);
		spriteBatch.setProjectionMatrix(camera.combined);
		
		draw.renderer(ShapeType.Line)
			.ring(Color.GREEN, game.gameObjects.getPlayerList().get(0))
		.end();
		if(dexConeSwipeController.swiping) {
			for (int i = 0; i < dexConeSwipeController.size(); i++) {
				draw.renderer(ShapeType.Line)
					.ring(Color.GREEN, dexConeSwipeController.get(i))
				.end();
			}
		}
		
        font.setColor(Color.WHITE);
		spriteBatch.begin();
		message = "";
        font.drawMultiLine(spriteBatch, message, camera.position.x - zoomRatio * camera.viewportWidth / 2 + 5, camera.position.y - zoomRatio * camera.viewportHeight / 2 + 10);
        spriteBatch.end();
	}
	
	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}
	
	public boolean touchDragged(int x, int y, int pointer) {
		screenPressPosition = camRelativePointCalculator.calc(x, y);
		dexConeSwipeController.swipe(screenPressPosition.x, screenPressPosition.y);
		
		return false;
	}
	
	boolean screenPressed;
	float screenPressedCounter = 0;
	BasePoint screenPressPosition = null;
	
	public boolean touchDown(int x, int y, int pointer, int button) {
		screenPressPosition = camRelativePointCalculator.calc(x, y);
		dexConeSwipeController.swipe(screenPressPosition.x, screenPressPosition.y);
		screenPressed = true;
		return false;
	}
	
	public boolean touchUp(int x, int y, int pointer, int button) {
		screenPressPosition = null;
		dexConeSwipeController.swiping = false;
		screenPressed = false;
		return false;
	}

	
	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		
		return false;
	}
	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		screenPressPosition = camRelativePointCalculator.calc(x, y);
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
	}
}
