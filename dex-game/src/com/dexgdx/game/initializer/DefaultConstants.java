package com.dexgdx.game.initializer;

public class DefaultConstants {

	private int actorRingWidth;
	private int actorRingHeight;
	private float playerSpeed;
	
	private int dexMissileRingWidth;
	private int dexMissileRingHeight;
	private float dexMissileSpeed;
	
	private int dexTrailRingWidth;
	private int dexTrailRingHeight;
	private float dexTrailSpeed;
	
	private int bulletRingWidth;
	private int bulletRingHeight;
	private float bulletSpeed;
	
	public DefaultConstants(
			int actorRingWidth, 
			int actorRingHeight,
			float playerSpeed,
			int dexMissileRingWidth,
			int dexMissileRingHeight, 
			float dexMissileSpeed, 
			int dexTrailRingWidth,
			int dexTrailRingHeight, 
			float dexTrailSpeed,
			int bulletRingWidth,
			int bulletRingHeight, 
			float bulletSpeed) {
		
		super();
		this.actorRingWidth = actorRingWidth;
		this.actorRingHeight = actorRingHeight;
		this.playerSpeed = playerSpeed;
		
		this.dexMissileRingWidth = dexMissileRingWidth;
		this.dexMissileRingHeight = dexMissileRingHeight;
		this.dexMissileSpeed = dexMissileSpeed;
		
		this.dexTrailRingWidth = dexTrailRingWidth;
		this.dexTrailRingHeight = dexTrailRingHeight;
		this.dexTrailSpeed = dexTrailSpeed;
		
		this.bulletRingWidth = bulletRingWidth;
		this.bulletRingHeight = bulletRingHeight;
		this.bulletSpeed = bulletSpeed;
	}
	
	public int getActorRingWidth() {
		return actorRingWidth;
	}
	public int getActorRingHeight() {
		return actorRingHeight;
	}
	public float getPlayerSpeed() {
		return playerSpeed;
	}
	public float getDexMissileSpeed() {
		return dexMissileSpeed;
	}
	public float getDexTrailSpeed() {
		return dexTrailSpeed;
	}
	public int getDexMissileRingHeight() {
		return dexMissileRingHeight;
	}
	public int getDexMissileRingWidth() {
		return dexMissileRingWidth;
	}
	public int getDexTrailRingWidth() {
		return dexTrailRingWidth;
	}
	public int getDexTrailRingHeight() {
		return dexTrailRingHeight;
	}
	public int getBulletRingWidth() {
		return bulletRingWidth;
	}
	public int getBulletRingHeight() {
		return bulletRingHeight;
	}
	public float getBulletSpeed() {
		return bulletSpeed;
	}
}
