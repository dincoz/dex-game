package com.dexgdx.game.object;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.constants.EObjectInfo;

public class BulletObject extends MobileObject{

	private static final long serialVersionUID = 7257081440647166830L;
	
	public Trajectory trajectory;

	public BulletObject(Game game, long objectInstOid,float x, float y, float height, float width) {
//		super(game, id, x, y, height, width, 
//			  game.defaultConstants.getDexMissileSpeed() * 3, 
//			  false, EObjectId.BULLET.getObjectId());
		super(game, EObjectInfo.BULLET.getObjectInfo(), objectInstOid, x, y, height, width, -30, -70, 70,
				70, game.defaultConstants.getDexMissileSpeed() * 3, false);
		initialize();
	}


	private void initialize(){
		setCollidingObject(false);
	}
	
	public void initializePosition(Trajectory trajectory){
		this.trajectory = trajectory;
	}

	public boolean isAnimationComplete(){
		boolean isComplete = true;
		return isComplete;
	}
	
	public AbstractActor getHitActor(){
		if(blockTarget instanceof AbstractActor){
			return (AbstractActor) blockTarget;
		}
		return null;
	}
	
	@Override
	public EMoveDirection getMoveDirection() {
		return EMoveDirection.SOUTH;
	}
}
