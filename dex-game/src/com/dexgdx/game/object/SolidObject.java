package com.dexgdx.game.object;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.action.AbstractAction;
import com.dexgdx.game.action.ActionFactory;
import com.dexgdx.game.action.MovementAction;
import com.dexgdx.game.action.runner.ActionRunner;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.util.ObjectInfo;

public class SolidObject extends BaseRegion{
	
	private static final long serialVersionUID = 7222567625350253896L;
	
	protected Game game;

	protected ObjectInfo objectInfo;
	protected long objectInstOid;
	
	protected TextureRegion imageTexture;
	protected BaseRegion imageRelativeRegion;
	protected BaseRegion imageCurrentRegion;

	protected ActionRunner actionRunner;	//dex missile has actions too...
	protected ActionFactory actionFactory;
	protected List<EActionType> possibleActiveActions;
	protected List<EActionType> possiblePassiveActions;

	protected float initialHitPoint;		//Doors-barricades can have hit points...
	protected float remainingHitPoint;
	
	protected SolidObject attackingTarget;
	protected boolean attacking;
	
	protected boolean destroyed;
	protected boolean invisible;

	private BaseRegion touchRegion;

	SolidObject(Game game, ObjectInfo objectInfo, long objectInstOid,
			float x, float y, float height, float width, 
			boolean solidObject) {
		
		super(x, y, height, width);

		initialize(game, objectInfo, objectInstOid, solidObject);
	}
	
	SolidObject(Game game, ObjectInfo objectInfo, long objectInstOid,
			float x, float y, float height, float width, 
			float imageDeltaX, float imageDeltaY, float imageHeight, float imageWidth, 
			boolean solidObject) {
		
		super(x, y, height, width);
		
		initialize(game, objectInfo, objectInstOid, solidObject);
		
		setImageRelativeRegion(imageDeltaX, imageDeltaY, imageHeight, imageWidth);
	}

	public List<EActionType> getPossibleActiveActions(){
		return possibleActiveActions;
	}
	
	public List<EActionType> getPossiblePassiveActions(){
		return possiblePassiveActions;
	}
	 
	private void initialize(Game game, ObjectInfo objectInfo, long objectInstOid, boolean solidObject){
		
		this.game = game;
		this.collidingObject = solidObject;
		this.objectInfo = objectInfo;
		this.objectInstOid = objectInstOid;

		actionFactory = new ActionFactory(game, this);
		actionRunner = new ActionRunner(game, this);

		imageCurrentRegion = new BaseRegion(0, 0, 0, 0);
		touchRegion = new BaseRegion(x, y, height + 8 * height / 9, width + 8 * width /9 );
		
		remainingHitPoint = 5;

		possibleActiveActions = new ArrayList<EActionType>();
		possiblePassiveActions = new ArrayList<EActionType>();
		
		collidingObject = true;
	}
	
	public void addPossibleActiveAction(EActionType actionType){
		if(!possibleActiveActions.contains(actionType))
			possibleActiveActions.add(actionType);
	}
	
	public void addPossiblePassiveAction(EActionType actionType){
		if(!possiblePassiveActions.contains(actionType))
			possiblePassiveActions.add(actionType);
	}
	
	public long getObjectId() {
		return objectInstOid;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SolidObject)
			return ((SolidObject)obj).getObjectId() == getObjectId();
		return false;
	}
	
	public ObjectInfo getObjectInfo() {
		return objectInfo;
	}

	public TextureRegion getImageTextureRegion() {
		return imageTexture;
	}

	public void loadImageTextureByFrame(int animIndex){
		imageTexture = game.getAnimationManager().getAnimationByKey(
				getObjectInfo().getInfoNumber(), 1, 1).getKeyFrame(game.stateTime + animIndex, true);
	}

	public void loadImageTextureByStopFrame(){
		//MobileObject i�in jenerik ama�l�
		loadImageTextureByFrame(1);
	}
	
	protected void setImageRelativeRegion(float deltaX, float deltaY, float imageHeight, float imageWidth){
        imageRelativeRegion = new BaseRegion(deltaX, deltaY, imageHeight, imageWidth);
	}
	
	public BaseRegion getImageCurrentRegion(){
		if(imageRelativeRegion==null){
			imageCurrentRegion.setFromRegion(this);
		}
		else {
			imageCurrentRegion.setFromRegion(imageRelativeRegion);
	    	imageCurrentRegion.x += x;
	    	imageCurrentRegion.y += y;
		}
		return imageCurrentRegion;
	}

	public ActionFactory getActionFactory() {
		return actionFactory;
	}

	public void hit(SolidObject targetObject, boolean async){
		addAction(actionFactory.hit(targetObject, 1));
	}
	
	public void startAttacking(SolidObject attackingTarget){
		this.attackingTarget = attackingTarget;
		setAttacking(true);
	}
	public void stopAttacking(){
		this.attackingTarget = null;
		setAttacking(false);
	}

	public boolean isAttacking() {
		return attacking;
	}

	public void setAttacking(boolean attacking) {
		this.attacking = attacking;
	}
	
	public SolidObject getAttackingTarget() {
		return attackingTarget;
	}
	public void nextMove(){
//		enlistedActions.runNextStep();
		actionRunner.run();
	}
	
	public void addAction(AbstractAction action){
//		enlistedActions.addAction(action);
		actionRunner.addPotentialAction(action);
	}
	
	public void addAction(MovementAction action){
//		enlistedActions.addAction(action);
		actionRunner.addActiveAction(action);
	}
	
	public int getActiveActionCount(){
		return actionRunner.getActiveActionRunner().getActionList().size();
	}
	
	public int getPotentialActionCount(){
		return actionRunner.getPotentialActionRunner().getActionList().size();
	}
	
	public AbstractAction getCurrentActiveAction(){
		if(getActiveActionCount() > 0)
			return actionRunner.getActiveActionRunner().getActionList().get(0);
		return null;
	}
	
	public void clearAllActions() {
		actionRunner.clearActiveActions();
		actionRunner.clearPotentialActions();
	}

	public void clearMoveBlockedActions(){
		actionRunner.clearMoveBlockedActions();
	}
	
	public boolean isDestroyed() {
		return destroyed;
	}

	public void setInvisible(boolean invisible) {
		this.invisible = invisible;
	}

	public void destroy() {
		this.destroyed = true;
	}
	
	public float getRemainingHitPoint() {
		return remainingHitPoint;
	}

	public float getInitialHitPoint() {
		return initialHitPoint;
	}

	public void raiseHitPoint(float healPoint) {
		if(remainingHitPoint + healPoint >= initialHitPoint)
			remainingHitPoint = initialHitPoint;
		else
			remainingHitPoint += healPoint;
	}
	public void lowerHitPoint(float attackPoint) {
		if(remainingHitPoint >= attackPoint)
			remainingHitPoint -= attackPoint;
		else
			remainingHitPoint = 0;
	}
	
	public BaseRegion getTouchRegion(){
		touchRegion.x = x - 4 * width / 9;
		touchRegion.y = y - 4 * height / 9;
		return touchRegion;
	}
	
	public ActionRunner getActionRunner() {
		return actionRunner;
	}

	@Override
	public int hashCode() {
		return super.hashCode() + (int)objectInstOid;
	}
	
}
