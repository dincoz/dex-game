package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.util.ObjectInfo;

public class InventoryItem extends SolidObject {

	private static final long serialVersionUID = -1916922901600616825L;
	
	public TextureRegion inventoryImage;

	InventoryItem(Game game, ObjectInfo objectInfo, long objectInstOid, TextureRegion inventoryImage, float x, float y, float height,
			float width) {
		super(game, objectInfo, objectInstOid, x, y, height, width, -10, -10, 64, 64, false);
		this.inventoryImage = inventoryImage;//game.getTexture(EImagePath.INVENTORY_SWORD);
	}
	
	public void pickedAction(){
		
	}
}
