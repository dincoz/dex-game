package com.dexgdx.game.object;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

public class TargetCursor extends BaseRegion {

	private static final long serialVersionUID = 2867371792593143370L;

	private static final int MAX_OFFSET = 5;
	private static final int MIN_OFFSET = 0; 
	private static final int WAIT_TIME = 2; 
	
	private BasePoint[] tri1;
	private BasePoint[] tri2;
	private BasePoint[] tri3;
	private BasePoint[] tri4;
	private BasePoint center;

	private int prevOffset;
	private int offset;
	private int waitCount;
	
	private Color color;

	public TargetCursor(float x, float y, float height, float width) {
		super(x, y, height, width);
		offset = 4;
		prevOffset = offset - 1;
		waitCount = 0;
		tri1 = new BasePoint[2];
		tri2 = new BasePoint[2];
		tri3 = new BasePoint[2];
		tri4 = new BasePoint[2];
		center = new BasePoint();
		String hexCode = Integer.toHexString(offset * 3);
		hexCode = hexCode.concat(hexCode);
		color = Color.valueOf(hexCode + "FF" + hexCode);
	}
	
	public TargetCursor getNext(){
		if(offset == MIN_OFFSET){
			prevOffset = MIN_OFFSET;
			offset = prevOffset + 1;
		}
		else if(offset == MAX_OFFSET){
			prevOffset = MAX_OFFSET;
			offset = prevOffset - 1;
		}
		center.x = x + width / 2;
		center.y = y + height / 2;
		
		tri1[0] = new BasePoint(x + offset, y + height / 4 + offset);
		tri1[1] = new BasePoint(x + width / 4 + offset, y + offset);

		tri2[0] = new BasePoint(x + offset, y + 3 * height / 4 - offset);
		tri2[1] = new BasePoint(x + width / 4 + offset, y + height - offset);

		tri3[0] = new BasePoint(x + width - offset, y + height / 4 + offset);
		tri3[1] = new BasePoint(x + 3 * width / 4 - offset, y + offset);

		tri4[0] = new BasePoint(x + width - offset, y + 3 * height / 4 - offset);
		tri4[1] = new BasePoint(x + 3 * width / 4 - offset, y + height - offset);
		
		if(waitCount == WAIT_TIME){
			waitCount = -1;
			if(offset < prevOffset){
				offset--;
				prevOffset--;
			}
			else {
				offset++;
				prevOffset++;
			}
			String hexCode = Integer.toHexString(offset * 3);
			hexCode = hexCode.concat(hexCode);
			color = Color.valueOf(hexCode + "FF" + hexCode);
		}

		waitCount++;
		
		Gdx.graphics.requestRendering();
		return this;
	}

	public BasePoint[] getTri1() {
		return tri1;
	}

	public BasePoint[] getTri2() {
		return tri2;
	}

	public BasePoint[] getTri3() {
		return tri3;
	}

	public BasePoint[] getTri4() {
		return tri4;
	}

	public BasePoint getCenter() {
		return center;
	}

	public Color getColor() {
		return color;
	}
	
	public static void main(String[] args) {
		System.out.print(Integer.toHexString(1));
	}
	
}
