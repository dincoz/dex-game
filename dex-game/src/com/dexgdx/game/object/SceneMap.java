package com.dexgdx.game.object;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.util.ObjectInfo;

public class SceneMap implements IObject {

	private ObjectInfo objectInfo;
	private long objectInstOid;
	
	private List<TextureRegion> imageOverlays;
	
	public SceneMap(ObjectInfo objectInfo, long objectInstOid, TextureRegion mainImage) {
		super();
		this.objectInfo = objectInfo;
		this.objectInstOid = objectInstOid;
		imageOverlays = new ArrayList<TextureRegion>();
		imageOverlays.add(mainImage);
	}
	
	public void addImage(TextureRegion image) {
		imageOverlays.add(image);
	}

	@Override
	public ObjectInfo getObjectInfo() {
		return objectInfo;
	}

	@Override
	public long getObjectInstOid() {
		return objectInstOid;
	}

	public List<TextureRegion> getImageOverlays() {
		return imageOverlays;
	}
	
	public TextureRegion getImageOverlay(int index) {
		return imageOverlays.get(index);
	}
	
	public TextureRegion getMainImage() {
		return imageOverlays.get(0);
	}
}
