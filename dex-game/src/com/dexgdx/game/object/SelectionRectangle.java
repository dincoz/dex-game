package com.dexgdx.game.object;

import com.badlogic.gdx.math.Intersector;
import com.dexgdx.game.Game;
import com.dexgdx.game.object.BaseRegion;
import java.util.*;

public class SelectionRectangle extends BaseRegion {
	private static final long serialVersionUID = 1552626677620249177L;
	
	private Game game;
	private boolean visible;

	private float startPointX;
	private float startPointY;
	
	private List<PlayerActor> resizeSelectedPlayers;
	
	public SelectionRectangle(Game game) {
		super(0,0,0,0);
		this.game = game;
		
		visible = false;
		resizeSelectedPlayers = new ArrayList<PlayerActor>();
	}

	public boolean visible() {
		return visible;
	}

	public void setVisible(float startPointX, float startPointY) {
		this.visible = true;
		this.startPointX = startPointX;
		this.startPointY = startPointY;
	}

	public void setInvisible() {
		this.visible = false;
		endResize();
	}

	public float getStartPointX() {
		return startPointX;
	}

	public float getStartPointY() {
		return startPointY;
	}
	
	public void resize(BasePoint relPoint){
		x = relPoint.x < startPointX ? relPoint.x : startPointX;
		y = relPoint.y < startPointY ? relPoint.y : startPointY;
		
		width = Math.abs(startPointX - relPoint.x);
		height = Math.abs(startPointY - relPoint.y);

		for(PlayerActor playerActor : game.gameObjects.getPlayerList()){
			if(!playerActor.isDestroyed() && Intersector.overlaps(this, playerActor)){
				if(!resizeSelectedPlayers.contains(playerActor)){
					resizeSelectedPlayers.add(playerActor);
				}
			}
			else {
				resizeSelectedPlayers.remove(playerActor);
			}
		}
	}
	public void endResize(){
		if(resizeSelectedPlayers.size() > 0){
			game.gameObjects.deSelectAllPlayers();
		}
		for(PlayerActor playerActor : resizeSelectedPlayers){
			game.gameObjects.selectPlayer(playerActor, true);
		}
		resizeSelectedPlayers.clear();
	}
}
