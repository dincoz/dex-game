package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EColor;
import com.dexgdx.game.util.ObjectInfo;

public class NeutralActor extends AbstractActor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7897141506091151882L;

	NeutralActor(Game game, ObjectInfo objectInfo, long id, float x,
			float y, float height, float width, float movementSpeed,
			TextureRegion portrait) {
		super(game, objectInfo, id, x, y, height, width, movementSpeed, portrait);
		remainingHitPoint = 10;
		initialHitPoint = 10;
	}

	@Override
	public EColor getRingColor() {
		return EColor.NEUTRAL_ACTOR;
	}

	@Override
	public EColor getOriginalRingColor() {
		return EColor.NEUTRAL_ACTOR;
	}

	@Override
	public void runDefaultAction(AbstractActor targetActor) {
		// TODO Auto-generated method stub

	}

}
