package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EColor;
import com.dexgdx.game.util.ObjectInfo;

public class PlayerActor extends AbstractActor {
	
	private static final long serialVersionUID = -4576553117998997710L;
	
	private TargetCursor targetCursor;
	
	public EActionType selectedActiveAttackAction;
	public EActionType selectedActiveDefenseAction;

	PlayerActor(Game game, ObjectInfo objectInfo, long objectInstOid, float x, float y, float height, float width, float movementSpeed, TextureRegion portrait) {
		super(game, objectInfo, objectInstOid, x, y, height, width, movementSpeed, portrait);
		
		targetCursor = new TargetCursor(0,0,0,0);
		remainingHitPoint = 30;
		initialHitPoint = 30;

		selectedActiveAttackAction = EActionType.OTHER;
		selectedActiveDefenseAction = EActionType.OTHER;
	}
	
	@Override
	public EColor getRingColor() {
		EColor color = null;
		if(isSelected()){
			color = EColor.SELECTED_PLAYER;
		}
		else {
			color = EColor.UNSELECTED_PLAYER;
		}
		return color;
	}

	@Override
	public EColor getOriginalRingColor() {
		return EColor.UNSELECTED_PLAYER;
	}

	public TargetCursor getTargetCursor() {
		if(this.getMoveTarget() != null)
			targetCursor.setFromRegion(this.getMoveTarget());
		else
			targetCursor = null;
		return targetCursor;
	}

	@Override
	public void destroy() {
		super.destroy();
		game.gameObjects.deSelectPlayer(this);
	}

	@Override
	public void runDefaultAction(AbstractActor targetActor) {
		// TODO Auto-generated method stub
		
	}
}
