package com.dexgdx.game.object;

//import com.dexgdx.game.Game;
//import com.dexgdx.game.scenario.ScenarioTrigger;
//import com.dexgdx.game.util.ObjectInfo;
//
//public class ScenarioInventoryItem extends InventoryItem{
//	private static final long serialVersionUID = 1L;
//	
//	ScenarioTrigger scenarioTrigger;
//
//	ScenarioInventoryItem(Game game, ObjectInfo objectInfo, long objectInstOid, long scenarioInstOid, float x, float y,
//			float height, float width, boolean solidObject) {
//		super(game, objectInfo, objectInstOid, x, y, height, width);
//		scenarioTrigger = new ScenarioTrigger(objectInstOid, scenarioInstOid);
//	}
//	
//	@Override
//	public void pickedAction(){
//		super.pickedAction();
//		scenarioTrigger.scenarioItemTriggered();
//	}
//	
//}