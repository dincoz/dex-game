package com.dexgdx.game.object;

public class Trajectory extends BasePoint {
	
	private float speed;
	private float absSpeed;
	private float directionSpeed;
	
	public Trajectory(float speed, float absSpeed,float directionSpeed) {
		super();
		this.speed = speed;
		this.absSpeed = absSpeed;
		this.directionSpeed = directionSpeed;
	}

	public Trajectory(float speed) {
		super();
		this.speed = speed;
		this.absSpeed = -1000f; 
		this.directionSpeed = -1000f;
	}

	public float getSpeed() {
		return speed;
	}
	
	public void setSpeed(float speed) {
		this.speed = speed;
		this.absSpeed = -1000f; 
		this.directionSpeed = -1000f;
	}

	public float getAbsSpeed() {
		if(absSpeed == -1000)
			absSpeed = Math.abs(speed);
		return absSpeed;
	}

	public float getDirectionSpeed() {
		if(directionSpeed == -1000)
			directionSpeed = (float) Math.sqrt(2 * (speed * speed));
		return directionSpeed;
	}
	
}
