package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.DexMissileObject;
import com.dexgdx.game.util.ObjectInfo;

public class ObjectFactory {
	
	private Game game;
	private long idCounter;
	
	public ObjectFactory(Game game) {
		super();
		this.game = game;
		
		idCounter = 0;
	}
	
	private synchronized long getNextNum(){
		idCounter++;
		return idCounter;
	}
	
	public SceneMap map(ObjectInfo mapObjectInfo, TextureRegion image) {
		return new SceneMap(mapObjectInfo, getNextNum(), image);
	}

	public BaseRegion region(
			float x, float y, float height, float width){
		return new BaseRegion(x, y, height, width);
	}
	
	public InventoryItem inventoryItem(ObjectInfo objectInfo, TextureRegion inventoryImage, float x, float y, float height,
			float width) {
		InventoryItem inventoryItem = new InventoryItem(game, objectInfo, getNextNum(), inventoryImage, x, y, height, width);
		return inventoryItem;
	}
	
	public DexMissileObject dexMissile(){
		DexMissileObject dexMissileObject = new DexMissileObject(game, getNextNum(), 0, 0, game.defaultConstants.getActorRingHeight(), game.defaultConstants.getActorRingWidth());
		return dexMissileObject;
	}
	
	public BulletObject bullet(){
		BulletObject bulletObject = new BulletObject(
				game, getNextNum(), 0, 0, 
				game.defaultConstants.getActorRingHeight()/5, 
				game.defaultConstants.getActorRingWidth()/5);
		return bulletObject;
	}
	
	public DexTrailObject dexTrail(AbstractActor actor){
		DexTrailObject dexTrailObject = new DexTrailObject(game, getNextNum(), actor, actor.x, actor.y);
		return dexTrailObject;
	}
	
	public PlayerActor player(ObjectInfo objectInfo, float x, float y, float height, float width, float movementSpeed, TextureRegion portrait){
		PlayerActor playerActor = new PlayerActor(game, objectInfo, getNextNum(), x, y, height, width, movementSpeed, portrait);
		return playerActor;
	}
	
	public EnemyActor enemy(ObjectInfo objectInfo, float x, float y, float height, float width, float movementSpeed, TextureRegion portrait){
		EnemyActor enemyActor = new EnemyActor(game, objectInfo, getNextNum(), x, y, height, width, movementSpeed, portrait);
		return enemyActor;
	}
	
	public NeutralActor neutralActor(ObjectInfo objectInfo, float x, float y, float height, float width, float movementSpeed, TextureRegion portrait){
		NeutralActor neutralActor = new NeutralActor(game, objectInfo, getNextNum(), x, y, height, width, movementSpeed, portrait);
		return neutralActor;
	}
}
