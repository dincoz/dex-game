package com.dexgdx.game.object;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.RegionAttackActionTrigger;
import com.dexgdx.game.constants.EObjectInfo;
import com.dexgdx.game.constants.EParticleType;
public class DexTrailObject extends MobileObject{

	private static final long serialVersionUID = 8964321500584743195L;
	
	private AbstractActor owningActor;

	private List<DexEffectObject> dexEffectObjects; 
	private List<RegionAttackActionTrigger> dexEffectActionTriggers; 
	private List<BasePoint> pathPositions;
	
	private Random random;
	
	public DexTrailObject(Game game, long objectInstOid, AbstractActor owningActor, float x, float y) {
		super(game, EObjectInfo.DEX_TRAIL.getObjectInfo(), objectInstOid, x, y, 
				game.defaultConstants.getDexTrailRingHeight(), 
				game.defaultConstants.getDexTrailRingWidth(), 
				game.defaultConstants.getDexTrailSpeed(), 
				false);
		this.owningActor = owningActor;
		dexEffectObjects = new ArrayList<DexEffectObject>();
		dexEffectActionTriggers = new ArrayList<RegionAttackActionTrigger>();
		DexEffectObject dexEffectObject= new DexEffectObject(
				game.getParticleManager().obtainFromParticleEffectPool(EObjectInfo.DEX_TRAIL), EParticleType.TRAIL); 
		dexEffectObject.setPosition(x, y);
		addDexEffectObject(dexEffectObject);
		random = new Random();
		pathPositions = new ArrayList<BasePoint>();
		pathPositions.add(new BasePoint(x, y));
		setCollidingObject(false);
	}
	
	private void addDexEffectObject(DexEffectObject dexEffectObject){
		dexEffectObjects.add(dexEffectObject);
		RegionAttackActionTrigger attackActionTrigger = new RegionAttackActionTrigger(game, this, owningActor, dexEffectObject);
		game.addRegionActionTrigger(attackActionTrigger);
		dexEffectActionTriggers.add(attackActionTrigger);
	}
	
	private DexEffectObject removeDexEffectObject(int index){
		DexEffectObject dexEffectObject = dexEffectObjects.remove(index);
		game.removeRegionActionTrigger(dexEffectActionTriggers.remove(index));
		return dexEffectObject;
	}
	
	public void removeDexEffectObject(DexEffectObject dexEffectObject){
		int index = dexEffectObjects.indexOf(dexEffectObject);
		dexEffectObjects.remove(dexEffectObject);
		game.removeRegionActionTrigger(dexEffectActionTriggers.remove(index));
	}

	public void initializePosition(){
		dexEffectObjects.get(0).setPosition(x, y);
	}
	
	public void setDexEffectPosition(){
		DexEffectObject dexEffectObject = null;
		if(dexEffectObjects.size() < 7){
			dexEffectObject = new DexEffectObject(
					game.getParticleManager().obtainFromParticleEffectPool(EObjectInfo.DEX_TRAIL), EParticleType.TRAIL);
		}
		else {
			dexEffectObject = removeDexEffectObject(0);
		}
		dexEffectObject.setPosition(x, y);
		addDexEffectObject(dexEffectObject);

		if(random.nextInt(5) > 2 && pathPositions.size() > 1){
			dexEffectObject = new DexEffectObject(
					game.getParticleManager().obtainFromParticleEffectPool(EObjectInfo.DEX_TRAIL), EParticleType.TRAIL);
			BasePoint basePoint = pathPositions.get(random.nextInt(pathPositions.size() - 1));
			dexEffectObject.setPosition(basePoint.x, basePoint.y);
			addDexEffectObject(dexEffectObject);
		}

		pathPositions.add(new BasePoint(x, y));
		
	}
	
	public boolean areAllParticlesDead(){
		boolean dead = true;
		for (DexEffectObject dexEffectObject : dexEffectObjects) {
			if(!dexEffectObject.isComplete()){
				dead = false;
				break;
			}
		}
		return dead;
	}

//	public void dexTrailAttack(AbstractActor ownerActor, BaseRegion targetRegion, boolean async){
//		addAction(actionFactory.dexTrailAttack(ownerActor, targetRegion, async));
//	}

	public void finalizeObject(){
		dexEffectObjects.clear();
	}
	
	public int getPooledEffectCount(){
		return dexEffectObjects.size();
	}
	
	public DexEffectObject getPooledEffect(int index) {
		if(index < dexEffectObjects.size()){
			return dexEffectObjects.get(index);
		}
		return null;
	}
	
}
