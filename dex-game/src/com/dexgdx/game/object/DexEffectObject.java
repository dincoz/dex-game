package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.dexgdx.game.constants.EParticleType;
import com.dexgdx.game.object.BasePoint;
import com.dexgdx.game.action.ITriggerSetter;

public class DexEffectObject implements ITriggerSetter{
	
	public int triggerIndex;
	
	BasePoint position;
	BaseRegion triggerRegion;
	PooledEffect pooledEffect;
	
	EParticleType eParticleType;
	boolean yFlipped;
	
	public DexEffectObject(PooledEffect pooledEffect, EParticleType eParticleType) {
		super();
		this.position = new BasePoint();
		this.triggerRegion= new BaseRegion(position.x, position.y, 0, 0);
		this.pooledEffect = pooledEffect;
		this.eParticleType = eParticleType;
		if(eParticleType == EParticleType.TRAIL){
			this.pooledEffect.flipY();
			yFlipped = true;
		}
	}
	
	public DexEffectObject() {
		super();
		this.position = new BasePoint();
	}

	public BasePoint getPosition() {
		return position;
	}

	public void setPosition(float x, float y){
		position.x = x;
		position.y = y;
		triggerRegion.x = x - 8;
		triggerRegion.y = y - 4;
		triggerRegion.width = 16;
		triggerRegion.height = 8;
	}
	
	public void setPooledEffect(PooledEffect pooledEffect){
		this.pooledEffect = pooledEffect;
		if(eParticleType == EParticleType.TRAIL && !yFlipped){
			this.pooledEffect.flipY();
		}
	}
	
	public PooledEffect getPooledEffectMapRel(){
		pooledEffect.setPosition(position.x, position.y);
		return pooledEffect;
	}
	
	public boolean isComplete(){
		return pooledEffect.isComplete();
	}

	public boolean isTriggerDead() {
		return isComplete();
	}

	public int getTriggerInterval() {
		return 50;
	}

	public BaseRegion getTriggerRegion() {
		return triggerRegion;
	}
	
}
