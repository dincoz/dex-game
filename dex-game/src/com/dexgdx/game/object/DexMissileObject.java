package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.dexgdx.game.Game;
import com.dexgdx.game.action.dexmissile.DexMissileAttackAction;
import com.dexgdx.game.constants.EObjectInfo;

public class DexMissileObject extends MobileObject{

	private static final long serialVersionUID = 8964321500584743195L;
	
	private PooledEffect pooledEffect; 

	public DexMissileObject(Game game, long objectInstOid, float x, float y, float height, float width) {
		super(game, EObjectInfo.DEX_MISSILE.getObjectInfo(), objectInstOid, x, y, height, width, 
				game.defaultConstants.getDexMissileSpeed(), 
				false);
		initialize();
	}
	
	private void initialize(){
		pooledEffect = game.getParticleManager().obtainFromParticleEffectPool(EObjectInfo.DEX_MISSILE);
		setPooledEffectPosition();
		setCollidingObject(false);
	}
	
	public DexMissileAttackAction dexMissileAttack(AbstractActor ownerActor, AbstractActor targetActor){
		DexMissileAttackAction dexMissileAttackAction = actionFactory.dexMissileAttack(ownerActor, targetActor);
		addAction(dexMissileAttackAction);
		return dexMissileAttackAction;
	}
	
	public PooledEffect getPooledEffect() {
		return pooledEffect;
	}

	public boolean isAnimationComplete(){
		boolean isComplete = pooledEffect.isComplete();
		if(isComplete){
			pooledEffect.free();
			game.getParticleManager().freeToParticleEffectPool(EObjectInfo.DEX_MISSILE, pooledEffect);
		}
		return isComplete;
	}
	
	public void setPooledEffectPosition(){
		pooledEffect.setPosition(x + width / 2, y - 30);
	}
	
	public void setPooledEffectPositionInvisible(){
		pooledEffect.setPosition(-2000, -2000);
	}
}
