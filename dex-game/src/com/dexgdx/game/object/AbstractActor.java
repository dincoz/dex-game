package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.*;
import com.dexgdx.game.*;
import com.dexgdx.game.action.*;
import com.dexgdx.game.constants.*;
import com.dexgdx.game.inventory.*;
import com.dexgdx.game.util.*;
import java.util.*;

public abstract class AbstractActor extends MobileObject {
	
	private static final long serialVersionUID = 4783108189582861810L;
	
	protected boolean selected;
	private List<RegionActionTrigger> sprungRegionActionTriggers;
	public TextureRegion portrait;
	
	public Inventory inventory;
	
	protected AbstractActor(Game game, ObjectInfo objectInfo, long objectInstOid, float x, float y, float height, float width, float movementSpeed, TextureRegion portrait) {
		super(game, objectInfo, objectInstOid, x, y, height, width, movementSpeed, true);
		this.portrait = portrait;
		sprungRegionActionTriggers = new ArrayList<RegionActionTrigger>();
		inventory = new Inventory();
	}

	public abstract EColor getRingColor();
	public abstract EColor getOriginalRingColor();
	
	public void meleeAttack(SolidObject targetObject){
		addAction(actionFactory.meleeAttack(targetObject));
	}
	
//	public void dexTrailAttack(BaseRegion targetRegion, boolean async){
//		DexTrailObject dexTrailObject = game.getObjectFactory().dexTrail(this);
//		game.addMovingObject(dexTrailObject);
//		dexTrailObject.dexTrailAttack(this, targetRegion, async);
//	}
	
	public void dexTrailAttack(BasePoint targetPoint){
		addAction(actionFactory.dexTrailAttack(targetPoint));
	}

	public void bulletAttack(BasePoint targetPoint){
		addAction(actionFactory.bulletAttack(targetPoint));
	}
	
	public void dexMissileAttack(AbstractActor targetActor){
		addAction(actionFactory.dexMissileAttack(this, targetActor));
	}

	public void heal(){
		addAction(actionFactory.heal());
	}

	public void dialog(AbstractActor dialogActor){
		addAction(actionFactory.dialog(dialogActor));
	}
	
	public void pickItem(InventoryItem inventoryItem) {
		addAction(actionFactory.pickItemComplete(inventoryItem));
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public void addSprungRegionActionTrigger(RegionActionTrigger regionActionTrigger){
		sprungRegionActionTriggers.add(regionActionTrigger);
	}
	
	public void deleteSprungRegionActionTrigger(RegionActionTrigger regionActionTrigger){
		sprungRegionActionTriggers.remove(regionActionTrigger);
	}
	
	@Override
	protected void postMoveAction() {
		game.checkTriggerSprings(null, this);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		return ((SolidObject)obj).objectInstOid == objectInstOid;
	}
	
	public abstract void runDefaultAction(AbstractActor targetActor);
}
