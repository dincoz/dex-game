package com.dexgdx.game.object;

import com.dexgdx.game.util.ObjectInfo;

public interface IObject {

	ObjectInfo getObjectInfo();
	long getObjectInstOid();
	
}
