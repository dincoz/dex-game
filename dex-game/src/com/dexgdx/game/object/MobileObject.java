package com.dexgdx.game.object;

import com.dexgdx.game.Game;
import com.dexgdx.game.animation.DexAnimation;
import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.util.ObjectInfo;
import com.dexgdx.game.util.trajectory.TrajectoryCalculator;

public class MobileObject extends SolidObject {

	private static final long serialVersionUID = -4802323628456349868L;

	protected boolean moving;
	protected BaseRegion moveTarget;
	protected BaseRegion blockTarget;
	protected float movementSpeed;
	
	protected EMoveDirection eMoveDirection;
	
	boolean isStandardImageObject;
	
	private int waitStateCounter;

	MobileObject(Game game, ObjectInfo objectInfo, long objectInstOid,
			float x, float y, float height, float width, 
			float movementSpeed, boolean solidObject) {
		super(game, objectInfo, objectInstOid, x, y, height, width, solidObject);
		this.movementSpeed = movementSpeed;
		this.eMoveDirection = EMoveDirection.EAST;
		
		isStandardImageObject = true;
		waitStateCounter = 0;
	}
	
	MobileObject(Game game, ObjectInfo objectId, long objectInstOid,
			float x, float y, float height, float width, 
			float imageDeltaX, float imageDeltaY, float imageHeight, float imageWidth, 
			float movementSpeed, boolean solidObject) {
		super(game, objectId, objectInstOid, x, y, height, width, imageDeltaX, imageDeltaY, imageHeight, imageWidth, solidObject);
		this.movementSpeed = movementSpeed;
		this.eMoveDirection = EMoveDirection.EAST;
		
		isStandardImageObject = false;
		waitStateCounter = 0;
	}

	public BaseRegion getMoveTarget() {
//		if(moveTarget == null)
//			moveTarget = new BaseRegion(400, 400, 40, 40);
		return moveTarget;
	}
	

	public void setMoveTarget(BaseRegion moveTarget) {
		this.moveTarget = moveTarget;
	}
	
	public void setBlockTarget(BaseRegion blockTarget) {
		this.blockTarget = blockTarget;
	}

	public BaseRegion getBlockTarget(){
		return blockTarget;
	}
	
	@Override
	public void loadImageTextureByFrame(int animIndex){
		imageTexture = getAnimation().getKeyFrame(game.stateTime + animIndex, true);
		
		if(isStandardImageObject)
			setImageRelativeRegionForStandardObject();
	}

	@Override
	public void loadImageTextureByStopFrame(){
		imageTexture = getAnimation().getStopFrame();
		if(isStandardImageObject)
			setImageRelativeRegionForStandardObject();
	}
	
	private DexAnimation getAnimation(){
		DexAnimation animation = game.getAnimationManager().getAnimationByKey(
			getObjectInfo().getInfoNumber(), 1, 1, getMoveDirection().index);
		if(animation == null){
			animation = game.getAnimationManager().getAnimationByKey(
				getObjectInfo().getInfoNumber(), 1, 1, TrajectoryCalculator.getSimpleDirection(getMoveDirection().index));
		}
		return animation;
	}
	
	private void setImageRelativeRegionForStandardObject(){
        float frameRatio = (float) imageTexture.getRegionHeight() / (float)imageTexture.getRegionWidth();
    	imageRelativeRegion = new BaseRegion(
    			-4, 
    			width/2 - (width + 4) * frameRatio, 
    			(width + 8) * frameRatio ,
    			width + 8 );
	}
	
	public void startMoving(BaseRegion target){
		this.moveTarget = target;
		moving = true;
	}
	
	public void stopMoving(){
		this.moveTarget = null;
		moving = false;
	}

	public void moveToRegion(BaseRegion fromRegion){
		setFromRegion(fromRegion);
		postMoveAction();
	}
	protected void postMoveAction(){}
	
	public boolean isMoving() {
		return moving;
	}
	
	public boolean hasFinalizedMoving(){
		return true;
	}
	
	public float getMovementSpeed() {
		return movementSpeed;
	}

	public EMoveDirection getMoveDirection() {
		return eMoveDirection;
	}

	public void setMoveDirection(EMoveDirection eMoveDirection) {
		this.eMoveDirection = eMoveDirection;
	}
	
	public void setWaitState(int waitStateCounter){
		this.waitStateCounter = waitStateCounter;
	}
	public boolean checkAndIncrementWaitState(){
		if(waitStateCounter > 0){
			waitStateCounter --;
			return true;
		}
		return false;
	}
	public boolean isInWaitState(){
		return waitStateCounter > 0;
	}

	public void wallHugMovement(BaseRegion targetRegion){
		clearMoveBlockedActions();
		
		addAction(actionFactory.wallHugMovement(targetRegion));
	}

	public boolean isOnTheEdgeOf(BaseRegion baseRegion){
		return x - (baseRegion.x + baseRegion.width) <= movementSpeed ||
				(baseRegion.x + baseRegion.width) - x <= movementSpeed ||
				y - (baseRegion.y + baseRegion.height) <= movementSpeed ||
						(baseRegion.y + baseRegion.height) - y <= movementSpeed;
	}
}
