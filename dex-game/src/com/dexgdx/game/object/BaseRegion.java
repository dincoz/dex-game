package com.dexgdx.game.object;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;

public class BaseRegion extends Rectangle {
	
	private static final long serialVersionUID = -3649759885863145011L;
	protected boolean collidingObject;
	
	private Polygon polygonRepresentation;
	
	private BasePoint centerPoint;
	private BaseRegion cursorToMiddlePosition;
	
	public BaseRegion(float x, float y, float height, float width) {
		super(x, y, width, height);
		collidingObject = false;
	}
	
	public void setFromRegion(BaseRegion fromRegion){
		x = fromRegion.x;
		y = fromRegion.y;
		height = fromRegion.height;
		width = fromRegion.width;
	}
	
	public void setToRegion(BaseRegion toRegion){
		toRegion.x = x;
		toRegion.y = y;
		toRegion.height = height;
		toRegion.width = width;
	}
	
	public BaseRegion fromCursorToMiddlePosition(float x, float y){
		if(cursorToMiddlePosition == null)
			cursorToMiddlePosition = new BaseRegion(0, 0, height, width);
		cursorToMiddlePosition.x = x - width / 2;
		cursorToMiddlePosition.y = y - height/  2;
		return cursorToMiddlePosition;
	}
	
	public BasePoint getCenterPoint(){
		if(centerPoint == null)
			centerPoint = new BasePoint(x + width / 2, y + height / 2);
		else {
			centerPoint.x = x + width / 2;
			centerPoint.y = y + height / 2;
		}
		return centerPoint;
	}
	
	public BaseRegion cloneRegion(){
		return new BaseRegion(x, y, height, width);
	}
	
	public boolean isCollidingWith(BaseRegion baseRegion) {
		return this.overlaps(baseRegion);
	}

	public boolean isOneStepAwayFrom(BaseRegion baseRegion, float stepSize){
		if(baseRegion == null)
			return true;
		return Math.abs(x - baseRegion.x) < stepSize && Math.abs(y - baseRegion.y) < stepSize;
	}
	
	public boolean isSamePositionAs(BaseRegion region){
		return (x == region.x && y == region.y);
	}
	

	public boolean isCollidingObject() {
		return collidingObject;
	}
	
	public void setCollidingObject(boolean collidingObject) {
		this.collidingObject = collidingObject;
	}

	public void setRegionFromPolygon(float[] vertices){
		if(polygonRepresentation == null)
			polygonRepresentation = new Polygon();
		polygonRepresentation.setVertices(vertices);
		this.set(polygonRepresentation.getBoundingRectangle());
	}
	
	public Polygon getAsPolygon(){
		float[] vertices = {
				x, 
				y, 
				x, 
				y + height, 
				x + width,
				y + height,
				x + width,
				y};
		polygonRepresentation.setVertices(vertices);
		return polygonRepresentation;
	}

	public Polygon getPolygonRepresentation() {
		return polygonRepresentation;
	}
	
}
