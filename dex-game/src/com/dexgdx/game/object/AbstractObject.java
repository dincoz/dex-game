package com.dexgdx.game.object;

import com.dexgdx.game.util.ObjectInfo;

public abstract class AbstractObject implements IObject {
	
	private SolidObject solidObject;

	public AbstractObject(SolidObject solidObject) {
		super();
		this.solidObject = solidObject;
	}

	@Override
	public ObjectInfo getObjectInfo() {
		return solidObject.getObjectInfo();
	}

	@Override
	public long getObjectInstOid() {
		return solidObject.objectInstOid;
	}

}
