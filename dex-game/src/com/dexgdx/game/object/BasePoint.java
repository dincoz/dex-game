package com.dexgdx.game.object;

import com.badlogic.gdx.math.Vector3;

public class BasePoint {
	
	public float x;
	public float y;
	
	public BasePoint() {
		super();
	}
	
	public BasePoint(float x, float y) {
		super();
		set(x, y);
	}
	
	public BasePoint set(float x, float y){
		this.x = x;
		this.y = y;
		return this;
	}
	
	public void setFromRegionPoint(BaseRegion region){
		x = region.x;
		y = region.y;
	}
	
	public void setFromPoint(BasePoint point){
		x = point.x;
		y = point.y;
	}
	
	public void setFromVector(Vector3 vector){
		x = vector.x;
		y = vector.y;
	}
	
	public void addPosition(BasePoint basePoint){
		x += basePoint.x;
		y += basePoint.y;
	}
	
	public BasePoint clonePoint() {
		return new BasePoint(x, y);
	}

}
