package com.dexgdx.game.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dexgdx.game.Game;
import com.dexgdx.game.ai.ActorAI;
import com.dexgdx.game.constants.EColor;
import com.dexgdx.game.constants.EObjectInfo;
import com.dexgdx.game.util.ObjectInfo;

public class EnemyActor extends AbstractActor {
	
	private static final long serialVersionUID = -4220630211708586307L;
	
	private ActorAI actorAI; 

	EnemyActor(Game game, ObjectInfo objectInfo, long objectInstOid, float x, float y, float height, float width, float movementSpeed, TextureRegion portrait) {
		super(game, objectInfo, objectInstOid, x, y, height, width, movementSpeed, portrait);
		actorAI = new ActorAI(this, game);
		remainingHitPoint = 20;
		initialHitPoint = 20;
	}
	
	@Override
	public EColor getRingColor() {
		EColor color = null;
//		if(isBeingMovedTo() || isBeingAttackedTo()){
//			color = ERingColor.BEING_TARGET;
//		}
		if(isSelected()){
			color = EColor.SELECTED_ENEMY;
		}
		else {
			color = EColor.UNSELECTED_ENEMY;
		}
		return color;
	}

	@Override
	public EColor getOriginalRingColor() {
		return EColor.UNSELECTED_ENEMY;
	}

	public ActorAI getActorAI() {
		return actorAI;
	}

	@Override
	public void nextMove(){
		actorAI.decideNextMove();
	}

	@Override
	public void runDefaultAction(AbstractActor targetActor) {
		if(getObjectInfo().getInfoNumber() == EObjectInfo.TIM.getObjectInfo().getInfoNumber()
						|| getObjectInfo().getInfoNumber() == EObjectInfo.ORC.getObjectInfo().getInfoNumber()){
			meleeAttack(targetActor);
		}
		else if(getObjectInfo().getInfoNumber() == EObjectInfo.MARY.getObjectInfo().getInfoNumber()){
			dexMissileAttack(targetActor);
		}
		else if(getObjectInfo().getInfoNumber() == EObjectInfo.GRIDLOCK.getObjectInfo().getInfoNumber()){
			bulletAttack(targetActor.cloneRegion().getCenterPoint());
		}
	}
	
}
