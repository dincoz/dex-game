package com.dexgdx.game.ai;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.AbstractActor;

public class ActorAI {
	
	private Game game;
	private AbstractActor actor;
	
	public ActorAI(AbstractActor actor, Game game) {
		super();
		this.actor = actor;
		this.game = game;
	}

	public void decideNextMove(){
		if(actor.getActiveActionCount() == 0 && actor.getPotentialActionCount() == 0){
			AbstractActor targetActor = null;
			if(!actor.isAttacking() && game.gameObjects.getPlayerList().size() > 0){
				int i = 0;
				do{
					targetActor = game.gameObjects.getPlayerList().get(i);
					i++;
				}
				while(targetActor.isDestroyed() && i < game.gameObjects.getPlayerList().size());
				actor.runDefaultAction(targetActor);
			}
		}
//		else if(actor.isAttacking() && !actor.isOnTheEdgeOf(actor.getAttackingTarget())){
//			actor.clearAllActions();
//			actor.stopAttacking();
//			actor.meleeAttack(actor.getAttackingTarget());
//		}
		actor.getActionRunner().run();
	}
}
