package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.dialog.DialogFlow;
import com.dexgdx.game.object.AbstractActor;

public class DialogAction extends AbstractAction {

	AbstractActor dialogOwnerObject;
	AbstractActor dialogingActor;
	
	public DialogAction(Game game, AbstractActor dialogOwnerObject, AbstractActor dialogingActor) {
		super(game, EActionType.DIALOG, dialogOwnerObject);
		this.dialogingActor = dialogingActor;
		this.dialogOwnerObject = dialogOwnerObject;
		waitTime = 10;
	}

	@Override
	public boolean isFinishedExceptWaitTime() {
		return true;
	}

	@Override
	public void runNextStep() {

	}

	@Override
	public void initialize() {
		
		DialogFlow flow = new DialogFlow()
			.addLine(dialogOwnerObject, "Hello")
			.addLine(dialogingActor, "Hi")
//			.addChoice(0, "What is this?")
//				.addChoiceLine(0, dialogOwnerObject, "What is this?")
//				.addChoiceLine(0, dialogingActor, "I dont really know")
//			.addChoice(1, "Why arent you ready?")
//				.addChoiceLine(1, dialogOwnerObject, "Why arent you ready?")
//				.addChoiceLine(1, dialogingActor, "I dont know how to wear this")
			;
		flow.choiceSet.addText("Dialog1");
		flow.choiceSet.addLine(0, dialogingActor,  "DialogChoice11");
		flow.choiceSet.addLine(0, dialogOwnerObject,  "DialogChoice12");
		flow.choiceSet.get(0).choiceSet.addText("Dialog2");
		flow.choiceSet.get(0).choiceSet.addLine(0, dialogingActor,  "DialogChoice21");
		flow.choiceSet.get(0).choiceSet.addLine(0, dialogOwnerObject,  "DialogChoice22");
		flow.choiceSet.get(0).choiceSet.addLine(0, dialogingActor,  "DialogChoice23");
		flow.choiceSet.get(0).choiceSet.addText("Dialog3");
		flow.choiceSet.get(0).choiceSet.addLine(1, dialogingActor,  "DialogChoice31");
		flow.choiceSet.get(0).choiceSet.addLine(1, dialogOwnerObject,  "DialoogChoice32");
		flow.choiceSet.addText("Dialog4");
		flow.choiceSet.addLine(1, dialogingActor,  "DialogChoice41");
		flow.choiceSet.addLine(1, dialogOwnerObject,  "DialogChoice42");
		game.dialogBox.startDialog(flow);
	}

}
