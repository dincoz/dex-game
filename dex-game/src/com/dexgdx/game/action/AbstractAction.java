package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.constants.*;

public abstract class AbstractAction {
	
	protected Game game;
	protected SolidObject actionOwnerObject;
	protected boolean started;
	public EActionType actionType;
	
	public int waitTime;
	
	public AbstractAction(Game game, EActionType actionType, SolidObject actionOwnerObject) {
		super();
		this.game= game;
		this.actionType = actionType;
		this.actionOwnerObject = actionOwnerObject;
		waitTime = 3;
	}
	public boolean hasStarted() {
		return started;
	}
	
	@Override
	public String toString() {
		return this.getClass().getName();
	}
	
	public boolean runInBackground() {
		return false;
	}
	
	public boolean isFinished(){
		return waitTime == 0 && isFinishedExceptWaitTime();
	}

	public abstract boolean isFinishedExceptWaitTime();
	public abstract void runNextStep();
	public abstract void initialize();
}
