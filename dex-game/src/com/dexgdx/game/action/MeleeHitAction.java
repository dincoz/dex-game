package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.constants.*;
import com.dexgdx.game.object.*;

public class MeleeHitAction extends AbstractAction {
	
	private MobileObject ownerMobileObject;
	private SolidObject targetObject;
	
	private int passedTime;

	private int attackPoint;
	private int attackSpeed;
	
	MeleeHitAction(Game game, MobileObject ownerMobileObject, SolidObject targetObject, int attackPoint, int attackSpeed) {
		super(game, EActionType.OTHER, ownerMobileObject);
		this.targetObject = targetObject;
		this.ownerMobileObject = ownerMobileObject;
		this.attackPoint = attackPoint;
		this.attackSpeed = attackSpeed;
		
		passedTime = 0;
	}

	@Override
	public boolean isFinishedExceptWaitTime() {
		if(targetObject == null){
			actionOwnerObject.stopAttacking();
			return true;
		}
		boolean finished = (passedTime == -1 || targetObject.getRemainingHitPoint() < 1);
		if(finished){
			actionOwnerObject.stopAttacking();
			if(targetObject.getRemainingHitPoint() < 1){
				targetObject.destroy();
			}
		}
		return finished;
	}

	@Override
	public void runNextStep() {
		if(ownerMobileObject.getAttackingTarget() == null){
			return;
		}
		if(!ownerMobileObject.isOnTheEdgeOf(ownerMobileObject.getAttackingTarget())){
			passedTime = attackSpeed;
		}
		else if(passedTime == attackSpeed){
			targetObject.lowerHitPoint(attackPoint);
			passedTime = -2;
		}
		passedTime++;
	}

	@Override
	public void initialize() {
		started = true;
		if(targetObject instanceof BulletObject){
			targetObject = ((BulletObject) targetObject).getHitActor();
		}
		if(targetObject != null){
			actionOwnerObject.startAttacking(targetObject);
		}
	}
}
