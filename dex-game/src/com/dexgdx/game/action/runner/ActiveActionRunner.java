package com.dexgdx.game.action.runner;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.action.AbstractAction;
import com.dexgdx.game.action.MovementAction;
import com.dexgdx.game.action.PickItemAction;
import com.dexgdx.game.action.PickItemCompleteAction;
import com.dexgdx.game.constants.EActionType;

public class ActiveActionRunner {
	
	private ActionRunner actionRunner;
	
	private List<AbstractAction> actionList;
	private List<AbstractAction> actionsToRemove;
	
	public ActiveActionRunner(ActionRunner actionRunner) {
		this.actionRunner = actionRunner;
		
		actionList = new ArrayList<AbstractAction>();
		actionsToRemove = new ArrayList<AbstractAction>();
	}

	public void run(){
		if(actionList.size() == 0)
			return;
		int index = 0;
		while(index < actionList.size()){
			if(!actionRunner.ownerObject.isDestroyed() ||
					actionList.get(index).actionType == EActionType.MISSILE ||
					actionList.get(index).actionType == EActionType.TRAIL ||
					actionList.get(index).actionType == EActionType.BULLET)
			runAction(index);
			index++;
		}
		
		clearFinishedActions();
	}

	private void runAction(int index){
		if(!actionList.get(index).hasStarted()){
			actionList.get(index).initialize();
		}
		actionList.get(index).runNextStep();
		if(actionList.get(index).isFinishedExceptWaitTime()){
			actionsToRemove.add(actionList.get(index));
		}
	}
	
	void addAction(AbstractAction action){
		actionList.add(action);
	}

	private void clearFinishedActions(){
		for (AbstractAction action : actionsToRemove) {
			actionList.remove(action);
		}
		actionsToRemove.clear();
	}

	public List<AbstractAction> getActionList() {
		return actionList;
	}
	
	public void clearAllActions(){
		actionList.clear();
	}

	public void clearMoveBlockedActiveActions(){
		List<AbstractAction> actionsToRemove = new ArrayList<AbstractAction>();
		for (AbstractAction abstractAction : actionList) {
			if(abstractAction instanceof PickItemCompleteAction ||
					abstractAction instanceof PickItemAction ||
					abstractAction instanceof MovementAction) {
				actionsToRemove.add(abstractAction);
			}
		}
		for (AbstractAction abstractAction : actionsToRemove) {
			actionList.remove(abstractAction);
		}
	}
}
