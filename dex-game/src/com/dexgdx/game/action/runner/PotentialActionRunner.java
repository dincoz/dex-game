package com.dexgdx.game.action.runner;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.dexgdx.game.action.AbstractAction;
import com.dexgdx.game.action.MovementAction;
import com.dexgdx.game.action.PickItemAction;
import com.dexgdx.game.action.PickItemCompleteAction;

public class PotentialActionRunner {
	
	private ActionRunner actionRunner;
	
	private List<AbstractAction> actionList;
	public int waitTime;
	boolean timeIsCounting;
	
	public PotentialActionRunner(ActionRunner actionRunner) {
		this.actionRunner = actionRunner;
		
		waitTime = 0;	
		actionList = new ArrayList<AbstractAction>();
	}

	public void run(){
		if(actionList.size() > 0 && waitTime == 0){
			if(timeIsCounting){
				actionRunner.activeActionRunner.addAction(actionList.remove(0));
				timeIsCounting = false;
			}
			if(actionList.size() > 0){
				waitTime = actionList.get(0).waitTime;
			}
		}
		if(actionList.size() == 0 || waitTime > 0){
			if(waitTime > 0){
				waitTime--;
				timeIsCounting = true;
			}
			Gdx.graphics.requestRendering();
			return;
		}
		Gdx.graphics.requestRendering();
	}
	
	void addAction(AbstractAction action){
		if(actionList.size() < 8){
			actionList.add(action);
		}
	}

	public List<AbstractAction> getActionList() {
		return actionList;
	}
	
	public void clearAllActions(){
		actionList.clear();
	}
	
	public void clearMoveBlockedPotentialActions(){
		List<AbstractAction> actionsToRemove = new ArrayList<AbstractAction>();
		for (AbstractAction abstractAction : actionList) {
			if(abstractAction instanceof PickItemCompleteAction ||
					abstractAction instanceof PickItemAction ||
					abstractAction instanceof MovementAction) {
				actionsToRemove.add(abstractAction);
			}
		}
		for (AbstractAction abstractAction : actionsToRemove) {
			actionList.remove(abstractAction);
		}
	}
	
}
