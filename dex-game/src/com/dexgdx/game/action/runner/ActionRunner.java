package com.dexgdx.game.action.runner;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.AbstractAction;
import com.dexgdx.game.object.SolidObject;

public class ActionRunner {

	Game game;
	SolidObject ownerObject;
	ActiveActionRunner activeActionRunner;
	PotentialActionRunner potentialActionRunner;
	
	public ActionRunner(Game game, SolidObject ownerObject) {
		this.game = game;
		this.ownerObject = ownerObject;
		activeActionRunner = new ActiveActionRunner(this);
		potentialActionRunner = new PotentialActionRunner(this);
	}
	
	public void addPotentialAction(AbstractAction action){
		potentialActionRunner.addAction(action);
	}
	
	public void addActiveAction(AbstractAction action){
		activeActionRunner.addAction(action);
	}
	
	public void run(){
		if(!ownerObject.isDestroyed())
			potentialActionRunner.run();
		activeActionRunner.run();
	}

	public PotentialActionRunner getPotentialActionRunner() {
		return potentialActionRunner;
	}
	
	public ActiveActionRunner getActiveActionRunner() {
		return activeActionRunner;
	}

	public void clearPotentialActions(){
		potentialActionRunner.clearAllActions();
	}

	public void clearMoveBlockedActions(){
		activeActionRunner.clearMoveBlockedActiveActions();
		potentialActionRunner.clearMoveBlockedPotentialActions();
	}

	public void clearActiveActions(){
		activeActionRunner.clearAllActions();
	}
	
}
