package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.constants.*;

public class MeleeAttackAction extends MultipleAction {

	MeleeAttackAction(Game game, AbstractActor ownerActor, SolidObject targetObject) {
		super(game, EActionType.OTHER, ownerActor, ownerActor.getActionFactory().wallHugMovement(targetObject));
		addAction(ownerActor.getActionFactory().meleeHit(targetObject, 1, 50));
	}
}
