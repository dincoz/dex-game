package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.InventoryItem;

public class PickItemCompleteAction extends MultipleAction {

	public PickItemCompleteAction(Game game, InventoryItem inventoryItem, AbstractActor ownerActor) {
		super(game, EActionType.PICKITEM, ownerActor, ownerActor.getActionFactory().wallHugMovement(inventoryItem));
		addAction(ownerActor.getActionFactory().pickItem(inventoryItem));
		waitTime = 1;
	}
}
