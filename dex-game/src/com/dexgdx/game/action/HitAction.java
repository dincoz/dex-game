package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.constants.*;

public class HitAction extends AbstractAction {

	private SolidObject targetObject;
	
	private boolean finished;
	private int attackPoint;
	
	HitAction(Game game, MobileObject ownerMobileObject, SolidObject targetObject, int attackPoint) {
		super(game, EActionType.OTHER, ownerMobileObject);
		this.targetObject = targetObject;
		this.attackPoint = attackPoint;
		
		finished = false;
	}

	@Override
	public boolean isFinishedExceptWaitTime() {
		if(finished){
			actionOwnerObject.stopAttacking();
			if(targetObject.getRemainingHitPoint() < 1){
				targetObject.destroy();
			}
		}
		return finished;
	}

	@Override
	public void runNextStep() {
		targetObject.lowerHitPoint(attackPoint);
		finished = true;
	}

	@Override
	public void initialize() {
		started = true;
		actionOwnerObject.startAttacking(targetObject);
	}

}
