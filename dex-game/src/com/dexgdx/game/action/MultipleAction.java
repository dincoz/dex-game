package com.dexgdx.game.action;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.constants.*;


public class MultipleAction extends AbstractAction{
	
	protected List<AbstractAction> actionList;
	private List<AbstractAction> actionsToRemove;
	
	public MultipleAction(Game game, EActionType actionType, SolidObject ownerActor, AbstractAction action) {
		super(game, actionType, ownerActor);
		this.actionList = new ArrayList<AbstractAction>();
		this.actionsToRemove = new ArrayList<AbstractAction>();
		actionList.add(action);
	}

	public void runNextStep(){
		if(actionList.size() == 0)
			return;
		int index = -1;
		do{
			index++;
			runAction(index);
		}
		while(actionList.size() > index + 1 
				&& 
//				(actionList.get(index).isAsync() || 
				actionList.get(index).runInBackground())
//				)
				;
		
		clearFinishedActions();
	}

	private void runAction(int index){
		if(!actionList.get(index).hasStarted()){
			actionList.get(index).initialize();
		}
		actionList.get(index).runNextStep();
		if(actionList.get(index).isFinishedExceptWaitTime()
//				&& !actionList.get(index).runInBackground()
				){
			actionsToRemove.add(actionList.get(index));
		}
	}
	
	private void clearFinishedActions(){
		for (AbstractAction action : actionsToRemove) {
			actionList.remove(action);
		}
		actionsToRemove.clear();
	}
	
	public void clearAllActions(){
		actionList.clear();
		actionsToRemove.clear();
	}
	
	public boolean isFinishedExceptWaitTime() {
		return actionList.size() == 0;
	}
	
	public void addAction(AbstractAction action){
		actionList.add(action);
	}

	public List<AbstractAction> getActionList() {
		return actionList;
	}

	@Override
	public void initialize() {}
	
	public int size(){
		return actionList == null ? 0 : actionList.size();
	}
	
}
