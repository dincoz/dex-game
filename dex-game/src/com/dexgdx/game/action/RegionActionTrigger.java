package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.*;

public abstract class RegionActionTrigger{
	
	protected Game game;
	
	protected ITriggerSetter triggerSetter;
	
	public RegionActionTrigger(Game game, ITriggerSetter triggerSetter) {
		super();
		this.game = game;
		this.triggerSetter = triggerSetter;
	}

	public abstract void springTrigger(AbstractActor actor);
	public abstract void runNextTrigger();
	
	public boolean isTriggerDead(){
		return triggerSetter.isTriggerDead();
	}

	public ITriggerSetter getTriggerSetter() {
		return triggerSetter;
	}
}
