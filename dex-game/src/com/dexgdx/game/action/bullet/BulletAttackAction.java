package com.dexgdx.game.action.bullet;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.MultipleAction;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.object.*;

public class BulletAttackAction extends MultipleAction {
	
	public BulletAttackAction(Game game, BulletObject ownerBulletObject, AbstractActor ownerActor, BasePoint targetPoint) {
		super(game, EActionType.BULLET, ownerActor, ownerBulletObject.getActionFactory().bulletMovement(ownerActor, targetPoint));
		addAction(ownerBulletObject.getActionFactory().meleeHit(ownerBulletObject, 2, 1));
		waitTime = 10;
	}
}
