package com.dexgdx.game.action.bullet;
import com.dexgdx.game.*;
import com.dexgdx.game.action.*;
import com.dexgdx.game.constants.*;
import com.dexgdx.game.object.*;
import com.dexgdx.game.util.pathcalc.*;

public class BulletMovementAction extends MovementAction{

	private BulletObject bulletObject;
	private AbstractActor ownerActor;

	public BulletMovementAction(Game game, BulletObject ownerBulletObject, AbstractActor ownerActor,
			BasePoint targetPoint) {
		super(game, ownerBulletObject, ownerBulletObject, new BaseRegion(targetPoint.x, targetPoint.y, 1, 1), new DirectPathCalcSession(game));
		this.bulletObject = ownerBulletObject;
		this.ownerActor = ownerActor;
		setStepCount(120);
	}

	@Override
	public void initialize() {
		
		bulletObject.x = ownerActor.getCenterPoint().x - bulletObject.width/2;
		bulletObject.y = ownerActor.getCenterPoint().y - bulletObject.height/2;

		super.initialize();

		bulletObject.x += pathCalSession.getTrajectory().x;
		bulletObject.y += pathCalSession.getTrajectory().y;

		bulletObject.initializePosition(pathCalSession.getTrajectory());
	}

	@Override
	public void runNextStep() {
		super.runNextStep();
	}

	@Override
	protected EMoveStopCondition getStopCondition() {
		return EMoveStopCondition.WHEN_TOUCHED_OR_STEPCOUNT_REACHED;
	}

	@Override
	public boolean runInBackground() {
		return false;
	}

	@Override
	public boolean customEndCondition(){
		SolidObject collidingObject = game.gameObjects.getCollidingObject(movingObject, movingObject);
		boolean reachedTarget = false;
		if(collidingObject != null && collidingObject != ownerActor){
			movingObject.setBlockTarget(collidingObject);
			reachedTarget = true;
		}
		return reachedTarget;
	}
}
