package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.dexmissile.DexMissileAttackAction;
import com.dexgdx.game.action.dexmissile.DexMissileMovementAction;
import com.dexgdx.game.action.dextrail.DexTrailAttackAction;
import com.dexgdx.game.action.dextrail.DexTrailMovementAction;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.BasePoint;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.DexMissileObject;
import com.dexgdx.game.object.DexTrailObject;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.util.pathcalc.WallHugDirectPathCalcSession;
import com.dexgdx.game.action.bullet.*;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.object.*;

public class ActionFactory {

	private Game game;
	private SolidObject ownerObject;
	
	public ActionFactory(Game game, SolidObject ownerActor) {
		this.game = game;
		this.ownerObject = ownerActor;
	}

	public SteadyAction steady(int steadyTime){
		return new SteadyAction(game, ownerObject, steadyTime);
	}
	
	public HealAction heal(){
		return new HealAction(game, ownerObject);
	}
	
	public MovementAction wallHugMovement(BaseRegion targetRegion){
		MovementAction movementAction = new MovementAction(game, ownerObject, (MobileObject) ownerObject, targetRegion, new WallHugDirectPathCalcSession(game)); 
		return movementAction;
	}
	
	public DexMissileAttackAction dexMissileAttack(AbstractActor ownerActor, AbstractActor targetActor){
		DexMissileObject dexMissileObject = game.getObjectFactory().dexMissile();
		game.gameObjects.addMovingObject(dexMissileObject);
		return new DexMissileAttackAction(game, dexMissileObject, ownerActor, targetActor);
	}
	
	public DexMissileMovementAction dexMissileMovement(AbstractActor ownerActor, AbstractActor targetActor){
		return new DexMissileMovementAction(game, (DexMissileObject) ownerObject, ownerActor, targetActor);
	}

	public DexTrailAttackAction dexTrailAttack(BasePoint targetPoint){
		DexTrailObject dexTrailObject = game.getObjectFactory().dexTrail((AbstractActor)ownerObject);
		game.gameObjects.addMovingObject(dexTrailObject);
		return new DexTrailAttackAction(game, dexTrailObject, (AbstractActor)ownerObject, targetPoint);
	}
	
	public DexTrailMovementAction dexTrailMovement(AbstractActor ownerActor, BasePoint targetPoint){
		DexTrailMovementAction dexTrailMovement = new DexTrailMovementAction(game, (DexTrailObject)ownerObject, ownerActor, targetPoint);
		return dexTrailMovement;
	}
	
	public BulletAttackAction bulletAttack(BasePoint targetPoint){
		BulletObject bulletObject = game.getObjectFactory().bullet();
		game.gameObjects.addMovingObject(bulletObject);
		return new BulletAttackAction(game, bulletObject, (AbstractActor)ownerObject, targetPoint);
	}
	
	public BulletMovementAction bulletMovement(AbstractActor ownerActor, BasePoint targetPoint){
		BulletMovementAction bulletMovement = new BulletMovementAction(game, (BulletObject)ownerObject, ownerActor, targetPoint);
		return bulletMovement;
	}
	
	public MeleeAttackAction meleeAttack(SolidObject targetObject){
		return new MeleeAttackAction(game, (AbstractActor) ownerObject, targetObject);
	}
	
	public MeleeHitAction meleeHit(SolidObject targetObject, int attackPoint, int attackSpeed){
		return new MeleeHitAction(game, (MobileObject) ownerObject, targetObject, attackPoint, attackSpeed);
	}
	
	public HitAction hit(SolidObject targetObject, int attackPoint){
		return new HitAction(game, (MobileObject) ownerObject, targetObject, attackPoint);
	}
	
	public DialogAction dialog(AbstractActor targetObject){
		return new DialogAction(game, (AbstractActor)ownerObject, targetObject);
	}
	
	public void setOwnerActor(AbstractActor ownerActor) {
		this.ownerObject = ownerActor;
	}
	
	public PickItemCompleteAction pickItemComplete(InventoryItem inventoryItem) {
		return new PickItemCompleteAction(game, inventoryItem, (AbstractActor)ownerObject);
	}
	
	public PickItemAction pickItem(InventoryItem inventoryItem) {
		return new PickItemAction(game, EActionType.PICKITEM, (AbstractActor)ownerObject, inventoryItem);
	}
}
