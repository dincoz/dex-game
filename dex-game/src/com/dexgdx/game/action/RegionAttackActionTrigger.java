package com.dexgdx.game.action;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.SolidObject;

public class RegionAttackActionTrigger extends RegionActionTrigger {

	private SolidObject owningObject;
	private AbstractActor owningActor;
	private List<AbstractActor> sprungActors;
	private List<Integer> sprungActorCounters;
	private List<Integer> finishedActorIndexes;
	
	public RegionAttackActionTrigger(Game game, SolidObject owningObject, AbstractActor owningActor, ITriggerSetter triggerSetter) {
		super(game, triggerSetter);
		this.owningObject = owningObject;
		this.owningActor = owningActor;

		sprungActors = new ArrayList<AbstractActor>();
		sprungActorCounters = new ArrayList<Integer>();
		finishedActorIndexes = new ArrayList<Integer>();
		
		for(AbstractActor actor : game.gameObjects.getActorList()){
			game.checkTriggerSprings(owningActor, actor);
		}
	}

	@Override
	public void springTrigger(AbstractActor actor){
		if(actor == owningActor)
			return;
		if(!sprungActors.contains(actor)){
			sprungActors.add(actor);
			sprungActorCounters.add(triggerSetter.getTriggerInterval());
		}
	}
	
	@Override
	public void runNextTrigger(){
		int currentCount = 0;
		finishedActorIndexes.clear();
		int index = 0;
		for (AbstractActor sprungActor : sprungActors) {
			index = sprungActors.indexOf(sprungActor);
			if(sprungActor == owningActor)
				continue;
			if(!sprungActor.isCollidingWith(triggerSetter.getTriggerRegion())){
				finishedActorIndexes.add(index);
			}
			else if(sprungActorCounters.size() > sprungActors.indexOf(sprungActor)){
				currentCount = sprungActorCounters.get(sprungActors.indexOf(sprungActor));
				if(currentCount - 1 == 0){
					runActorTriggerAction(sprungActor);
					currentCount = triggerSetter.getTriggerInterval();
				}
				sprungActorCounters.set(sprungActors.indexOf(sprungActor), currentCount - 1);
			}
		}
		for (Integer i : finishedActorIndexes) {
			sprungActors.remove(i);
			sprungActorCounters.remove(i);
		}
	}
	public void runActorTriggerAction(AbstractActor actor){
		owningObject.hit(actor, true);
	}
}
