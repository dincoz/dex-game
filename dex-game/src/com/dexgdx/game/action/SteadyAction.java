package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.constants.*;

public class SteadyAction extends AbstractAction {

	SteadyAction(Game game, SolidObject ownerObject, int steadyTime) {
		super(game, EActionType.OTHER, ownerObject);
		this.waitTime = steadyTime;
	}
	
	@Override
	public boolean isFinishedExceptWaitTime() {
		return true;
	}

	@Override
	public boolean hasStarted() {
		return true;
	}

	@Override
	public void runNextStep() {
	}

	@Override
	public void initialize() {
	}

}
