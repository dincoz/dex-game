package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.constants.*;

public class HealAction extends AbstractAction {

	public HealAction(Game game, SolidObject actionOwnerObject) {
		super(game, EActionType.HEAL, actionOwnerObject);
		waitTime = 20;
	}

	@Override
	public boolean isFinishedExceptWaitTime() {
		return true;
	}

	@Override
	public void runNextStep() {
		actionOwnerObject.raiseHitPoint(2);

	}

	@Override
	public void initialize() {

	}

}
