package com.dexgdx.game.action;

import com.dexgdx.game.object.BaseRegion;

public interface ITriggerSetter
{
	boolean isTriggerDead();
	int getTriggerInterval();
	BaseRegion getTriggerRegion();
}
