package com.dexgdx.game.action.dexmissile;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.MovementAction;
import com.dexgdx.game.constants.EMoveStopCondition;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.DexMissileObject;
import com.dexgdx.game.util.pathcalc.DirectPathCalcSession;

public class DexMissileMovementAction extends MovementAction {

	private DexMissileObject dexMissileObject;
	private AbstractActor ownerActor;
	private AbstractActor targetActor;
	
	public DexMissileMovementAction(Game game, DexMissileObject ownerDexMissileObject, AbstractActor ownerActor, 
			AbstractActor targetActor) {
		super(game, ownerDexMissileObject, ownerDexMissileObject, targetActor, new DirectPathCalcSession(game));
		this.dexMissileObject = ownerDexMissileObject;
		this.ownerActor = ownerActor;
		this.targetActor = targetActor;
	}
	
	@Override
	public void initialize() {
		dexMissileObject.setFromRegion(ownerActor);
		super.initialize();
	}
	
	@Override
	public void runNextStep() {
		super.runNextStep();
		if(!isFinishedCalculation() && !targetActor.isDestroyed()){
			dexMissileObject.setPooledEffectPosition();
		}
		else{
			dexMissileObject.setPooledEffectPositionInvisible();
		}
	}
	
	@Override
	protected EMoveStopCondition getStopCondition() {
		return EMoveStopCondition.WHEN_OVER;
	}
	
	@Override
	public boolean runInBackground() {
		return isFinishedExceptWaitTime() && !dexMissileObject.isAnimationComplete();
	}
}
