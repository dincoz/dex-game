package com.dexgdx.game.action.dexmissile;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.MultipleAction;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.DexMissileObject;
import com.dexgdx.game.constants.*;

public class DexMissileAttackAction extends MultipleAction {
	
	public DexMissileAttackAction(Game game, DexMissileObject ownerDexMissileObject, AbstractActor ownerActor, AbstractActor targetActor) {
		super(game, EActionType.MISSILE, ownerDexMissileObject, ownerDexMissileObject.getActionFactory().dexMissileMovement(ownerActor, targetActor));
		addAction(ownerDexMissileObject.getActionFactory().meleeHit(targetActor, 2, 1));
		waitTime = 150;
	}
	
	@Override
	public boolean isFinishedExceptWaitTime() {
		if(actionList.size() > 0)
			return actionList.get(actionList.size() - 1).isFinishedExceptWaitTime();
		else
			return super.isFinishedExceptWaitTime();
	}
	
	@Override
	public boolean runInBackground() {
		if(actionList.size() > 0)
			return actionList.get(actionList.size() - 1).runInBackground();
		else
			return super.runInBackground();
	}
}
