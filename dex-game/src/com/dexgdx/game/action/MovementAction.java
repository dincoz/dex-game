package com.dexgdx.game.action;

import com.badlogic.gdx.Gdx;
import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EMoveStopCondition;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.util.pathcalc.IPathCalcSession;
import com.dexgdx.game.constants.*;

public class MovementAction extends AbstractAction {
	
	public MobileObject movingObject; 
	private BaseRegion initialTarget;
	
	protected IPathCalcSession pathCalSession;
	
	public MovementAction(Game game, SolidObject actionOwnerObject, MobileObject movingObject,
			BaseRegion targetRegion, IPathCalcSession pathCalSession) {
		super(game, EActionType.OTHER, actionOwnerObject);
		this.pathCalSession = pathCalSession;
		this.movingObject = movingObject;
		this.initialTarget = targetRegion;
		started = false;
	}
	
	@Override
	public void initialize() {
		game.gameObjects.addMovingObject(movingObject);
		movingObject.startMoving(initialTarget);
		pathCalSession.initialize(this, movingObject, getStopCondition());
		started = true;
	}
	
	@Override
	public void runNextStep() {
		movingObject.moveToRegion(pathCalSession.getNextPosition());
		//if(actionOwnerObject.isAttacking()) {
			movingObject.setMoveDirection(pathCalSession.getDirection());
		//}
		
		if(isFinishedExceptWaitTime() && !runInBackground()){
			movingObject.stopMoving();
			game.gameObjects.removeMovingObject(movingObject);
		}
		Gdx.graphics.requestRendering();
	}
	
	
	@Override
	public boolean isFinishedExceptWaitTime() {
		return pathCalSession.isCalculationFinished() 
				&& movingObject.hasFinalizedMoving();
	}

	protected EMoveStopCondition getStopCondition(){
		return movingObject.getMoveTarget().isCollidingObject() ? EMoveStopCondition.WHEN_TARGET_TOUCHED : EMoveStopCondition.WHEN_OVER;
	}
	

	public boolean customEndCondition(){
		return true;
	}
	
	protected boolean isFinishedCalculation() {
		return pathCalSession.isCalculationFinished();
	}
	
	public void setStepCount(int stepCount){
		pathCalSession.setStepCount(stepCount);
	}
}
