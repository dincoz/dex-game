package com.dexgdx.game.action;

import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.InventoryItem;

public class PickItemAction extends AbstractAction {
	
	private InventoryItem inventoryItem;
	
	public PickItemAction(Game game, EActionType actionType,
			AbstractActor actionOwner, InventoryItem inventoryItem) {
		super(game, actionType, actionOwner);
		this.inventoryItem = inventoryItem;
	}

	@Override
	public boolean isFinishedExceptWaitTime() {
		return true;
	}

	@Override
	public void runNextStep() {
		((AbstractActor)actionOwnerObject).inventory.addItem(inventoryItem);
		game.gameObjects.removeOtherSceneObject(inventoryItem);
	}

	@Override
	public void initialize() {

	}

}
