package com.dexgdx.game.action.dextrail;

import com.dexgdx.game.Game;
import com.dexgdx.game.action.MovementAction;
import com.dexgdx.game.constants.EMoveStopCondition;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.BasePoint;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.DexTrailObject;
import com.dexgdx.game.util.pathcalc.DirectPathCalcSession;

public class DexTrailMovementAction extends MovementAction {

	private DexTrailObject dexTrailObject; 
	private AbstractActor ownerActor;
	
	public DexTrailMovementAction(Game game, DexTrailObject ownerDexTrailObject, AbstractActor ownerActor, 
			BasePoint targetPoint) {
		super(game, ownerDexTrailObject, ownerDexTrailObject, new BaseRegion(targetPoint.x, targetPoint.y, 1, 1), new DirectPathCalcSession(game));
		this.dexTrailObject = ownerDexTrailObject;
		this.ownerActor = ownerActor;
		setStepCount(20);
	}
	
	@Override
	public void initialize() {
		BaseRegion tempRegion = ownerActor.cloneRegion();
		
		tempRegion.x += tempRegion.width/2;
		tempRegion.y += tempRegion.height/2;

		dexTrailObject.setFromRegion(tempRegion);
		
		super.initialize();

		dexTrailObject.x += pathCalSession.getTrajectory().x;
		dexTrailObject.y += pathCalSession.getTrajectory().y;

		dexTrailObject.initializePosition();
	}

	@Override
	public void runNextStep() {
		super.runNextStep();
		if(!isFinishedCalculation()){
			dexTrailObject.setDexEffectPosition();
		}
	}

	@Override
	public boolean isFinishedExceptWaitTime() {
		boolean finished = false;
		if(pathCalSession.isCalculationFinished() && dexTrailObject.areAllParticlesDead()){
			game.gameObjects.removeSolidObject(dexTrailObject);
			finished = true;
		}
		return finished;
	}
	
	@Override
	protected EMoveStopCondition getStopCondition() {
		return EMoveStopCondition.WHEN_STEPCOUNT_REACHED;
	}
}
