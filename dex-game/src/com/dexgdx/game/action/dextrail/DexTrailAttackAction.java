package com.dexgdx.game.action.dextrail;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.BasePoint;
import com.dexgdx.game.object.DexTrailObject;
import com.dexgdx.game.action.MultipleAction;
import com.dexgdx.game.constants.*;

public class DexTrailAttackAction extends MultipleAction {
	
	public DexTrailAttackAction(Game game, DexTrailObject ownerDexTrailObject, AbstractActor ownerActor, BasePoint targetPoint) {
		super(game, EActionType.TRAIL, ownerActor, ownerDexTrailObject.getActionFactory().dexTrailMovement(ownerActor, targetPoint));
		waitTime = 80;
	}
}
