package com.dexgdx.game.action;

import com.dexgdx.game.Game;

public class ActionManager {
	
	private Game game;
	
	public ActionManager(Game game) {
		super();
		this.game = game;
	}
	
	public synchronized void runActorNextActions(){
		int i = 0;
		while(i < game.gameObjects.getSolidObjects().size()){
//			if(!game.getBaseObjectList().get(i).isDestroyed()){
				game.gameObjects.getSolidObjects().get(i).nextMove();
//			}
			i++;
		}
	}
	public synchronized void runActorNextTriggers(){
		int i = 0;
		while(i < game.getRegionActionTriggers().size()){
			if(!game.getRegionActionTriggers().get(i).isTriggerDead()){
				game.getRegionActionTriggers().get(i).runNextTrigger();
			}
			i++;
		}
	}
	
}
