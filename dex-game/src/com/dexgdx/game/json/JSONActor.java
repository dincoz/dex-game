package com.dexgdx.game.json;

import java.io.Serializable;

import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EPlayerType;
import com.dexgdx.game.util.ObjectInfo;

public class JSONActor implements Serializable{

	private static final long serialVersionUID = -6045737556135900095L;
	
	private EPlayerType ePlayerType;
	private ObjectInfo objectInfo;
	
	private boolean isSimpleActorRegion;
	
	private float x;
	private float y;
	private float height;
	private float width;

	private float movementSpeed;
	private String moveDirection;
	
	private EImagePath portraitPath;

	public JSONActor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JSONActor(EPlayerType ePlayerType, boolean isSimpleActorRegion, float x,
			float y, float height, float width, float movementSpeed, String moveDirection, ObjectInfo playerIdentification, EImagePath portraitPath) {
		super();
		this.ePlayerType = ePlayerType;
		this.isSimpleActorRegion = isSimpleActorRegion;
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.movementSpeed = movementSpeed;
		this.moveDirection = moveDirection;
		this.objectInfo = playerIdentification;
		this.portraitPath = portraitPath;
		
	}
	
	public boolean isSimpleActorRegion() {
		return isSimpleActorRegion;
	}

	public void setSimpleActorRegion(boolean isSimpleActorRegion) {
		this.isSimpleActorRegion = isSimpleActorRegion;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}
	
	public EPlayerType getPlayerType() {
		return ePlayerType;
	}

	public void setPlayerType(EPlayerType ePlayerType) {
		this.ePlayerType = ePlayerType;
	}

	public float getMovementSpeed() {
		return movementSpeed;
	}

	public String getMoveDirection() {
		return moveDirection;
	}

	public ObjectInfo getObjectInfo() {
		return objectInfo;
	}

	public EImagePath getPortraitPath() {
		return portraitPath;
	}

}
