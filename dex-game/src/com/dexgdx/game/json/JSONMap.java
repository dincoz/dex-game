package com.dexgdx.game.json;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.util.ObjectInfo;

public class JSONMap {

	private ObjectInfo objectInfo;
	private List<EImagePath> imagePaths;
	
	public JSONMap() {
		super();
		imagePaths = new ArrayList<EImagePath>();
	}
	
	public ObjectInfo getObjectInfo() {
		return objectInfo;
	}
	public void setObjectInfo(ObjectInfo objectInfo) {
		this.objectInfo = objectInfo;
	}
	public List<EImagePath> getImagePaths() {
		return imagePaths;
	}
	public void setImagePaths(List<EImagePath> imagePaths) {
		this.imagePaths = imagePaths;
	}
	public void addImagePath(EImagePath imagePath) {
		this.imagePaths.add(imagePath);
	}
	
}
