package com.dexgdx.game.json;

import java.io.Serializable;
import java.util.List;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.constants.EObjectInfo;

public class JSONAnimationObject implements Serializable {

	private static final long serialVersionUID = 5018753134339469290L;
	
	private String spriteSheetPath;
	private int columnSize;
	private int rowSize;
	private int stopRowIndex;
	
	private EObjectInfo playerObjectInfo;
	private int customNameId;
	private int animationType;
	
	private List<EMoveDirection> eMoveDirections;
	
	public JSONAnimationObject() {
		super();
	}
	
	public JSONAnimationObject(String spriteSheetPath, int columnSize,
			int rowSize, int stopRowIndex, EObjectInfo playerObjectInfo, int costumeNameId,
			int animationType, List<EMoveDirection> eMoveDirections) {
		super();
		this.spriteSheetPath = spriteSheetPath;
		this.columnSize = columnSize;
		this.rowSize = rowSize;
		this.stopRowIndex = stopRowIndex;
		this.playerObjectInfo = playerObjectInfo;
		this.customNameId = costumeNameId;
		this.animationType = animationType;
		this.eMoveDirections = eMoveDirections;
	}
	
	public String getSpriteSheetPath() {
		return spriteSheetPath;
	}
	public void setSpriteSheetPath(String spriteSheetPath) {
		this.spriteSheetPath = spriteSheetPath;
	}
	public int getColumnSize() {
		return columnSize;
	}
	public void setColumnSize(int columnSize) {
		this.columnSize = columnSize;
	}
	public int getRowSize() {
		return rowSize;
	}
	public void setRowSize(int rowSize) {
		this.rowSize = rowSize;
	}
	public int getStopRowIndex() {
		return stopRowIndex;
	}
	public void setStopRowIndex(int stopRowIndex) {
		this.stopRowIndex = stopRowIndex;
	}

	public EObjectInfo getPlayerObjectInfo() {
		return playerObjectInfo;
	}

	public void setPlayerObjectInfo(EObjectInfo playerObjectInfo) {
		this.playerObjectInfo = playerObjectInfo;
	}

	public int getCostumeNameId() {
		return customNameId;
	}

	public void setCostumeNameId(int costumeNameId) {
		this.customNameId = costumeNameId;
	}

	public int getAnimationType() {
		return animationType;
	}

	public void setAnimationType(int animationType) {
		this.animationType = animationType;
	}

	public List<EMoveDirection> getMoveDirections() {
		return eMoveDirections;
	}

	public void setMoveDirections(List<EMoveDirection> eMoveDirections) {
		this.eMoveDirections = eMoveDirections;
	}
	
}
