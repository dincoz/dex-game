package com.dexgdx.game.json;

import java.io.Serializable;

import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.util.ObjectInfo;

public class JSONInventoryItem implements Serializable{
	private static final long serialVersionUID = -1194666270412678116L;

	private ObjectInfo objectInfo;
	
	private float x;
	private float y;
	private float height;
	private float width;
	
	private EImagePath inventoryImagePath;
	
	
	public JSONInventoryItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	public JSONInventoryItem(ObjectInfo objectInfo, float x, float y,
			float height, float width, EImagePath imagePath) {
		super();
		this.objectInfo = objectInfo;
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.inventoryImagePath = imagePath;
	}
	public ObjectInfo getObjectInfo() {
		return objectInfo;
	}
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public float getHeight() {
		return height;
	}
	public float getWidth() {
		return width;
	}
	public EImagePath getInventoryImagePath() {
		return inventoryImagePath;
	}
}
