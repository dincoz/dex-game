package com.dexgdx.game.json;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Json;
import com.dexgdx.game.Game;
import com.dexgdx.game.animation.AnimationInfoKey;
import com.dexgdx.game.animation.DexAnimation;
import com.dexgdx.game.constants.EActionType;
import com.dexgdx.game.constants.EImagePath;
import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.constants.EObjectInfo;
import com.dexgdx.game.constants.EPlayerType;
import com.dexgdx.game.object.AbstractActor;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.InventoryItem;
import com.dexgdx.game.object.PlayerActor;
import com.dexgdx.game.object.SceneMap;

@SuppressWarnings("unchecked")
public class JSONManager {
	
	private Game game;
	private Json json;
	
	public JSONManager(Game game) {
		super();
		this.game = game;
		
		json = new Json();
	}
	
	public String prettyPrint(Object object){
		return json.prettyPrint(object);
	}

    public void saveJSONActors(){
		Json json = new Json();
		ArrayList<JSONActor> actors = new ArrayList<JSONActor>();
		actors.add(new JSONActor(EPlayerType.PLAYER, true, 210, 500, -1, -1, -1, "EAST", EObjectInfo.BRO1.getObjectInfo(), EImagePath.PORTRAIT_BRO1));
		actors.add(new JSONActor(EPlayerType.PLAYER, true, 110, 500, -1, -1, -1, "EAST", EObjectInfo.BRO2.getObjectInfo(), EImagePath.PORTRAIT_BRO2));
		actors.add(new JSONActor(EPlayerType.PLAYER, true, 160, 550, -1, -1, -1, "EAST", EObjectInfo.MARY.getObjectInfo(), EImagePath.PORTRAIT_SIS));
//		actors.add(new JSONActor(EPlayerType.PLAYER, true, 410, 500, -1, -1, -1, "EAST", EObjectInfo.WARRIOR.getObjectInfo(), EImagePath.PORTRAIT_NONE));
//		actors.add(new JSONActor(EPlayerType.PLAYER, true, 410, 1000, -1, -1, -1, "EAST", EObjectInfo.WARRIOR.getObjectInfo(), EImagePath.PORTRAIT_BRO));
		actors.add(new JSONActor(EPlayerType.NEUTRAL, true, 110, 700, -1, -1, -1, "NORTH", EObjectInfo.MARY.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.PLAYER, true, 170, 730, -1, -1, -1, "NORTH_EAST", EObjectInfo.JENNA.getObjectInfo()));
		
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 220, 250, -1, -1, 0.5f, "SOUTH", EObjectInfo.GRIDLOCK.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 290, 260, -1, -1, 0.5f, "SOUTH", EObjectInfo.ORC.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 360, 300, -1, -1, 0.5f, "SOUTH", EObjectInfo.GRIDLOCK.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 170, 380, -1, -1, 0.4f, "SOUTH", EObjectInfo.MARY.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 900, 530, -1, -1, 0.7f, "NORTH_EAST", EObjectInfo.TIM.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 850, 230, -1, -1, 0.4f, "NORTH_EAST", EObjectInfo.ORC.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 800, 830, -1, -1, 0.4f, "NORTH_EAST", EObjectInfo.ORC.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 900, 730, -1, -1, 0.7f, "NORTH_EAST", EObjectInfo.GRIDLOCK.getObjectInfo(), null));
//		actors.add(new JSONActor(EPlayerType.ENEMY, true, 1100, 930, -1, -1, 0.7f, "NORTH_EAST", EObjectInfo.TIM.getObjectInfo(), null));
		
		String jsonString = json.prettyPrint(actors);
//    	System.out.print(jsonString);
		
		FileHandle file = Gdx.files.local("data/json/actors.json");
		file.writeString(jsonString, false);
    }
    
    public void saveJSONAnimationObjects(){
		Json json = new Json();
		ArrayList<JSONAnimationObject> jSONAnimationObjects = new ArrayList<JSONAnimationObject>();

		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH_WEST, EMoveDirection.SOUTH, EMoveDirection.SOUTH_EAST, EMoveDirection.WEST, 
				EMoveDirection.NORTH_WEST, EMoveDirection.EAST, EMoveDirection.NORTH_EAST, EMoveDirection.NORTH,
				null, null, null, null, null, null, null, null,
				"actors/naked_man_walk.png", 9, 8, 0, EObjectInfo.TIM, 1, 1));

		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, EMoveDirection.SOUTH_WEST, EMoveDirection.WEST, EMoveDirection.NORTH_WEST,
				EMoveDirection.NORTH, EMoveDirection.NORTH_EAST, EMoveDirection.EAST, EMoveDirection.SOUTH_EAST,
				null, null, null, null, null, null, null, null,
				"actors/warrior_woman_walk.gif", 8, 8, 7, EObjectInfo.BRO1, 1, 1));

		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, EMoveDirection.SOUTH_WEST, EMoveDirection.WEST, EMoveDirection.NORTH_WEST,
				EMoveDirection.NORTH, EMoveDirection.NORTH_EAST, EMoveDirection.EAST, EMoveDirection.SOUTH_EAST,
				null, null, null, null, null, null, null, null,
				"actors/normal_woman_walk.png", 9, 8, 0, EObjectInfo.MARY, 1, 1));

		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.NORTH_WEST, EMoveDirection.NORTH, EMoveDirection.NORTH_EAST, EMoveDirection.EAST,
				EMoveDirection.WEST, EMoveDirection.SOUTH_WEST, EMoveDirection.SOUTH, EMoveDirection.SOUTH_EAST,
				null, null, null, null, null, null, null, null,
				"actors/gridlock_walk.png", 9, 8, 0, EObjectInfo.GRIDLOCK, 1, 1));

		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, EMoveDirection.SOUTH_WEST, EMoveDirection.WEST, EMoveDirection.NORTH_WEST,
				EMoveDirection.NORTH, EMoveDirection.NORTH_EAST, EMoveDirection.EAST, EMoveDirection.SOUTH_EAST, 
				null, null, null, null, null, null, null, null,
				"actors/orc_walk.png", 12, 8, 5, EObjectInfo.ORC, 1, 1));
		
		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, EMoveDirection.SOUTH_WEST, EMoveDirection.SOUTH_WEST_2, EMoveDirection.SOUTH_WEST_3,
				EMoveDirection.WEST, EMoveDirection.NORTH_WEST, EMoveDirection.NORTH_WEST_2, EMoveDirection.NORTH_WEST_3, 
				EMoveDirection.NORTH, EMoveDirection.NORTH_EAST, EMoveDirection.NORTH_EAST_2, EMoveDirection.NORTH_EAST_3, 
				EMoveDirection.EAST, EMoveDirection.SOUTH_EAST, EMoveDirection.SOUTH_EAST_2, EMoveDirection.SOUTH_EAST_3, 
				"actors/warrior_walk.png", 8, 16, 9, EObjectInfo.BRO2, 1, 1));

		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null,
				"baseobjects/tree.gif", 1, 1, 0, EObjectInfo.TREE, 1, 1));
		
		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null,
				"baseobjects/bullet.png", 1, 1, 0, EObjectInfo.BULLET, 1, 1));
		
		jSONAnimationObjects.add(loadTestObject(
				EMoveDirection.SOUTH, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null,
				"baseobjects/sword.png", 1, 1, 0, EObjectInfo.SWORD, 1, 1));
		
		String jsonString = json.prettyPrint(jSONAnimationObjects);
//    	System.out.print(jsonString);
		
		FileHandle file = Gdx.files.local("data/json/animations.json");
		file.writeString(jsonString, false);
    }

    public void saveJSONMap(){
		Json json = new Json();
		JSONMap map = new JSONMap();
		map.setObjectInfo(EObjectInfo.MAP_FOREST_1.getObjectInfo());
		map.addImagePath(EImagePath.FOREST_MAP);
		map.addImagePath(EImagePath.FOREST_MAP_OVERLAY);
		
		String jsonString = json.prettyPrint(map);
//    	System.out.print(jsonString);
		
		FileHandle file = Gdx.files.local("data/json/maps.json");
		file.writeString(jsonString, false);
		
    }

    public void saveJSONInventoryItems(){
		Json json = new Json();
		List<JSONInventoryItem> items = new ArrayList<JSONInventoryItem>();
		items.add(new JSONInventoryItem(EObjectInfo.SWORD.getObjectInfo(), 420, 250, game.defaultConstants.getActorRingHeight(), game.defaultConstants.getActorRingWidth(), EImagePath.INVENTORY_SWORD_1));
//		items.add(new JSONInventoryItem(EObjectInfo.SWORD.getObjectInfo(), 250, 210, 60, 60, EImagePath.SWORD));
		String jsonString = json.prettyPrint(items);
//    	System.out.print(jsonString);
		
		FileHandle file = Gdx.files.local("data/json/items.json");
		file.writeString(jsonString, false);
		
    }
    
    private JSONAnimationObject loadTestObject(
    		EMoveDirection moveDirection1,
    		EMoveDirection moveDirection2,
    		EMoveDirection moveDirection3,
    		EMoveDirection moveDirection4,
    		EMoveDirection moveDirection5,
    		EMoveDirection moveDirection6,
    		EMoveDirection moveDirection7,
    		EMoveDirection moveDirection8,
    		EMoveDirection moveDirection9,
    		EMoveDirection moveDirection10,
    		EMoveDirection moveDirection11,
    		EMoveDirection moveDirection12,
    		EMoveDirection moveDirection13,
    		EMoveDirection moveDirection14,
    		EMoveDirection moveDirection15,
    		EMoveDirection moveDirection16,
    		String spriteSheetPath, int columnSize,
			int rowSize, int stopColumnIndex, EObjectInfo playerObjectInfo, int costumeNameId,
			int animationType){
    	
		List<EMoveDirection> eMoveDirections = new ArrayList<EMoveDirection>();
		eMoveDirections.add(moveDirection1);
		if(moveDirection2 != null)
			eMoveDirections.add(moveDirection2);
		if(moveDirection3 != null)
			eMoveDirections.add(moveDirection3);
		if(moveDirection4 != null)
			eMoveDirections.add(moveDirection4);
		if(moveDirection5 != null)
			eMoveDirections.add(moveDirection5);
		if(moveDirection6 != null)
			eMoveDirections.add(moveDirection6);
		if(moveDirection7 != null)
			eMoveDirections.add(moveDirection7);
		if(moveDirection8 != null)
			eMoveDirections.add(moveDirection8);
		if(moveDirection9 != null)
			eMoveDirections.add(moveDirection9);
		if(moveDirection10 != null)
			eMoveDirections.add(moveDirection10);
		if(moveDirection11 != null)
			eMoveDirections.add(moveDirection11);
		if(moveDirection12 != null)
			eMoveDirections.add(moveDirection12);
		if(moveDirection13 != null)
			eMoveDirections.add(moveDirection13);
		if(moveDirection14 != null)
			eMoveDirections.add(moveDirection14);
		if(moveDirection15 != null)
			eMoveDirections.add(moveDirection15);
		if(moveDirection16 != null)
			eMoveDirections.add(moveDirection16);
		return new JSONAnimationObject(spriteSheetPath, columnSize, rowSize, stopColumnIndex, playerObjectInfo, costumeNameId, animationType, eMoveDirections);
    }

	public void loadActorsFromJSON(){
		ArrayList<JSONActor> jSONActors = json.fromJson(ArrayList.class, Gdx.files.local("data/json/actors.json"));

		AbstractActor actor = null;
		BaseRegion region = null;
		for (JSONActor jSONActor : jSONActors) {
			
			if(jSONActor.isSimpleActorRegion()){
				region = new BaseRegion(jSONActor.getX(), jSONActor.getY(), 
						game.defaultConstants.getActorRingHeight(), game.defaultConstants.getActorRingWidth());
			}
			else {
				region = new BaseRegion(jSONActor.getX(), jSONActor.getY(), jSONActor.getHeight(), jSONActor.getWidth());
			}
			
			float movementSpeed = jSONActor.getMovementSpeed() == -1 ? 
					game.defaultConstants.getPlayerSpeed() : jSONActor.getMovementSpeed();
			
			EMoveDirection eMoveDirection = jSONActor.getMoveDirection() == null ? 
					EMoveDirection.SOUTH : EMoveDirection.valueOf(jSONActor.getMoveDirection());
			
			if(jSONActor.getPlayerType() == EPlayerType.ENEMY){
				actor = game.getObjectFactory().enemy(jSONActor.getObjectInfo(), region.x, region.y, 
						region.height, region.width, movementSpeed, game.getTexture(jSONActor.getPortraitPath()));
				
				actor.addPossiblePassiveAction(EActionType.HEAL);
				actor.addPossiblePassiveAction(EActionType.GUARD);
				actor.addPossibleActiveAction(EActionType.MISSILE);
				actor.addPossibleActiveAction(EActionType.BULLET);
				actor.addPossibleActiveAction(EActionType.TRAIL);
				
			}
			else if(jSONActor.getPlayerType() == EPlayerType.PLAYER){
				actor = game.getObjectFactory().player(jSONActor.getObjectInfo(), region.x, region.y, 
						region.height, region.width, movementSpeed, game.getTexture(jSONActor.getPortraitPath()));
				PlayerActor player = (PlayerActor) actor;

				if(jSONActor.getObjectInfo().getInfoNumber() == EObjectInfo.BRO1.getObjectInfo().getInfoNumber()){
					player.addPossibleActiveAction(EActionType.BULLET);
					player.selectedActiveAttackAction = EActionType.BULLET;
					player.addPossiblePassiveAction(EActionType.HEAL);
				}
				else if(jSONActor.getObjectInfo().getInfoNumber() == EObjectInfo.BRO2.getObjectInfo().getInfoNumber()){
					player.addPossibleActiveAction(EActionType.BULLET);
					player.selectedActiveAttackAction = EActionType.BULLET;
				}
				else{
					player.addPossiblePassiveAction(EActionType.GUARD);
					player.addPossibleActiveAction(EActionType.MISSILE);
					player.addPossibleActiveAction(EActionType.TRAIL);
					player.selectedActiveAttackAction = EActionType.TRAIL;
					player.addPossiblePassiveAction(EActionType.HEAL);
				}
				
			}
			else if(jSONActor.getPlayerType() == EPlayerType.NEUTRAL){
				actor = game.getObjectFactory().neutralActor(jSONActor.getObjectInfo(), region.x, region.y, 
						region.height, region.width, movementSpeed, game.getTexture(jSONActor.getPortraitPath()));
			}

			actor.setMoveDirection(eMoveDirection);
			
			game.gameObjects.addActor(actor);
		}
	}

	public void loadAnimationsFromJSON(){
		ArrayList<JSONAnimationObject> jSONAnimationObjects = json.fromJson(ArrayList.class, Gdx.files.local("data/json/animations.json"));
		
		for (JSONAnimationObject jsonAnimationObject : jSONAnimationObjects) {
			Texture texture = new Texture(
					Gdx.files.internal("data/objects/" + jsonAnimationObject.getSpriteSheetPath()));
	        TextureRegion[][] tmp = TextureRegion.split(texture, 
	        		texture.getWidth() / jsonAnimationObject.getColumnSize(), 
	        		texture.getHeight() / jsonAnimationObject.getRowSize());
	        
	        for (int rowIndex = 0; rowIndex < jsonAnimationObject.getRowSize(); rowIndex++) {
	        	EMoveDirection eMoveDirection = jsonAnimationObject.getMoveDirections().get(rowIndex);
	        	TextureRegion[] textureRegions = new TextureRegion[jsonAnimationObject.getColumnSize()];
	            int textureIndex = 0;
	            for (int colIndex = 0; colIndex < jsonAnimationObject.getColumnSize(); colIndex++) {
	            	if(!tmp[rowIndex][colIndex].isFlipY())
	            		tmp[rowIndex][colIndex].flip(false, true);
	            	textureRegions[textureIndex++] = tmp[rowIndex][colIndex];
	            }
	            AnimationInfoKey animationInfoKey = AnimationInfoKey.fromData(jsonAnimationObject, eMoveDirection.index);
	            DexAnimation dexAnimation = new DexAnimation(eMoveDirection, jsonAnimationObject.getStopRowIndex(), 0.05f, textureRegions);
	            game.getAnimations().put(animationInfoKey.getKey(), dexAnimation);
	        }
		}
        game.stateTime = 0f;  
	}
	
	public void loadMapsFromJSON() {
		JSONMap jSONmap = json.fromJson(JSONMap.class, Gdx.files.local("data/json/maps.json"));
		SceneMap map = game.getObjectFactory().map(jSONmap.getObjectInfo(), game.getTexture(jSONmap.getImagePaths().get(0)));
		for (int i = 1 ; i < jSONmap.getImagePaths().size() ; ++i) {
			map.addImage(game.getTexture(jSONmap.getImagePaths().get(i)));
		}
		game.currentMap = map;
	}

	public void loadInventoryItemsFromJSON() {
		List<JSONInventoryItem> items = json.fromJson(ArrayList.class, Gdx.files.local("data/json/items.json"));
		for (JSONInventoryItem jsonInventoryItem : items) {
			InventoryItem inventoryItem = game.getObjectFactory().inventoryItem(
					jsonInventoryItem.getObjectInfo(), game.getTexture(jsonInventoryItem.getInventoryImagePath()), jsonInventoryItem.getX(), jsonInventoryItem.getY(), jsonInventoryItem.getHeight(), jsonInventoryItem.getWidth());
			game.gameObjects.addOtherSceneObject(inventoryItem);
		}
	}
}
