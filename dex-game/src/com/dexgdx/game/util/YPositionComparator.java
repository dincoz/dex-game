package com.dexgdx.game.util;

import java.util.Comparator;

import com.dexgdx.game.object.SolidObject;

public class YPositionComparator implements Comparator<SolidObject> {
    @Override
    public int compare(SolidObject baseObject1, SolidObject baseObject2) {
		return Float.valueOf(baseObject1.y+baseObject1.height).compareTo(Float.valueOf(baseObject2.y+baseObject2.height));
    }
}

