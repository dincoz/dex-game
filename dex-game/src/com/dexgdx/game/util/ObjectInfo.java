package com.dexgdx.game.util;

public class ObjectInfo {
	
	private int infoNumber;
	private String name;
	
	public ObjectInfo() {
		super();
	}
	
	public ObjectInfo(int infoNumber, String name) {
		super();
		this.infoNumber = infoNumber;
		this.name = name;
	}
	public int getInfoNumber() {
		return infoNumber;
	}
	public void setInfoNumber(int infoNumber) {
		this.infoNumber = infoNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
