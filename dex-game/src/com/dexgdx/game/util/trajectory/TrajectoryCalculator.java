package com.dexgdx.game.util.trajectory;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.object.Trajectory;

public class TrajectoryCalculator {

	private Trajectory trajectory;
	
	public TrajectoryCalculator(float speed) {
		trajectory = new Trajectory(speed,  Math.abs(speed), (float) Math.sqrt(2 * (speed * speed)));
	}
	
	public Trajectory calculate(MobileObject movingObject){
		return calculate(movingObject, movingObject.getMoveTarget());
	}
	
	public Trajectory calculate(BaseRegion fromObject, BaseRegion targetRegion){
		float deltaX = targetRegion.getX() - fromObject.getX();
		float deltaY = targetRegion.getY() - fromObject.getY();
		double divider = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
		trajectory.x = (float) ((trajectory.getSpeed() * deltaX) / divider);
		trajectory.y = (float) ((trajectory.getSpeed() * deltaY) / divider);
		return trajectory;
	}

	public Trajectory getTrajectory() {
		return trajectory;
	}
	
	public static EMoveDirection findDirection2(Trajectory trajectory, boolean visual){
		EMoveDirection direction = null;
		
		float control1 = trajectory.getAbsSpeed() * .25f;
		float control2 = trajectory.getAbsSpeed() * .5f;
		float control3 = trajectory.getAbsSpeed() * .75f;
		
		if(trajectory.x > 0){
			if(trajectory.y > control3){
				direction = EMoveDirection.SOUTH_EAST_3;
			}
			else if(trajectory.y > control2){
				direction = EMoveDirection.SOUTH_EAST_2;
			}
			else if(trajectory.y > control1){
				direction = EMoveDirection.SOUTH_EAST;
			}
			if(trajectory.y < control3 * -1){
				direction = EMoveDirection.NORTH_EAST_3;
			}
			else if(trajectory.y < control2 * -1){
				direction = EMoveDirection.NORTH_EAST_2;
			}
			else if(trajectory.y < control1 * -1){
				direction = EMoveDirection.NORTH_EAST;
			}
			else{
				direction = EMoveDirection.EAST;
			}
		}
		else if(trajectory.x < 0){
			if(trajectory.y > control3){
				direction = EMoveDirection.SOUTH_WEST_3;
			}
			else if(trajectory.y > control2){
				direction = EMoveDirection.SOUTH_WEST_2;
			}
			else if(trajectory.y > control1){
				direction = EMoveDirection.SOUTH_WEST;
			}
			if(trajectory.y < control3 * -1){
				direction = EMoveDirection.NORTH_WEST_3;
			}
			else if(trajectory.y < control2 * -1){
				direction = EMoveDirection.NORTH_WEST_2;
			}
			else if(trajectory.y < control1 * -1){
				direction = EMoveDirection.NORTH_WEST;
			}
			else{
				direction = EMoveDirection.WEST;
			}
		}
		else {
			if(trajectory.y > 0){
				direction = EMoveDirection.SOUTH;
			}
			else {
				direction = EMoveDirection.NORTH;
			}
		}
		return direction;
	}

	
	public static EMoveDirection findDirection(Trajectory trajectory, boolean visual){
		EMoveDirection direction = null;
		float comparison = visual ? 0.3f : 0f;
		comparison *= trajectory.getAbsSpeed();
		if(trajectory.x > comparison){
			if(trajectory.y > comparison){
				direction = EMoveDirection.SOUTH_EAST;
			}
			else if(trajectory.y < comparison * -1){
				direction = EMoveDirection.NORTH_EAST;
			}
			else {
				direction = EMoveDirection.EAST;
			}
		}
		else if(trajectory.x < comparison * -1){
			if(trajectory.y > comparison){
				direction = EMoveDirection.SOUTH_WEST;
			}
			else if(trajectory.y < comparison * -1){
				direction = EMoveDirection.NORTH_WEST;
			}
			else {
				direction = EMoveDirection.WEST;
			}
		}
		else {
			if(trajectory.y > comparison){
				direction = EMoveDirection.SOUTH;
			}
			else {
				direction = EMoveDirection.NORTH;
			}
		}
		return direction;
	}
	
	public static int getSimpleDirection(int moveDirection){
		int response = moveDirection;
		if(moveDirection == EMoveDirection.NORTH_EAST_2.index || moveDirection == EMoveDirection.NORTH_EAST_3.index)
			response = EMoveDirection.NORTH_EAST.index;
		if(moveDirection == EMoveDirection.SOUTH_EAST_2.index || moveDirection == EMoveDirection.SOUTH_EAST_3.index)
			response = EMoveDirection.SOUTH_EAST.index;
		if(moveDirection == EMoveDirection.SOUTH_WEST_2.index || moveDirection == EMoveDirection.SOUTH_WEST_3.index)
			response = EMoveDirection.SOUTH_WEST.index;
		if(moveDirection == EMoveDirection.NORTH_WEST_2.index || moveDirection == EMoveDirection.NORTH_WEST_3.index)
			response = EMoveDirection.NORTH_WEST.index;
		return response;
	}
}
