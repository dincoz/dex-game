package com.dexgdx.game.util.trajectory.node;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.object.Trajectory;
import com.dexgdx.game.object.BasePoint;

public abstract class TrajectoryNode extends BasePoint {
	
	public TrajectoryNode next;
	public EMoveDirection direction;
	
	public TrajectoryNode getNext() {
		return next;
	}
	
	public abstract TrajectoryNode clone();
	
	public abstract TrajectoryNode initalizeFrom(Trajectory trajectory);
	
}
