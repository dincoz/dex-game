package com.dexgdx.game.util.trajectory.node;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.object.Trajectory;


public class EastTrajectory extends TrajectoryNode {

	@Override
	public TrajectoryNode initalizeFrom(Trajectory trajectory) {
		y = 0;
		x = trajectory.getAbsSpeed();
		direction = EMoveDirection.EAST;
		return this;
	}

	@Override
	public TrajectoryNode clone() {
		EastTrajectory eastTrajectory = new EastTrajectory();
		eastTrajectory.setFromPoint(this);
		return eastTrajectory;
	}
	
}
