package com.dexgdx.game.util.trajectory.node;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.object.Trajectory;

public class NorthTrajectoryNode extends TrajectoryNode {

	@Override
	public TrajectoryNode initalizeFrom(Trajectory trajectory) {
		x = 0;
		y = trajectory.getAbsSpeed() * -1;
		direction = EMoveDirection.NORTH;
		return this;
	}
	
	@Override
	public TrajectoryNode clone() {
		NorthTrajectoryNode trajectory = new NorthTrajectoryNode();
		trajectory.setFromPoint(this);
		return trajectory;
	}
	
}
