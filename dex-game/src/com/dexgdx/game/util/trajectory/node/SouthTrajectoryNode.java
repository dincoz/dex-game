package com.dexgdx.game.util.trajectory.node;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.object.Trajectory;

public class SouthTrajectoryNode extends TrajectoryNode {

	@Override
	public TrajectoryNode initalizeFrom(Trajectory trajectory) {
		x = 0;
		y = trajectory.getAbsSpeed();
		direction = EMoveDirection.SOUTH;
		return this;
	}

	@Override
	public TrajectoryNode clone() {
		SouthTrajectoryNode trajectory = new SouthTrajectoryNode();
		trajectory.setFromPoint(this);
		return trajectory;
	}
}
