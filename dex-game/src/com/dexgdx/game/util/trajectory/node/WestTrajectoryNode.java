package com.dexgdx.game.util.trajectory.node;

import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.object.Trajectory;

public class WestTrajectoryNode extends TrajectoryNode {

	@Override
	public TrajectoryNode initalizeFrom(Trajectory trajectory) {
		y = 0;
		x = trajectory.getAbsSpeed() * -1;
		direction = EMoveDirection.WEST;
		return this;
	}
	
	@Override
	public TrajectoryNode clone() {
		WestTrajectoryNode trajectory = new WestTrajectoryNode();
		trajectory.setFromPoint(this);
		return trajectory;
	}
	
}
