package com.dexgdx.game.util.pathcalc;

import java.util.HashMap;
import java.util.Map;

import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EMoveStopCondition;
import com.dexgdx.game.object.SolidObject;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.object.Trajectory;
import com.dexgdx.game.util.trajectory.node.EastTrajectory;
import com.dexgdx.game.util.trajectory.node.NorthTrajectoryNode;
import com.dexgdx.game.util.trajectory.node.SouthTrajectoryNode;
import com.dexgdx.game.util.trajectory.node.TrajectoryNode;
import com.dexgdx.game.util.trajectory.node.WestTrajectoryNode;
import com.dexgdx.game.action.*;

public class WallHugDirectPathCalcSession extends AbstractPathFindCalcSession {

	private Map<TrajectoryNode, TrajectoryNode> mapLeftHandOf;
	private Map<TrajectoryNode, TrajectoryNode> mapRightHandOf;

	TrajectoryNode et;
	TrajectoryNode wt;
	TrajectoryNode st;
	TrajectoryNode nt;

	private TrajectoryNode currentTrajectoryNode;
	private Trajectory currentTrajectory;
	
	private Map<TrajectoryNode, TrajectoryNode> currentHandSideMap;
	
	private boolean collisionAvoided;
	private SolidObject collidedObject;

	private int collTrajCounter;
	private Map<TrajectoryNode, TrajectoryNode> previousHandSideMap;
	private TrajectoryNode previousTrajectoryNode;
	
	public WallHugDirectPathCalcSession(Game game) {
		super(game);
		collTrajCounter = 0;
	}
	
	@Override
	public void initialize(MovementAction movementAction, MobileObject movingObject,
			EMoveStopCondition stopCondition) {

		super.initialize(movementAction, movingObject, stopCondition);
		currentTrajectory = new Trajectory(movingObject.getMovementSpeed());

		detectingCollision = true;
		
		et = (new EastTrajectory()).initalizeFrom(trajCalc.getTrajectory());
		wt = (new WestTrajectoryNode()).initalizeFrom(trajCalc.getTrajectory());
		st = (new SouthTrajectoryNode()).initalizeFrom(trajCalc.getTrajectory());
		nt = (new NorthTrajectoryNode()).initalizeFrom(trajCalc.getTrajectory());
		
		mapLeftHandOf = new HashMap<TrajectoryNode, TrajectoryNode>();
		mapLeftHandOf.put(et, nt);
		mapLeftHandOf.put(nt, wt);
		mapLeftHandOf.put(wt, st);
		mapLeftHandOf.put(st, et);
		
		mapRightHandOf = new HashMap<TrajectoryNode, TrajectoryNode>();
		mapRightHandOf.put(et, st);
		mapRightHandOf.put(st, wt);
		mapRightHandOf.put(wt, nt);
		mapRightHandOf.put(nt, et);
	}
	@Override
	protected void initializeCalculation(){
		trajCalc.calculate(movingObject);
		targetPreviousRegion.setFromRegion(movingObject.getMoveTarget());
	}
	
	@Override
	protected BaseRegion getNextCalculatedPosition(){
		if(!movingObject.checkAndIncrementWaitState()){
			calculatedPreviousRegion.setFromRegion(calculatedNextRegion);
			calculatedNextRegion.x += trajCalc.getTrajectory().x;
			calculatedNextRegion.y += trajCalc.getTrajectory().y;
			if(detectingCollision)
				avoidCollision();
		}
		return calculatedNextRegion;
	}
	
	protected void avoidCollision() {
		if(movingObject.getMoveTarget() == null) {
			collisionAvoided = true;
			return;
		}
		collidedObject = game.gameObjects.getCollidingObject(movingObject, calculatedNextRegion);
		
		if(collidedObject != null){
			
			if(collidedObject == movingObject.getMoveTarget() &&
					stopCondition == EMoveStopCondition.WHEN_TARGET_TOUCHED){
				reachedTarget = true;
				return;
			}
			
			boolean collidedMovingObject = collidedObject instanceof MobileObject;
			MobileObject mobileCollidedObject = null;
			if(collidedMovingObject){
				mobileCollidedObject = (MobileObject) collidedObject;
			}
			
			if(currentTrajectoryNode == null){
				float cy = collidedObject.y;
				float cyh = collidedObject.height + cy;
				float cx = collidedObject.x;
//				float cxw = collidedObject.width + cx;
				float my = movingObject.y;
				float myh = movingObject.height + my;
				float mx = movingObject.x;
				float mxw = movingObject.width + mx;
				float ty = movingObject.getMoveTarget().y;
				float tx = movingObject.getMoveTarget().x;
				
				if(cy > myh){
					if(tx > cx){
						currentTrajectoryNode = et;
						currentHandSideMap = mapRightHandOf;
					}
					else {
						currentTrajectoryNode = wt;
						currentHandSideMap = mapLeftHandOf;
					}
				}
				else if(my > cyh){
					if(tx > cx){
						currentTrajectoryNode = et;
						currentHandSideMap = mapLeftHandOf;
					}
					else {
						currentTrajectoryNode = wt;
						currentHandSideMap = mapRightHandOf;
					}
				}
				else if(cx > mxw){
					if(ty > cy){
						currentTrajectoryNode = st;
						currentHandSideMap = mapLeftHandOf;
					}
					else {
						currentTrajectoryNode = nt;
						currentHandSideMap = mapRightHandOf;
					}
				}
				else{
					if(ty > cy){
						currentTrajectoryNode = st;
						currentHandSideMap = mapRightHandOf;
					}
					else {
						currentTrajectoryNode = nt;
						currentHandSideMap = mapLeftHandOf;
					}
				}
			}
			
			int collisionAvoiderFailSafeCounter = 0;
			do{
				collisionAvoided = true;
				calculatedNextRegion.setFromRegion(calculatedPreviousRegion);
				calculatedNextRegion.x += currentHandSideMap.get(currentTrajectoryNode).x;
				calculatedNextRegion.y += currentHandSideMap.get(currentTrajectoryNode).y;
				if(game.gameObjects.getCollidingObject(movingObject, calculatedNextRegion) != null){
					calculatedNextRegion.setFromRegion(calculatedPreviousRegion);
					calculatedNextRegion.x += currentTrajectoryNode.x;
					calculatedNextRegion.y += currentTrajectoryNode.y;
					if(game.gameObjects.getCollidingObject(movingObject, calculatedNextRegion) != null){
						currentTrajectoryNode = currentHandSideMap.get(currentTrajectoryNode);
						collisionAvoided = false;
					}
				}
				collisionAvoiderFailSafeCounter++;
				if(collisionAvoiderFailSafeCounter > 100000){
					collisionAvoiderFailSafeCounter = 0;
					break;
				}
			}
			while(!collisionAvoided);
			
			if(previousHandSideMap != currentHandSideMap ||
					previousTrajectoryNode != currentTrajectoryNode){
				collTrajCounter = 0;
				previousHandSideMap = currentHandSideMap;
				previousTrajectoryNode = currentTrajectoryNode;
			}
			else if(collTrajCounter > 70 && collidedMovingObject && !mobileCollidedObject.checkAndIncrementWaitState()){
				movingObject.setWaitState(100);
//				currentTrajectoryNode = nt;
//				currentHandSideMap = mapLeftHandOf;
//				collisionAvoided = false;
				collTrajCounter = 0;
			}
			else {
				collTrajCounter++;
			}
		}
		else {
			if(currentTrajectoryNode != null){
				//Hesaplama isleminden sonra ilk hesaplanan do�rultuda ilerlenebilmi�se, yeni do�rultuyu hesapla
				currentTrajectoryNode = null;
				trajCalc.calculate(movingObject);
			}
		}
	}
	
	@Override
	public Trajectory getCurrentTrajectory() {
		if(currentTrajectoryNode == null)
			return trajCalc.getTrajectory();
		currentTrajectory.set(currentTrajectoryNode.x, currentTrajectoryNode.y);
		return currentTrajectory;
	}
	
	@Override
	public boolean isCalculationFinished() {
		return reachedTarget;
	}
	
	@Override
	protected void finalizeCalculation() {
	}
	
}
