package com.dexgdx.game.util.pathcalc;

import com.dexgdx.game.Game;
import com.dexgdx.game.constants.EMoveDirection;
import com.dexgdx.game.constants.EMoveStopCondition;
import com.dexgdx.game.object.BaseRegion;
import com.dexgdx.game.object.MobileObject;
import com.dexgdx.game.object.Trajectory;
import com.dexgdx.game.util.trajectory.TrajectoryCalculator;
import com.dexgdx.game.action.*;

public abstract class AbstractPathFindCalcSession implements IPathCalcSession {
	
	protected Game game;
	protected MovementAction movementAction;
	
	protected boolean detectingCollision;
	protected MobileObject movingObject;
	protected BaseRegion targetPreviousRegion;
	protected BaseRegion calculatedNextRegion;
	protected BaseRegion calculatedPreviousRegion;
	protected EMoveStopCondition stopCondition;
	protected TrajectoryCalculator trajCalc;
	protected boolean reachedTarget;

	protected int stepCount;
	protected int currentCount;
	
	public AbstractPathFindCalcSession(Game game) {
		this.game = game;
	}

	public void initialize(MovementAction movementAction, MobileObject movingObject, EMoveStopCondition stopCondition) {
		this.movementAction = movementAction;
		this.movingObject = movingObject;
		this.stopCondition = stopCondition;
		
		calculatedNextRegion = movingObject.cloneRegion(); 
		calculatedPreviousRegion = movingObject.cloneRegion(); 
		targetPreviousRegion = movingObject.getMoveTarget().cloneRegion();
		trajCalc = new TrajectoryCalculator(movingObject.getMovementSpeed());
		initializeCalculation();
	}

	@Override
	public EMoveDirection getDirection() {
		return TrajectoryCalculator.findDirection(trajCalc.getTrajectory(), true);
	}
	
	public Trajectory getCurrentTrajectory() {
		return trajCalc.getTrajectory();
	}
	

	@Override
	public BaseRegion getNextPosition() {
		if(!reachedTarget){
			if(movingObject.getMoveTarget() != null && !movingObject.getMoveTarget().isSamePositionAs(targetPreviousRegion)){
				initializeCalculation();
			}
			
			calculatedPreviousRegion.setFromRegion(calculatedNextRegion);
			calculatedNextRegion = getNextCalculatedPosition();
			
			currentCount++;
			
			if(hasRegionReachedTarget(calculatedNextRegion) || reachedTarget){
				setFinalPosition(calculatedNextRegion);
			}
		}
	
		return calculatedNextRegion;
	}
	
	protected boolean hasRegionReachedTarget(BaseRegion calculatedNextRegion) {
		boolean reachedTarget = false;
		if(stopCondition == EMoveStopCondition.WHEN_TARGET_TOUCHED){
			reachedTarget =  calculatedNextRegion.isCollidingWith(movingObject.getMoveTarget());
		}
		else if(stopCondition == EMoveStopCondition.WHEN_OVER){
			reachedTarget =  calculatedNextRegion.isOneStepAwayFrom(movingObject.getMoveTarget(), movingObject.getMovementSpeed());
		}
		else if(stopCondition == EMoveStopCondition.WHEN_STEPCOUNT_REACHED){
			reachedTarget = currentCount >= stepCount;
		}
		else if(stopCondition == EMoveStopCondition.WHEN_TOUCHED_OR_STEPCOUNT_REACHED){
			reachedTarget = 
			currentCount >= stepCount || 
			movementAction.customEndCondition();
		}
		//	Hedefe var�ld� ve calculatedNextRegion � SET ETME
		return reachedTarget;
	}

	public void setFinalPosition(BaseRegion calculatedNextRegion) {
		if(reachedTarget){
			if(stopCondition == EMoveStopCondition.WHEN_OVER){
				calculatedNextRegion.setFromRegion(movingObject.getMoveTarget());
			}
			else if(stopCondition == EMoveStopCondition.WHEN_TARGET_TOUCHED ||
				stopCondition == EMoveStopCondition.WHEN_TOUCHED_OR_STEPCOUNT_REACHED){
				calculatedNextRegion.setFromRegion(movingObject);
			}
		}
		finalizeCalculation();
		reachedTarget = true;
	}
	
	@Override
	public Trajectory getTrajectory() {
		return trajCalc.getTrajectory();
	}
	
	public void setStepCount(int stepCount) {
		this.stepCount = stepCount;
	}
	
	
	
	protected abstract void initializeCalculation();
	protected abstract BaseRegion getNextCalculatedPosition();
	protected abstract void finalizeCalculation();

	
}
