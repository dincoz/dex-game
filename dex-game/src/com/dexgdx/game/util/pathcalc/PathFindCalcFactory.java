package com.dexgdx.game.util.pathcalc;

import com.dexgdx.game.Game;

public class PathFindCalcFactory {
	
	private Game game;
	
	public PathFindCalcFactory(Game game) {
		super();
		this.game = game;
	}
	public WallHugDirectPathCalcSession newWallHugDirectPathCalcSession(){
		return new WallHugDirectPathCalcSession(game);
	}
}
