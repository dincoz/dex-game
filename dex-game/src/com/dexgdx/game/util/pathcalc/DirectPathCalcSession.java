package com.dexgdx.game.util.pathcalc;

import com.dexgdx.game.Game;
import com.dexgdx.game.object.BaseRegion;

public class DirectPathCalcSession extends AbstractPathFindCalcSession {
	
	public DirectPathCalcSession(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}
	@Override
	protected void initializeCalculation() {
		trajCalc.calculate(movingObject);
		targetPreviousRegion.setFromRegion(movingObject.getMoveTarget());
		
	}
	@Override
	protected BaseRegion getNextCalculatedPosition() {
		calculatedNextRegion.x += trajCalc.getTrajectory().x;
		calculatedNextRegion.y += trajCalc.getTrajectory().y;
		return calculatedNextRegion;
	}

	@Override
	public boolean isCalculationFinished() {
		return reachedTarget;
	}
	
	@Override
	protected void finalizeCalculation() {
		
	}
}
