package com.dexgdx.game.util.pathcalc;

import com.dexgdx.game.action.*;
import com.dexgdx.game.constants.*;
import com.dexgdx.game.object.*;

public interface IPathCalcSession {

	public void initialize(MovementAction movementAction, MobileObject movingObject, EMoveStopCondition stopCondition);
	public BaseRegion getNextPosition();
	public boolean isCalculationFinished();
	public EMoveDirection getDirection();
	public Trajectory getTrajectory();
	public void setStepCount(int stepCount);
}
