package com.dexgdx.game.flow;

public class Element {

	public int id;
	public int gameInstId;
	public int sceneInstId;
	public int elementDefId;
	public float x;
	public float y;
	public float height;
	public float width;
}
