package com.dexgdx.game.flow;

import java.util.List;

public class Scene {
	public int id;
	public int gameInstId;
	public int sceneDefId;
	public boolean enabled;

	public List<Element> elements;
}
