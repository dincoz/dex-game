package com.dexgdx.game.dialog;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.object.AbstractActor;

public class DialogFlow implements IDialogStep{
	
	private int currentLine;
	public List<DialogSimpleLine> lines;
	public DialogChoiceSet choiceSet;
	
	public DialogFlow() {
		super();
		currentLine = 0;
		lines = new ArrayList<DialogSimpleLine>();
		choiceSet = new DialogChoiceSet();
	}

	public DialogFlow addLine(AbstractActor actor, String text) {
		lines.add(new DialogSimpleLine(actor, text));
		return this;
	}
	
	public DialogFlow addChoice(int choiceFlowIndex, String choiceFlowText){
		choiceSet.addText(choiceFlowIndex, choiceFlowText);
		return this;
	}
	
	public DialogFlow addChoice(String choiceFlowText) {
		choiceSet.addText(choiceFlowText);
		return this;
	}
	
	public DialogFlow addChoiceLine(int choiceFlowIndex, AbstractActor actor, String simpleLineText) {
		choiceSet.addLine(choiceFlowIndex, actor, simpleLineText);
		return this;
	}
	
	public DialogFlow addChoiceLine(AbstractActor actor, String simpleLineText) {
		choiceSet.addLine(actor, simpleLineText);
		return this;
	}
	
	public DialogSimpleLine currentLine(){
		return lines.get(currentLine);
	}

	@Override
	public String outCome() {
		return choiceSet.outCome();
	}

	@Override
	public IDialogStep currentStep() {
		if(currentLine < lines.size())
			return this;
		else if(choiceSet.flows.size() > 0)
			return choiceSet.currentStep();
		else return null;
	}

	@Override
	public void nextStep(int choice) {
		currentLine++;
		if(currentLine >= lines.size())
			choiceSet.nextStep(-1);
	}

	@Override
	public IDialogStep stepDrawClone() {
		DialogFlow flow = new DialogFlow();
		flow.lines.add(new DialogSimpleLine(lines.get(currentLine).lineOwner, lines.get(currentLine).text));
		return flow;
	}
}
