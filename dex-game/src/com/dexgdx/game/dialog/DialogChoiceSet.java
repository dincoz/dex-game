package com.dexgdx.game.dialog;

import java.util.ArrayList;
import java.util.List;

import com.dexgdx.game.object.AbstractActor;

public class DialogChoiceSet implements IDialogStep {
	
	public String outCome;
	public int chosenFlow;
	
	public List<DialogFlow> flows;
	public List<String> choiceTexts;
	
	public DialogChoiceSet() {
		flows = new ArrayList<DialogFlow>();
		choiceTexts = new ArrayList<String>();
		chosenFlow = -1;
	}
	
	public DialogFlow get(int i){
		return flows.get(i);
	}
	
	public DialogChoiceSet addText(String choiceText) {
		choiceTexts.add(choiceText);
		return this;
	}
	
	public DialogChoiceSet addText(int flowIndex, String choiceText) {
		if(flowIndex == choiceTexts.size()) {
			choiceTexts.add(choiceText);
		}
		else {
			choiceTexts.set(flowIndex, choiceText);
		}
		return this;
	}

	public DialogChoiceSet addLine(int flowIndex, AbstractActor actor, String text) {
		if(flowIndex == flows.size()) {
			flows.add(new DialogFlow());
		}
		flows.get(flowIndex).addLine(actor, text);
		return this;
	}

	public DialogChoiceSet addLine(AbstractActor actor, String text) {
		flows.get(flows.size() - 1).addLine(actor, text);
		return this;
	}

	public DialogChoiceSet addSimpleChoiceLine(int flowIndex, int choiceFlowIndex, AbstractActor actor, String text) {
		if(flowIndex == flows.size()) {
			flows.add(new DialogFlow());
		}
		flows.get(flowIndex).addChoiceLine(choiceFlowIndex, actor, text);
		return this;
	}

	public DialogChoiceSet addSimpleChoiceLine(int choiceFlowIndex, AbstractActor actor, String text) {
		flows.get(flows.size() - 1).addChoiceLine(choiceFlowIndex, actor, text);
		return this;
	}

	public DialogChoiceSet addSimpleChoiceLine(AbstractActor actor, String text) {
		flows.get(flows.size() - 1).addChoiceLine(actor, text);
		return this;
	}

	@Override
	public IDialogStep currentStep() {
		if(chosenFlow > -1)
			return flows.get(chosenFlow).currentStep();
		else
			return this;
	}

	@Override
	public String outCome() {
		String response = outCome + ",";
		for (DialogFlow flow : flows) {
			response += flow.outCome() + ",";
		}
		return response;
	}

	@Override
	public void nextStep(int choice) {
		chosenFlow = choice;
	}
	
	@Override
	public IDialogStep stepDrawClone() {
		DialogChoiceSet clone = new DialogChoiceSet();
		clone.choiceTexts.addAll(choiceTexts);
		return clone;
	}
}
