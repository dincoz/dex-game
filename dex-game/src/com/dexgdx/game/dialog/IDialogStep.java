package com.dexgdx.game.dialog;

public interface IDialogStep {

	IDialogStep currentStep();
	void nextStep(int choice);
	String outCome();
	IDialogStep stepDrawClone();
}
