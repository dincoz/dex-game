package com.dexgdx.game.dialog;

import com.dexgdx.game.object.AbstractActor;

public class DialogSimpleLine {
	
	public AbstractActor lineOwner;
	public String text;

	public DialogSimpleLine(AbstractActor lineOwner, String text) {
		super();
		this.text = text;
		this.lineOwner = lineOwner;
	}
	
}
