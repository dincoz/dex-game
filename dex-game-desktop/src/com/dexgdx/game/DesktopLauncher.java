package com.dexgdx.game;

import java.awt.Toolkit;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.dexgdx.game.constants.EGamePlatform;
import com.dexgdx.game.db.DesktopDBHandler;

public class DesktopLauncher {
	
	static float zoomRatio = 1f;
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.fullscreen = false;
        config.height = Toolkit.getDefaultToolkit().getScreenSize().height-200;
        config.width = Toolkit.getDefaultToolkit().getScreenSize().width/3;
		config.addIcon("data/icons/rsz_icon.png", FileType.Internal);
		config.title = "Dex";
		config.samples = 4;
		new LwjglApplication(new DexGdxGame(zoomRatio, EGamePlatform.PC, new DesktopDBHandler()), config);
//		new LwjglApplication(new DexGdxGameStage(), config);
//		new LwjglApplication(new DexGdxGameTest(zoomRatio, EGamePlatform.PC, new DesktopDBHandler()), config);
	}
}
