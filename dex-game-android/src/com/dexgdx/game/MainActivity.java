package com.dexgdx.game;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.dexgdx.game.constants.EGamePlatform;
import com.dexgdx.game.db.AndroidDBHandler;

public class MainActivity extends AndroidApplication {
	
	static float zoomRatio = 0.45f;
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new DexGdxGame(zoomRatio, EGamePlatform.ANDROID, new AndroidDBHandler(this.getBaseContext())), config);
//		initialize(new DexGdxGameStage(), config);
	}
}
