package com.dexgdx.game.db;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.dexgdx.game.db.core.AbstractDataHandler;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

public class AndroidDBHandler extends AbstractDataHandler {

	protected AndroidDB db_connection;
	protected SQLiteDatabase stmt;
	private final Context myContext;

	public AndroidDBHandler(Context context) {
		myContext = context;
		
		db_connection = new AndroidDB(myContext, database_name, null, version);

		try {
			db_connection.createDataBase();
		}
		catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
		
		stmt = db_connection.getWritableDatabase();
	}

	public void execute(String sql) {
		stmt.execSQL(sql);
	}

	public int executeUpdate(String sql) {
		stmt.execSQL(sql);
		SQLiteStatement tmp = stmt.compileStatement("SELECT CHANGES()");
		return (int) tmp.simpleQueryForLong();
	}

	public Result query(String sql) {
		ResultAndroid result = new ResultAndroid(stmt.rawQuery(sql, null));
		return result;
	}

	class AndroidDB extends SQLiteOpenHelper {

		public AndroidDB(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			stmt = db;
			AndroidDBHandler.this.onCreate();
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			stmt = db;
			AndroidDBHandler.this.onUpgrade();
		}

		public void createDataBase() throws IOException {
			
//			boolean dbExist = checkDataBase();

			boolean dbExist = false;
			if (dbExist) {
				// do nothing - database already exist
			}
			else {
				// By calling this method and empty database will be created
				// into the default system path
				// of your application so we are gonna be able to overwrite that
				// database with our database.
				db_connection.getReadableDatabase();

				try {
					copyDataBase();
				}
				catch (IOException e) {
					throw new Error("Error copying database");

				}
			}
		}

		/**
		 * Check if the database already exist to avoid re-copying the file each
		 * time you open the application.
		 * 
		 * @return true if it exists, false if it doesn't
		 */
//		private boolean checkDataBase() {
//			
//			SQLiteDatabase checkDB = null;
//			try {
//				checkDB = SQLiteDatabase.openDatabase(myContext
//						.getDatabasePath(database_name).getPath(), null,
//						SQLiteDatabase.OPEN_READONLY);
//			}
//			catch (SQLiteException e) {
//
//			}
//			if (checkDB != null) {
//				checkDB.close();
//
//			}
//			return checkDB != null ? true : false;
//		}

		private void copyDataBase() throws IOException {
			
			// Open your local db as the input stream
			InputStream myInput = myContext.getAssets().open("data/db/" + database_name+ ".sqlite");
			
			// Open the empty db as the output stream
			OutputStream myOutput = new FileOutputStream(myContext.getDatabasePath(database_name));

			// transfer bytes from the inputfile to the outputfile
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0, length);
			}

			// Close the streams
			myOutput.flush();
			myOutput.close();
			myInput.close();
		}

		public void openDataBase() throws SQLException {
			// Open the database
			stmt = SQLiteDatabase.openDatabase(
					myContext.getDatabasePath(database_name).getPath(), null,
					SQLiteDatabase.OPEN_READONLY);
		}

	}

	public class ResultAndroid implements Result {
		Cursor cursor;
		
		public ResultAndroid(Cursor cursor) {
			this.cursor = cursor;
		}

		public boolean isEmpty() {
			return cursor.getCount() == 0;
		}

		public int getColumnIndex(String name) {
			return cursor.getColumnIndex(name);
		}

		public String[] getColumnNames() {
			return cursor.getColumnNames();
		}

		public float getFloat(int columnIndex) {
			return cursor.getFloat(columnIndex);
		}

		public boolean moveToNext() {
			return cursor.moveToNext();
		}

		public int getInt(int columnIndex) {
			return cursor.getInt(columnIndex);
		}

		public String getString(int columnIndex) {
			return cursor.getString(columnIndex);
		}

		@Override
		public Object get(int columnIndex) {
			Object obj = null;
			if(cursor.getType(columnIndex) == Cursor.FIELD_TYPE_INTEGER)
				obj = cursor.getInt(columnIndex);
			else if(cursor.getType(columnIndex) == Cursor.FIELD_TYPE_STRING)
				obj = cursor.getString(columnIndex);
			else if(cursor.getType(columnIndex) == Cursor.FIELD_TYPE_FLOAT)
				obj = cursor.getFloat(columnIndex);
			return obj;
		}
	}

}